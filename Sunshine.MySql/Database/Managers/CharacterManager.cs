﻿using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.World;
using Sunshine.Protocol.Types;
using System;
using System.Collections.Generic;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.MySql.Database.Auth;
using Sunshine.Protocol.Utils;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.MySql.Database.Auth.Worlds;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Actors.Characters;
using Sunshine.MySql.Database.World.Breeds;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Game.Actors.Characters.Spells;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using Sunshine.MySql.Database.World.Characters.Shortcuts;
using Sunshine.WorldServer.Game.Actors.Characters.Shortcuts;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Actors.Stats;
using Sunshine.Logs;
using Sunshine.MySql.Database.World.Characters.Items;
using Sunshine.WorldServer.Game.Effects;
using Sunshine.WorldServer.Game.Actors.Characters.Inventory;
using Sunshine.MySql.Database.World.Items;
using Sunshine.Protocol.IO;
using Sunshine.WorldServer.Game.Items;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Actors.Characters.Jobs;
using Sunshine.WorldServer.Game.Actors.Characters.Quests;

namespace Sunshine.MySql.Database.Managers
{
    public class CharacterManager : Singleton<CharacterManager>
    {
        public Dictionary<int, Character> Characters = new Dictionary<int, Character>();

        public readonly Dictionary<StatsBoostTypeEnum, StatsEnum> StatsRelations = new Dictionary<StatsBoostTypeEnum, StatsEnum>
        {
                {StatsBoostTypeEnum.Strength, StatsEnum.Strength},
                {StatsBoostTypeEnum.Agility, StatsEnum.Agility},
                {StatsBoostTypeEnum.Chance, StatsEnum.Chance},
                {StatsBoostTypeEnum.Wisdom, StatsEnum.Wisdom},
                {StatsBoostTypeEnum.Intelligence, StatsEnum.Intelligence},
                {StatsBoostTypeEnum.Vitality, StatsEnum.Vitality},
        };

        public void CreateCharacter(int accountId, CharacterRecord character)
        {
            DatabaseManager.Connection.Insert(character);
            AccountManager accountManager = Singleton<AccountManager>.Instance;
            BreedManager breedManager = Singleton<BreedManager>.Instance;
            CreateCharacterStats(new CharacterStatsRecord { OwnerId = character.Id, AP = 6, MP = 3, Health = 55 });
            CreateCharacterAlign(new CharacterAlignmentRecord { OwnerId = character.Id, Grade = 1 });
            CreateCharacterSpells(character.Id, breedManager.GetSpells(character.Breed));
            CreateCharacterShortcutSpells(breedManager.GetSpellShortcuts(character.Id, character.Breed));
            accountManager.InsertCharacter(new WorldCharacter { Account = accountId, Owner = character.Id, World = 1 });
            Logger.WriteInfo($"Character {character.Name} was create...");
        }        

        private void CreateCharacterStats(CharacterStatsRecord characterStats)
        {
            DatabaseManager.Connection.Insert(characterStats);
        }

        public void CreateCharacterAlign(CharacterAlignmentRecord characteralign)
        {
            DatabaseManager.Connection.Insert(characteralign);
        }

        public void CreateCharacterSpells(int characterId, IEnumerable<BreedSpell> breedSpell)
        {
            string cmd = $"INSERT INTO characters_spells (OwnerId, Spell, Level) VALUES({characterId}, @Spell, 1)";
            DatabaseManager.Connection.Execute(cmd, breedSpell);
        }

        public void CreateCharacterShortcutSpells(IEnumerable<SpellShortcut> spellShortcut)
        {
            string cmd = "INSERT INTO characters_shortcuts_spells (OwnerId, Spell, Slot) VALUES(@OwnerId, @Spell, @Slot)";
            DatabaseManager.Connection.Execute(cmd, spellShortcut);
        }

        public IEnumerable<CharacterRecord> GetAllCharacters()
        {
            return DatabaseManager.Connection.Query<CharacterRecord>("SELECT * FROM characters");
        }

        public CharacterStatsRecord GetCharacterStats(int characterId)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<CharacterStatsRecord>($"SELECT * FROM characters_stats WHERE OwnerId = '{characterId}'");
        }

        public CharacterAlignmentRecord GetCharacterAlign(int characterId)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<CharacterAlignmentRecord>($"SELECT * FROM characters_alignments WHERE OwnerId = '{characterId}'");
        }

        public List<CharacterSpellRecord> GetCharacterSpells(int characterId)
        {
            return DatabaseManager.Connection.Query<CharacterSpellRecord>($"SELECT * FROM characters_spells WHERE OwnerId = '{characterId}'").ToList();
        }

        public List<T> GetShortcuts<T>(int characterId) where T : class
        {
            if (typeof(T).Name == "SpellShortcut")
                return DatabaseManager.Connection.Query<T>($"SELECT * FROM characters_shortcuts_spells WHERE OwnerId = '{characterId}'").ToList();
            else
                return DatabaseManager.Connection.Query<T>($"SELECT * FROM characters_shortcuts_items WHERE OwnerId = '{characterId}'").ToList();
        }

        public Dictionary<int, List<BasePlayerItem>> GetCharacterItems(int characterId)
        {
            var chrItems = DatabaseManager.Connection.Query<CharacterItemRecord>($"SELECT * FROM characters_items WHERE OwnerId = '{characterId}'");

            Dictionary<int, List<BasePlayerItem>> items = new Dictionary<int, List<BasePlayerItem>>();

            foreach(var chr in chrItems)
            {                      
                BasePlayerItem playerItem = new BasePlayerItem(chr.Item)
                {
                    Id = ItemManager.Instance.GenerateId(),
                    Position = (CharacterInventoryPositionEnum)chr.Position,
                    Stack = chr.Stack,
                    Effects = EffectManager.Instance.GetEffects(chr.Effects),
                    EffectSets = ItemManager.Instance.Items[chr.Item].EffectSets                    
                };

                if (items.ContainsKey(chr.Item))
                    items[chr.Item].Add(playerItem);
                else
                    items.Add(chr.Item, new List<BasePlayerItem>() { playerItem });
            }
            return items;
        }

        public List<CharacterJobRecord> GetCharacterJobs(int characterId)
        {
            return DatabaseManager.Connection.Query<CharacterJobRecord>($"SELECT * FROM characters_jobs WHERE OwnerId = '{characterId}'").ToList();
        }

        public List<CharacterQuestRecord> GetCharacterQuests(int characterId)
        {
            return DatabaseManager.Connection.Query<CharacterQuestRecord>($"SELECT * FROM characters_quests WHERE OwnerId = '{characterId}'").ToList();
        }

        public void Save(Character character)
        {
            try
            {
                StringBuilder builder = new StringBuilder();
                character.Record.EntityLook = EntityManager.Instance.ParseEntityLook(character.Look.GetEntityLook());
                for (int i = 0; i < character.Zaaps.Count; i++)
                    builder.Append(i == character.Zaaps.Count - 1 ? character.Zaaps[i].Id.ToString() 
                                                                  : character.Zaaps[i].Id + ",");
                character.Record.Zaaps = builder.ToString();
                DatabaseManager.Connection.Update(character.Record);
                SaveStats(character.Id, character.Stats);
                SaveAlignment(character.Id, character.Alignment);
                SaveSpells(character.Id, character.Spells);
                SaveShortcuts(character.Id, character.Shortcuts);
                SaveItems(character.Id, character.Inventory);
                SaveJobs(character.Id, character.Jobs);
                SaveQuests(character.Id, character.Quests);
            }
            catch (Exception e)
            {
                Logger.WriteError(e.ToString());
            }         
        }
       
        private void SaveStats(int characterId, StatsFields characterStats)
        {
            DatabaseManager.Connection.Execute($"DELETE FROM characters_stats WHERE OwnerId = '{characterId}'");
            DatabaseManager.Connection.Insert(characterStats.Record);
        }

        private void SaveAlignment(int characterId, CharacterAlignment characterAlign)
        {
            DatabaseManager.Connection.Execute($"DELETE FROM characters_alignments WHERE OwnerId = '{characterId}'");
            DatabaseManager.Connection.Insert(characterAlign.Record);
        }

        private void SaveSpells(int characterId, SpellInventory characterSpells)
        {
            DatabaseManager.Connection.Execute($"DELETE FROM characters_spells WHERE OwnerId = '{characterId}'");
            DatabaseManager.Connection.Insert(characterSpells.GetSpells());
        }

        private void SaveShortcuts(int characterId, ShortcutBar shortcutBar)
        {
            DatabaseManager.Connection.Execute($"DELETE FROM characters_shortcuts_spells WHERE OwnerId = '{characterId}'");
            if (shortcutBar.SpellShortcuts.Count > 0)
                DatabaseManager.Connection.Insert(shortcutBar.SpellShortcuts);
            DatabaseManager.Connection.Execute($"DELETE FROM characters_shortcuts_items WHERE OwnerId = '{characterId}'");
            if (shortcutBar.ItemShortcuts.Count > 0)
                DatabaseManager.Connection.Insert(shortcutBar.ItemShortcuts);
        }

        private void SaveItems(int characterId, Inventory inventory)
        {
            DatabaseManager.Connection.Execute($"DELETE FROM characters_items WHERE OwnerId = '{characterId}'");
            var itemsRecord = inventory.GetItems();
            foreach (var item in itemsRecord)
            {
                var chrItem = new CharacterItemRecord
                {
                    OwnerId = characterId,
                    Item = item.Template.Id,
                    Position = (byte)item.Position,
                    Stack = item.Stack,
                    Effects = EffectManager.Instance.SetEffects(new BigEndianWriter(), item.Effects)
                };
                DatabaseManager.Connection.Insert(chrItem);
            }
        }

        private void SaveJobs(int characterId, JobsCollection jobs)
        {
            DatabaseManager.Connection.Execute($"DELETE FROM characters_jobs WHERE OwnerId = '{characterId}'");
            foreach(var job in jobs.GetJobs())
                DatabaseManager.Connection.Insert(job);
        }

        private void SaveQuests(int characterId, QuestsCollection quests)
        {
            DatabaseManager.Connection.Execute($"DELETE FROM characters_quests WHERE OwnerId = '{characterId}'");
            foreach (var quest in quests.GetQuests())
                DatabaseManager.Connection.Insert(quest);
        }

        public void DeleteCharacterOnAccount(WorldClient client, Character character)
        {
            client.Characters.ToList().Remove(character.Record);
            DatabaseManager.Connection.Execute($"DELETE FROM worlds_characters WHERE Owner = '{character.Id}'");
            DatabaseManager.Connection.Execute($"DELETE FROM characters WHERE Id = '{character.Id}'");
            DatabaseManager.Connection.Execute($"DELETE FROM characters_stats WHERE OwnerId = '{character.Id}'");
            DatabaseManager.Connection.Execute($"DELETE FROM characters_alignments WHERE OwnerId = '{character.Id}'");
            DatabaseManager.Connection.Execute($"DELETE FROM characters_spells WHERE OwnerId = '{character.Id}'");
            if (character.Shortcuts.SpellShortcuts.Count > 0)
                DatabaseManager.Connection.Execute($"DELETE FROM characters_shortcuts_spells WHERE OwnerId = '{character.Id}'");
            if (character.Shortcuts.ItemShortcuts.Count > 0)
                DatabaseManager.Connection.Execute($"DELETE FROM characters_shortcuts_items WHERE OwnerId = '{character.Id}'");
        }

        public Character GetCharacter(string name)
        {
            var character = Characters.FirstOrDefault(x => x.Value.Name == name && x.Value.IsInWorld);
            return character.Value ?? null;
        }

        public Character GetCharacter(int id)
        {
            var character = Characters.FirstOrDefault(x => x.Value.Id == id && x.Value.IsInWorld);
            return character.Value ?? null;
        }

        public Character GetCharacter(int map, short cell)
        {
            var character = Characters.FirstOrDefault(x => x.Value.Map.Id == map && x.Value.Cell.Id == cell 
                                                                                 && x.Value.IsInWorld);
            return character.Value ?? null;
        }

        public Character GetCharacterByAccount(int id)
        {
            var character = Characters.FirstOrDefault(x => x.Value.Account.Id == id && x.Value.IsInWorld);
            return character.Value ?? Characters.FirstOrDefault(x => x.Value.Account.Id == id).Value;
        }

        public bool DoesNameExist(string name)
        {
            return Characters.FirstOrDefault(x => x.Value.Name == name).Value == null ? false : true;
        }

        public string GenerateName()
        {
            string text;
            do
            {
                System.Random random = new System.Random();
                int num = random.Next(5, 10);
                text = string.Empty;
                bool flag = random.Next(0, 2) == 0;
                text += Utils.GetChar(flag, random).ToString(System.Globalization.CultureInfo.InvariantCulture).ToUpper();
                flag = !flag;
                for (int i = 0; i < num - 1; i++)
                {
                    text += Utils.GetChar(flag, random);
                    flag = !flag;
                }
            }
            while (DoesNameExist(text));
            return text;
        }
    }
}
