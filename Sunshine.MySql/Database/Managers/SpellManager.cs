﻿using Dapper;
using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.World.Spells;
using Sunshine.Protocol.Utils;
using Sunshine.WorldServer.Game.Effects;
using Sunshine.WorldServer.Game.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.MySql.Database.Managers
{
    public class SpellManager : Singleton<SpellManager>
    {
        public Dictionary<int, List<Spell>> Spells = new Dictionary<int, List<Spell>>();

        public IEnumerable<SpellTemplate> GetSpellTemplate(int spellId)
        {
            return DatabaseManager.Connection.Query<SpellTemplate>($"SELECT * FROM spells WHERE SpellId = '{spellId}'");
        }

        public IEnumerable<SpellTemplate> GetAllSpellsTemplate()
        {
            return DatabaseManager.Connection.Query<SpellTemplate>($"SELECT * FROM spells");
        }

        public Spell GetSpell(SpellTemplate spellTemplate, sbyte level)
        {
            var effects = EffectManager.Instance.GetEffects(spellTemplate.Effects);
            var criticalEffects = EffectManager.Instance.GetEffects(spellTemplate.CriticalEffects);
            var spell = new Spell(spellTemplate.SpellId, level)
            {
                Effects = effects,
                CriticalEffects = criticalEffects,
                Template = spellTemplate
            };
            return spell;
        }
    }
}
