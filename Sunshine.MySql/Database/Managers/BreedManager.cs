﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Threading.Tasks;
using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.World;
using Sunshine.MySql.Database.World.Breeds;
using Sunshine.MySql.Database.World.Characters.Shortcuts;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Utils;

namespace Sunshine.MySql.Database.Managers
{
    public class BreedManager : Singleton<BreedManager>
    {
        public Dictionary<int, List<Tuple<byte, int, int>>> BreedStats = new Dictionary<int, List<Tuple<byte, int, int>>>();

        public Dictionary<int, Protocol.Tools.D2o.Classes.Breed> BreedColors = new Dictionary<int, Protocol.Tools.D2o.Classes.Breed>();

        public string GetLook(int breedId, bool sex)
        {
            if (!sex)
                return DatabaseManager.Connection.QueryFirstOrDefault<string>($"SELECT MaleLook FROM breeds WHERE Id = '{breedId}'");
            else
                return DatabaseManager.Connection.QueryFirstOrDefault<string>($"SELECT FemaleLook FROM breeds WHERE Id = '{breedId}'");
        }

        public int GetStartMap(int breedId)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<int>($"SELECT StartMap FROM breeds WHERE Id = '{breedId}'");
        }

        public short GetStartCell(int breedId)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<short>($"SELECT StartCell FROM breeds WHERE Id = '{breedId}'");
        }

        public int GetStartDirection(int breedId)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<int>($"SELECT StartDirection FROM breeds WHERE Id = '{breedId}'");
        }

        public IEnumerable<BreedSpell> GetSpells(int breedId)
        {
            return DatabaseManager.Connection.Query<BreedSpell>($"SELECT * FROM breeds_spells WHERE Breed = '{breedId}'");
        }

        public List<short> GetSpellIds(int breedId, int level)
        {
            return DatabaseManager.Connection.Query<short>($"SELECT Spell FROM breeds_spells WHERE Breed = '{breedId}' && ObtainLevel <= '{level}'").ToList();
        }

        public IEnumerable<SpellShortcut> GetSpellShortcuts(int characterId, int breedId)
        {
            var spells = GetSpellIds(breedId, 1);
            List<SpellShortcut> spellShortcuts = new List<SpellShortcut>();
            for (int i = 0; i < spells.Count; i++)
                spellShortcuts.Add(new SpellShortcut { OwnerId = characterId, Spell = spells[i], Slot = i});

            return spellShortcuts;
        }

        public string GetStatsFormulas(int breedId, StatsBoostTypeEnum statsBoost)
        {
            switch (statsBoost)
            {
                case StatsBoostTypeEnum.Agility:
                    return DatabaseManager.Connection.QueryFirstOrDefault<string>($"SELECT StatsPointsForAgilityCSV FROM breeds WHERE Id = '{breedId}'");

                case StatsBoostTypeEnum.Chance:
                    return DatabaseManager.Connection.QueryFirstOrDefault<string>($"SELECT StatsPointsForChanceCSV FROM breeds WHERE Id = '{breedId}'");

                case StatsBoostTypeEnum.Intelligence:
                    return DatabaseManager.Connection.QueryFirstOrDefault<string>($"SELECT StatsPointsForIntelligenceCSV FROM breeds WHERE Id = '{breedId}'");

                case StatsBoostTypeEnum.Strength:
                    return DatabaseManager.Connection.QueryFirstOrDefault<string>($"SELECT StatsPointsForStrengthCSV FROM breeds WHERE Id = '{breedId}'");

                default:
                    return null;
            }
        }

        public int SetStatsPoints(int boostPoint, out int endPoint, int breedId, StatsBoostTypeEnum statsBoost, short currentPoint)
        {
            if (statsBoost == StatsBoostTypeEnum.Vitality)
            {
                endPoint = 0;
                return breedId == 11 ? boostPoint * 2 : boostPoint;
            }
            else if (statsBoost == StatsBoostTypeEnum.Wisdom)
            {
                endPoint = (boostPoint - ((boostPoint / 3) * 3));
                return boostPoint / 3;
            }
            else
            {
                #region variables
                int totalStats = 0;
                var breedStats = BreedStats[breedId];
                var stepStats = breedStats.Where(x => x.Item1 == (byte)(statsBoost)).ToList();
                var step = stepStats.LastOrDefault(x => x.Item3 <= currentPoint);
                int actualStep = step.Item2;
                int i = stepStats.IndexOf(step);
                #endregion
                // Item1 = breedId | Item2 = coeff | Item3 = maxStat
                while (boostPoint > 0)
                {
                    int actualStats = stepStats[i].Item3;
                    int nextStats = stepStats[i + 1 >= 4 ? 4 : i + 1].Item3;
                    if (boostPoint > 0)
                    {
                        int countTotal = (nextStats - actualStats) * actualStep;
                        if (boostPoint >= countTotal)
                        {
                            if (countTotal == 0)
                            {
                                boostPoint = 0;
                                break;
                            }
                            boostPoint -= countTotal;
                            totalStats += nextStats - actualStats;
                            i = i >= 4 ? 4 : i + 1;
                        }
                        else
                        {
                            totalStats += boostPoint / actualStep;
                            boostPoint = boostPoint - ((boostPoint / actualStep) * actualStep);
                            break;
                        }
                    }
                    else
                        break;
                }
                endPoint = boostPoint;
                return totalStats;
            }
        }
    }
}
