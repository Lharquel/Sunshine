﻿using Dapper;
using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.World.Maps.Interactives;
using Sunshine.Protocol.Types;
using Sunshine.Protocol.Utils;
using Sunshine.WorldServer.Game.Maps.Interactives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.MySql.Database.Managers
{
    public class InteractiveManager : Singleton<InteractiveManager>
    {
        private UniqueIdProvider _idProvider = new UniqueIdProvider();

        public Dictionary<int, Tuple<int, int>> Interactives = new Dictionary<int, Tuple<int, int>>();

        public Dictionary<int, List<InteractiveSkill>> GetAllInteractiveSkills()
        {
            Dictionary<int, List<InteractiveSkill>> interactives = new Dictionary<int, List<InteractiveSkill>>();
            var skills = DatabaseManager.Connection.Query<InteractiveSkill>($"SELECT * FROM interactives_skills");
            foreach(var skill in skills)
            {
                if (interactives.ContainsKey(skill.ParentJob))
                    interactives[skill.ParentJob].Add(skill);
                else
                    interactives.Add(skill.ParentJob, new List<InteractiveSkill> { skill });
            }
            return interactives;
        }

        public Dictionary<int, List<Interactive>> GetAllInteractiveSpawns()
        {
            Dictionary<int, List<Interactive>> interactives = new Dictionary<int, List<Interactive>>();
            var spawns = DatabaseManager.Connection.Query<InteractiveSpawn>($"SELECT * FROM worlds_interactives");
            foreach (var spawn in spawns)
            {
                if (interactives.ContainsKey(spawn.Map))
                    interactives[spawn.Map].Add(new Interactive(spawn));
                else
                    interactives.Add(spawn.Map, new List<Interactive> { new Interactive(spawn) });
            }
            return interactives;
        }

        public IEnumerable<InteractiveSpawn> GetInteractiveSpawn(int map)
        {
            return DatabaseManager.Connection.Query<InteractiveSpawn>($"SELECT * FROM worlds_interactives WHERE Map = '{map}'");
        }

        public List<Interactive> GetInteractive(int map)
        {
            List<Interactive> interactives = new List<Interactive>();
            foreach(var interactive in GetInteractiveSpawn(map))
                interactives.Add(new Interactive(interactive));
            return interactives;
        }

        public int GenerateId()
        {
            return _idProvider.Pop();
        }
    }
}
