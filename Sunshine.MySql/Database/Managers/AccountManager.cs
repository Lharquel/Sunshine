﻿using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.Auth;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using Dapper.Contrib.Extensions;
using Sunshine.Protocol.Utils;
using Sunshine.Protocol.Types;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using Sunshine.MySql.Database.World;
using Sunshine.MySql.Database.Auth.Accounts;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.MySql.Database.Auth.Worlds;

namespace Sunshine.MySql.Database.Managers
{
    public class AccountManager : Singleton<AccountManager>
    {
        public Protocol.Types.Version requiredVersion = new Protocol.Types.Version(2, 3, 7, 35100, 0, 0);

        public Account GetAccount(string username, bool isForServer = false)
        {
            try
            {
                if (isForServer)
                    return CharacterManager.Instance.Characters.FirstOrDefault(x => x.Value.Account.Username == username).Value.Account;
                return DatabaseManager.Connection.QueryFirstOrDefault<Account>($"SELECT * FROM accounts WHERE Username = '{username}'");
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Account GetAccountByTicket(string ticket)
        {
            try
            {
                return DatabaseManager.Connection.QueryFirstOrDefault<Account>($"SELECT * FROM accounts WHERE Ticket = '{ticket}'");
            }
            catch (Exception)
            {
                return null;
            }       
        }

        public Account GetAccount(int characterId)
        {
            var character = DatabaseManager.Connection.QueryFirstOrDefault<WorldCharacter>($"SELECT * FROM worlds_characters WHERE Owner = {characterId}");
            return DatabaseManager.Connection.QueryFirstOrDefault<Account>($"SELECT * FROM accounts WHERE Id = '{character.Account}'");
        }

        public CharacterRecord[] GetCharacters(int accountId)
        {
            var characters = new List<CharacterRecord>();
            var worldCharacter = DatabaseManager.Connection.Query<WorldCharacter>($"SELECT * FROM worlds_characters WHERE Account = '{accountId}'");
            foreach(var chr in worldCharacter)
            {
                CharacterRecord _character = DatabaseManager.Connection.QueryFirstOrDefault<CharacterRecord>($"SELECT * FROM characters WHERE Id = '{chr.Owner}'");
                characters.Add(_character);
            }
            return characters.ToArray();
        }

        public CharacterRecord[] GetCharacters(int accountId, int serverId)
        {
            List<CharacterRecord> characters = new List<CharacterRecord>();
            var worldCharacter = DatabaseManager.Connection.Query<WorldCharacter>($"SELECT * FROM worlds_characters WHERE Account = '{accountId}' && World = '{serverId}'");
            foreach (var chr in worldCharacter)
            {
                CharacterRecord _character = DatabaseManager.Connection.QueryFirstOrDefault<CharacterRecord>($"SELECT * FROM characters WHERE Id = '{chr.Owner}'");
                characters.Add(_character);
            }
            return characters.ToArray();
        }

        public CharacterRecord GetCharacter(int characterId)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<CharacterRecord>($"SELECT * FROM characters WHERE Id = '{characterId}'");
        }

        public CharacterRecord GetFirstCharacter(int accountId)
        {
            var character = DatabaseManager.Connection.QueryFirstOrDefault<WorldCharacter>($"SELECT * FROM worlds_characters WHERE Account = '{accountId}'");
            return character == null ? null : DatabaseManager.Connection.QueryFirstOrDefault<CharacterRecord>($"SELECT * FROM characters WHERE Id = '{character.Owner}'");
        }

        public CharacterRecord GetCharacter(string name)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<CharacterRecord>($"SELECT * FROM characters WHERE Name = '{name}'");
        }

        public void InsertCharacter(WorldCharacter worldCharacter)
        {
            string cmd = "INSERT INTO worlds_characters (Account, Owner, World) values (@Account, @Owner, @World)";
            DatabaseManager.Connection.Execute(cmd, worldCharacter);
        }

        public void CreateAccount(Account account)
        {
            DatabaseManager.Connection.Insert(account);
            Logs.Logger.WriteInfo($"account {account.Username} was create...");
        }

        public void UpdateAccount(Account account)
        {
            string cmd = @"UPDATE accounts set Ticket = @Ticket where Username = @Username";
            DatabaseManager.Connection.Execute(cmd, account);
        }
       
        public bool IsBadVersion(Protocol.Types.Version clientVersion)
        {          
            if (Equals(requiredVersion, clientVersion))
                return false;

            return false;
        }
    }
}
