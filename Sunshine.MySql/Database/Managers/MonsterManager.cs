﻿using Sunshine.MySql.Database.World.Monsters;
using System;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Mysql.Database;
using Sunshine.Protocol.Utils;

namespace Sunshine.MySql.Database.Managers
{
    public class MonsterManager : Singleton<MonsterManager>
    {
        public IEnumerable<MonsterTemplate> GetAllMonsters()
        {
            return DatabaseManager.Connection.Query<MonsterTemplate>("SELECT * FROM monsters");
        }

        public IEnumerable<MonsterDrop> GetMonsterDrops(int monsterId)
        {
            return DatabaseManager.Connection.Query<MonsterDrop>($"SELECT * FROM monsters_drops WHERE MonsterId = '{monsterId}'");
        }

        public MonsterTemplate GetMonster(int monsterId)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<MonsterTemplate>($"SELECT * FROM monsters WHERE Id = '{monsterId}'");
        }

        public MonsterGrade[] GetMonsterGrades(int monsterId)
        {
            return DatabaseManager.Connection.Query<MonsterGrade>($"SELECT * FROM monsters_grades WHERE MonsterId = '{monsterId}'").ToArray();
        }

        public IEnumerable<MonsterSpawn> GetMonsterSpawns()
        {
            return DatabaseManager.Connection.Query<MonsterSpawn>($"SELECT * FROM worlds_monsters");
        }
    }
}
