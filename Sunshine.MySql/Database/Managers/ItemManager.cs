﻿using Dapper;
using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.World.Items;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Utils;
using Sunshine.WorldServer.Game.Actors.Characters.Inventory;
using Sunshine.WorldServer.Game.Items;
using Sunshine.Protocol.Utils.Extensions;
using Sunshine.WorldServer.Game.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.MySql.Database.Managers
{
    public class ItemManager : Singleton<ItemManager>
    {
        private UniqueIdProvider _idProvider = new UniqueIdProvider();

        public Dictionary<int, ItemRecord> Items = new Dictionary<int, ItemRecord>();

        public IEnumerable<ItemRecord> GetAllItemsTemplate()
        {
            var items = DatabaseManager.Connection.Query<ItemTemplate>("SELECT * FROM items");
            var weapons = DatabaseManager.Connection.Query<WeaponTemplate>("SELECT * FROM items_weapons");
            return new List<ItemRecord>().Concat(items).Concat(weapons);
        }

        public IEnumerable<ItemSetTemplate> GetAllItemSetsTemplate()
        {
            return DatabaseManager.Connection.Query<ItemSetTemplate>("SELECT * FROM items_sets");
        }

        public ItemSetTemplate GetItemSetTemplate(int itemSet)
        {
            return DatabaseManager.Connection.QueryFirstOrDefault<ItemSetTemplate>($"SELECT * FROM items_sets WHERE Id = '{itemSet}'");
        }

        public BasePlayerItem CreatePlayerItem(int itemId, int quantity = 1, bool isJetMax = false)
        {
            var item = new BasePlayerItem(itemId)
            {
                Id = _idProvider.Pop(),
                Position = CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED,
                Stack = quantity,
                Effects = Items[itemId].EffectsBase.Clone(),
                EffectSets = Items[itemId].EffectSets
            };

            item.Effects.ForEach(x => x.GenerateEffect(isJetMax));

            return item.Clone();
        }

        public BasePlayerItem CreatePlayerItem(int itemId, List<Effect> effects, int quantity = 1)
        {
            var item = new BasePlayerItem(itemId)
            {
                Id = _idProvider.Pop(),
                Position = CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED,
                Stack = quantity,
                Effects = effects.Clone(),
                EffectSets = Items[itemId].EffectSets
            };

            return item.Clone();
        }

        public int GenerateId()
        {
            return _idProvider.Pop();
        }
    }
}
