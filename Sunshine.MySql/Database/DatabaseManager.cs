﻿using MySql.Data.MySqlClient;
using Sunshine.Logs;
using Sunshine.Protocol.Utils;
using Sunshine.Servers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.Mysql.Database
{
    public static class DatabaseManager
    {
        private static string[] _reader = System.IO.File.ReadAllLines(".\\Database.xml");
        private static MySqlConnection _connection;
        private static object _locker = new object();

        public static MySqlConnection Connection
        {
            get
            {
                lock (_locker)
                {
                    return _connection;
                }
            }
            set
            {
                _connection = value;
            }
        }

        private static string Database()
        {
           return _reader[2].Remove(0, 11);
        }

        private static string Hostname()
        {
            return _reader[3].Remove(0, 11);
        }

        private static string Port()
        {
            return _reader[4].Remove(0, 7);
        }

        private static string Username()
        {
            return _reader[5].Remove(0, 11);
        }

        private static string Password()
        {
            return _reader[6].Remove(0, 10);
        }
          
        public static void Initilize()
        {
            try
            {
                string m_mysql = string.Format("server={0};database={1};uid={2};pwd={3};Allow User Variables=True", Hostname(), Database(), Username(), Password());
                _connection = new MySqlConnection(m_mysql);
                Logger.Write("[ Server MYSQL ] Initialization Database");
                Logger.Write("[ Server MYSQL ] Opening Database....");
                _connection.Open();
                Logger.Write("[ Server MYSQL ] Connected to the Database");                    
            }
            catch (Exception e)
            {
                Logger.WriteError(e.ToString());
            }
        }                                    
    }
}
