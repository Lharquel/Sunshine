﻿using Dapper.Contrib.Extensions;
using Sunshine.Protocol.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.MySql.Database.World.Characters
{
    [Table("characters_quests")]
    public class CharacterQuestRecord
    {
        public int OwnerId { get; set; }
        public short Quest { get; set; }
        public string StepsStartedCSV { get; set; }
        public string StepsEndedCSV { get; set; }
        public string ObjectivesStartedCSV { get; set; }
        public string ObjectivesEndedCSV { get; set; }
        public bool IsFinished { get; set; }
        public bool isValided { get; set; }
    }
}
