﻿namespace Sunshine.MySql.Database.World.Characters.Shortcuts
{
    public abstract class Shortcut
    {
        public abstract Protocol.Types.Shortcut GetNetworkShortcut();
    }
}
