﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;

namespace Sunshine.MySql.Database.World.Npcs
{
    [Table("npcs_replies")]
    public class NpcReplie
    {
        public int NpcId { get; set; }
        public string TypeIdsCSV { get; set; }
        public short MessageIdsCSV { get; set; }
        public string ReplieIdsCSV { get; set; }
        public string ParametersCSV { get; set; }
    }
}
