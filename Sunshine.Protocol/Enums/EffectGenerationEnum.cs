namespace Sunshine.Protocol.Enums
{
	public enum EffectGenerationEnum
	{
		Normal,
		MaxEffects,
		MinEffects
	}
}
