﻿namespace Sunshine.Protocol.Enums
{
    public enum InteractiveStateEnum
    {
        STATE_NORMAL = 0,
        STATE_ACTIVATED = 1,
        STATE_ANIMATED = 2,
    }
}