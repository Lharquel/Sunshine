using System;
namespace Sunshine.Protocol.Enums
{
	public enum StatsBoostTypeEnum
	{
        Strength = 10,
        Vitality,
        Wisdom,
        Chance,
        Agility,
        Intelligence
    }
}
