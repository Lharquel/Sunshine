using System;
using System.Collections.Generic;

namespace Sunshine.Protocol.Enums
{
	public enum PartyTypeEnum
	{
		PARTY_TYPE_UNDEFINED,
		PARTY_TYPE_CLASSICAL,
		PARTY_TYPE_DUNGEON
	}
}
