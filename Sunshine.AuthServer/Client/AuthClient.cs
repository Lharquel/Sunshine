﻿using Sunshine.AuthServer.Handlers;
using Sunshine.BaseClient;
using Sunshine.Logs;
using Sunshine.MySql.Database.Auth;
using Sunshine.MySql.Database.Auth.Accounts;
using Sunshine.Protocol.IO;
using Sunshine.Protocol.Messages;
using Sunshine.Servers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.AuthServer.Client
{
    public class AuthClient : IBaseClient
    {
        #region Socket
        private Socket _client;
        private byte[] _buffer = new byte[8192];
        public string Ip { get; set; }
        public int Port { get; set; }

        public void Initialize()
        {
            _client.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), _client);
        }

        public void Disconnect()
        {
            Logger.WriteInfo(string.Format("Client <{0}:{1}> disconnected from AuthServer...", Ip, Port));
            ServersManager.Instance.RemoveClient(this);
        }      

        private void ReceiveCallBack(IAsyncResult result)
        {
            try
            {
                _client = (Socket)result.AsyncState;

                if (!this.Connected())
                {
                    this.Disconnect();
                    return;
                }

                int size = _client.EndReceive(result);
                byte[] buffer = new byte[size];
                Array.Copy(_buffer, buffer, buffer.Length);
                MessageDispatcher.Dispatch(this, buffer);
                _client.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), _client);
            }
            catch (Exception e)
            {
                return;
            }
           
        }

        public void Send(Message message)
        {
            try
            {
                using (BigEndianWriter writer = new BigEndianWriter())
                {
                    message.Pack(writer);
                    _client.BeginSend(writer.Data, 0, writer.Data.Length, SocketFlags.None, new AsyncCallback(SendCallBack), _client);
                    Logger.WriteServer("AuthServer", AuthServer.Ip, AuthServer.Port, message.ToString());
                }
            }
            catch (Exception e)
            {
                Logger.WriteError(e.ToString());
                Disconnect();
            }
        }

        private void SendCallBack(IAsyncResult result)
        {
            try
            {
                _client = (Socket)result.AsyncState;
                _client.EndSend(result);
            }
            catch (Exception e)
            {
                return;
            }

        }

        private bool Connected()
        {
            if ((_client != null && _client.Connected))
            {
                try
                {
                    if (_client.Poll(0, SelectMode.SelectRead))
                    {
                        if (_client.Receive(new byte[1], SocketFlags.Peek) == 0)
                            return false;
                    }
                    return true;
                }
                catch (SocketException ex)
                {
                    return false;
                }
            }
            else
                return false;
        }
        #endregion

        public AuthClient(Socket client)
        {
            _client = client;
            Ip = ((IPEndPoint)_client.RemoteEndPoint).Address.ToString();
            Port = ((IPEndPoint)_client.RemoteEndPoint).Port;
        }

        public string Ticket { get; set; }

        public Account Account { get; set; }
    }
}
