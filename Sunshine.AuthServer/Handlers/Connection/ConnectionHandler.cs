﻿using Sunshine.AuthServer.Client;
using Sunshine.MySql.Database.Auth;
using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.IO;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Utils;
using Sunshine.Protocol.Types;
using Sunshine.MySql.Database.Auth.Accounts;
using Sunshine.MySql.Database.Auth.Worlds;

namespace Sunshine.AuthServer.Handlers.Connection
{
    public class ConnectionHandler : AuthPacketHandler
    {
        [AuthHandler(4)]
        public static void HandleIdentificationMessage(AuthClient client, IdentificationMessage message)
        {
            Account account = AccountManager.Instance.GetAccount(message.login);

            if (account == null)
            {
                client.Send(new IdentificationFailedMessage((sbyte)IdentificationFailureReasonEnum.WRONG_CREDENTIALS));
                return;
            }
               
            if(account.IsBanned)
            {
                client.Send(new IdentificationFailedBannedMessage((sbyte)IdentificationFailureReasonEnum.BANNED, 60));
                return;
            }
            else
            {               
                if(AccountManager.Instance.IsBadVersion(message.version))
                {
                    client.Send(new IdentificationFailedForBadVersionMessage((sbyte)IdentificationFailureReasonEnum.BAD_VERSION, AccountManager.Instance.requiredVersion));
                    return;
                }

                string _passHash = Utils.GetMD5Hash(account.Password + client.Ticket);

                if(message.password != _passHash)
                    client.Send(new IdentificationFailedMessage((sbyte)IdentificationFailureReasonEnum.WRONG_CREDENTIALS));
                else
                {
                    client.Send(new IdentificationSuccessMessage(account.Role > (int)RoleEnum.Player, false, account.Nickname, account.Id, 0, account.SecretQuestion, 1520534951.0009));
                    client.Account = account;
                    client.Account.Ticket = client.Ticket;
                    AccountManager.Instance.UpdateAccount(client.Account);
                    ConnectionHandler.SendServerListMessage(client);
                }                
            }                 
        }

        [AuthHandler(6194)]
        public static void HandleIdentificationWithServerIdMessage(AuthClient client, IdentificationWithServerIdMessage message)
        {
            Account account = AccountManager.Instance.GetAccount(message.login, true);

            if (account == null)
            {
                client.Send(new IdentificationFailedMessage((sbyte)IdentificationFailureReasonEnum.WRONG_CREDENTIALS));
                return;
            }

            if (account.IsBanned)
            {
                client.Send(new IdentificationFailedBannedMessage((sbyte)IdentificationFailureReasonEnum.BANNED, 60));
                return;
            }
            else
            {
                if (AccountManager.Instance.IsBadVersion(message.version))
                {
                    client.Send(new IdentificationFailedForBadVersionMessage((sbyte)IdentificationFailureReasonEnum.BAD_VERSION, AccountManager.Instance.requiredVersion));
                    return;
                }

                string passHash = Utils.GetMD5Hash(account.Password + client.Ticket);

                if (message.password != passHash)
                    client.Send(new IdentificationFailedMessage((sbyte)IdentificationFailureReasonEnum.WRONG_CREDENTIALS));
                else
                {
                    client.Send(new IdentificationSuccessMessage(account.Role > (int)RoleEnum.Player, false, account.Nickname, account.Id, 0, account.SecretQuestion, 1520534951.0009));
                    client.Account = account;
                    client.Account.Ticket = client.Ticket;
                    AccountManager.Instance.UpdateAccount(client.Account);
                    World world = WorldServerManager.Instance.GetWorld(message.serverId);
                    client.Send(new SelectedServerDataMessage((short)world.Id, world.Address, (ushort)world.Port, true, client.Ticket));
                    client.Disconnect();
                }
            }
        }

        [AuthHandler(40)]
        public static void HandleServerSelectionMessage(AuthClient client, ServerSelectionMessage message)
        {
            World world = WorldServerManager.Instance.GetWorld(message.serverId);

            if (world == null)
                client.Send(new SelectedServerRefusedMessage((short)message.serverId, (sbyte)ServerConnectionErrorEnum.SERVER_CONNECTION_ERROR_LOCATION_RESTRICTED, 0));         
            else if (world.Status != (int)ServerStatusEnum.ONLINE)
                client.Send(new SelectedServerRefusedMessage((short)world.Id, (sbyte)ServerConnectionErrorEnum.SERVER_CONNECTION_ERROR_DUE_TO_STATUS, (sbyte)world.Status));
            else if (world.RequiredRole > client.Account.Role)
                client.Send(new SelectedServerRefusedMessage((short)world.Id, (sbyte)ServerConnectionErrorEnum.SERVER_CONNECTION_ERROR_ACCOUNT_RESTRICTED, (sbyte)world.Status));
            else
            {
                client.Send(new SelectedServerDataMessage((short)world.Id, world.Address, (ushort)world.Port, true, client.Ticket));
                client.Disconnect();
            }
        }

        public static void SendServerListMessage(AuthClient client)
        {
            World[] worlds = WorldServerManager.Instance.GetWorlds();
            List<GameServerInformations> serversInformations = new List<GameServerInformations>();
            
            if(worlds.Length > 0)
            {
                foreach(var server in worlds)
                {
                    serversInformations.Add(new GameServerInformations(
                    (ushort)server.Id,
                    (sbyte)server.Status,
                    (sbyte)server.CharCapacity,
                    server.Selectable,
                    (sbyte)AccountManager.Instance.GetCharacters(client.Account.Id, server.Id).Length));
                }
            }
            client.Send(new ServersListMessage(serversInformations));
        }

        public static void SendServerStatusUpdateMessage(AuthClient client, ServerStatusEnum status)
        {
            World[] worlds = WorldServerManager.Instance.GetWorlds();
            List<GameServerInformations> serversInformations = new List<GameServerInformations>();

            if (worlds.Length > 0)
            {
                foreach (var server in worlds)
                {
                    client.Send(new ServerStatusUpdateMessage(new GameServerInformations((ushort)server.Id, (sbyte)status,
                       (sbyte)server.CharCapacity, server.Selectable, (sbyte)AccountManager.Instance.GetCharacters(client.Account.Id, server.Id).Length)));
                }
            }         
        }

    }
}
