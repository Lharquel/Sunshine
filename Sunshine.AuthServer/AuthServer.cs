﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;
using Sunshine.Logs;
using Sunshine.Protocol.Messages;
using Sunshine.Protocol.IO;
using Sunshine.AuthServer.Client;
using Sunshine.Protocol.Utils;
using Sunshine.Servers;
using Sunshine.BaseClient;

namespace Sunshine.AuthServer
{
    public class AuthServer
    {
        #region Socket
        private static string[] m_config = File.ReadAllLines(".\\Config.xml");
        public static string Ip { get { return m_config[5].Remove(0, 5); } }
        public static int Port { get { return int.Parse(m_config[6].Remove(0, 7)); } }
        private Socket m_authserver;
        private Socket m_client;
        public Dictionary<Socket, IBaseClient> authClients = new Dictionary<Socket, IBaseClient>();
        private IPEndPoint Auth_Address
        {
            get { return new IPEndPoint(IPAddress.Parse(Ip), Port); }
        }
        #endregion

        public AuthServer()
        {
            m_authserver = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Initialize()
        {
            Bind();
            Listen(100);
            Accept();
        }

        private void Bind()
        {
            m_authserver.Bind(Auth_Address);
        }

        private void Listen(int file)
        {
            m_authserver.Listen(file);
            Logger.WriteInfo(string.Format("Starting IPC Auth {0}:{1}", Ip, Port));
        }

        private void Accept()
        {
            m_authserver.BeginAccept(new AsyncCallback(AcceptCallBack), null);
        }

        private void AcceptCallBack(IAsyncResult result)
        {
            AuthClient authClient = new AuthClient(m_client = m_authserver.EndAccept(result));
            try
            {
                ServersManager.Instance.AddClient(m_client, authClient);
                Logger.WriteInfo(string.Format("Client <{0}:{1}> connected to AuthServer...", authClient.Ip, authClient.Port));
                authClient.Send(new ProtocolRequired(1375, 1375));
                authClient.Send(new HelloConnectMessage(1, authClient.Ticket = Utils.RandomString(32, true)));
                authClient.Initialize();
                Accept();
            }
            catch (Exception e)
            {
                Logger.WriteError(e.ToString());
                authClient.Disconnect();
            }
        }
    }
}
