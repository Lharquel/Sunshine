﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Items;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Items
{
    public class BasePlayerItem
    {
        public ItemRecord Template { get; set; }

        public BasePlayerItem(ItemRecord item)
        {
            Template = item.Clone();
        }

        public BasePlayerItem(int id)
        {
            Template = ItemManager.Instance.Items[id].Clone();
        }

        public int Id { get; set; }

        public short Level { get { return Template.Level; } }

        public ItemTypeEnum Type { get { return (ItemTypeEnum)Template.TypeId; } }

        public short AppearanceId { get { return Template.AppearanceId; } }

        public string Criteria { get { return Template.Criteria; } }

        public int Weight { get { return Template.Weight; } }

        public CharacterInventoryPositionEnum Position { get; set; }

        public int Stack { get; set; }

        public List<Effect> Effects { get; set; }

        public List<List<Effect>> EffectSets { get; set; }

        public int Price { get; set; }

        public bool Selled { get; set; }

        public bool IsEquiped()
        {
            return Position >= CharacterInventoryPositionEnum.ACCESSORY_POSITION_AMULET
              && Position <= CharacterInventoryPositionEnum.INVENTORY_POSITION_MOUNT;
        }

        public ObjectItem GetObjectItem()
        {
            return new ObjectItem((byte)Position, (short)Template.Id, 0, false, 
                                  Effects.Select(x => x.GetObjectEffectInteger()), Id, Stack);
        }
        
        public ObjectItemToSellInBid GetObjectItemToSell()
        {
            return new ObjectItemToSellInBid((short)Template.Id, 0, false, Effects.Select(x => x.GetObjectEffectInteger()), Template.Id, Stack, Price, 0);
        }

        public BidExchangerObjectInfo GetBidExchangerObjectInfo()
        {
            return new BidExchangerObjectInfo(Id, 0, false, Effects.Select(x => x.GetObjectEffectInteger()), BidHouseManager.Instance.BidsHousePrices[this]);
        }

        public ObjectItemNotInContainer GetObjectItemNotInContainer()
        {
            return new ObjectItemNotInContainer((short)Template.Id, 0, false, Effects.Select(x => x.GetObjectEffectInteger()), Id, Stack);
        }

        public BasePlayerItem Clone()
        {
            return (BasePlayerItem)this.MemberwiseClone();
        }     
    }
}
