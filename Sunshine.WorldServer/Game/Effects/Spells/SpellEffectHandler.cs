﻿using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.WorldServer.Game.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Effects.Spells
{
    public abstract class SpellEffectHandler
    {
        private EffectsEnum _id;
        private uint _diceNum;
        private uint _diceFace;
        private int _value;
        private int _delay;
        private int _duration;
        private SpellTargetType _target;
        private short _targetedCell;
        private IEnumerable<FightActor> _affectedActors;
        private FightActor _caster;
        private Spell _spell;

        public abstract void Apply();

        public void Initialize(List<object> parameters)
        {           
            _id = (EffectsEnum)parameters[0];
            _diceNum = (uint)parameters[1];
            _diceFace = (uint)parameters[2];
            _value = (int)parameters[3];
            _delay = (int)parameters[4];
            _duration = (int)parameters[5];
            _target = (SpellTargetType)parameters[6];
            _targetedCell = (short)parameters[7];
            _affectedActors = (IEnumerable<FightActor>)parameters[8];
            _caster = (FightActor)parameters[9];
            _spell = (Spell)parameters[10];
            this.Apply();
        }

        public EffectsEnum Id { get { return _id; } set { _id = value; } }

        public uint DiceNum { get { return _diceNum; } set { _diceNum = value; } }

        public uint DiceFace { get { return _diceFace; } set { _diceFace = value; } }

        public int Value { get { return _value; } set { _value = value; } }

        public int Delay { get { return _delay; } set { _delay = value; } }

        public int Duration { get { return _duration; } set { _duration = value; } }

        public SpellTargetType Target { get { return _target; } set { _target = value; } }

        public IEnumerable<FightActor> GetAffectedActors() { return _affectedActors; }

        public FightActor Caster { get { return _caster; } }

        public Spell Spell { get { return _spell; } }

        public short TargetedCell { get { return _targetedCell; } set { _targetedCell = value; } }

        public Fight Fight { get { return Caster != null ? Caster.Fight : null; } }
    }
}
