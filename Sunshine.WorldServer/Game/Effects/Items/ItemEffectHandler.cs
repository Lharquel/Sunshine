﻿using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Effects.Items
{
    public static class ItemEffectHandler
    {
        public static Dictionary<EffectsEnum, StatsEnum> EffectsRelations = new Dictionary<EffectsEnum, StatsEnum>()
        {
            {EffectsEnum.Effect_AddAP_111, StatsEnum.AP},
            {EffectsEnum.Effect_AddMP, StatsEnum.MP},
            {EffectsEnum.Effect_AddMP_128, StatsEnum.MP},
            {EffectsEnum.Effect_AddRange, StatsEnum.Range},
            {EffectsEnum.Effect_AddRange_136, StatsEnum.Range},
            {EffectsEnum.Effect_AddProspecting, StatsEnum.Prospecting},
            {EffectsEnum.Effect_IncreaseWeight, StatsEnum.Weight},
            {EffectsEnum.Effect_AddLock, StatsEnum.TackleBlock},
            {EffectsEnum.Effect_AddDodge, StatsEnum.TackleEvade},
            {EffectsEnum.Effect_412, StatsEnum.MPAttack},
            {EffectsEnum.Effect_APAttack, StatsEnum.APAttack},
            {EffectsEnum.Effect_AddHealth, StatsEnum.Health},
            {EffectsEnum.Effect_AddHealBonus, StatsEnum.HealBonus},
            {EffectsEnum.Effect_AddInitiative, StatsEnum.Initiative},
            {EffectsEnum.Effect_AddIntelligence, StatsEnum.Intelligence},
            {EffectsEnum.Effect_AddAgility, StatsEnum.Agility},
            {EffectsEnum.Effect_AddChance, StatsEnum.Chance},
            {EffectsEnum.Effect_AddVitality, StatsEnum.Vitality},
            {EffectsEnum.Effect_AddStrength, StatsEnum.Strength},
            {EffectsEnum.Effect_AddWisdom, StatsEnum.Wisdom},
            {EffectsEnum.Effect_AddAirDamageBonus, StatsEnum.AirDamageBonus},
            {EffectsEnum.Effect_AddFireDamageBonus, StatsEnum.FireDamageBonus},
            {EffectsEnum.Effect_AddWaterDamageBonus, StatsEnum.WaterDamageBonus},
            {EffectsEnum.Effect_AddEarthDamageBonus, StatsEnum.EarthDamageBonus},
            {EffectsEnum.Effect_AddNeutralDamageBonus, StatsEnum.NeutralDamageBonus},
            {EffectsEnum.Effect_AddCriticalHit, StatsEnum.CriticalHit},
            {EffectsEnum.Effect_AddCriticalMiss, StatsEnum.CriticalMiss},
            {EffectsEnum.Effect_AddCriticalDamageBonus, StatsEnum.CriticalDamageBonus},
            {EffectsEnum.Effect_AddPushDamageBonus, StatsEnum.PushDamageBonus},
            {EffectsEnum.Effect_AddNeutralResistPercent, StatsEnum.NeutralResistPercent},
            {EffectsEnum.Effect_AddEarthResistPercent, StatsEnum.EarthResistPercent},
            {EffectsEnum.Effect_AddFireResistPercent, StatsEnum.FireResistPercent},
            {EffectsEnum.Effect_AddWaterResistPercent, StatsEnum.WaterResistPercent},
            {EffectsEnum.Effect_AddAirResistPercent, StatsEnum.AirResistPercent},
            {EffectsEnum.Effect_AddPvpNeutralResistPercent, StatsEnum.PvpNeutralResistPercent},
            {EffectsEnum.Effect_AddPvpEarthResistPercent, StatsEnum.PvpEarthResistPercent},
            {EffectsEnum.Effect_AddPvpFireResistPercent, StatsEnum.PvpFireResistPercent},
            {EffectsEnum.Effect_AddPvpWaterResistPercent, StatsEnum.PvpWaterResistPercent},
            {EffectsEnum.Effect_AddPvpAirResistPercent, StatsEnum.PvpAirResistPercent},
            {EffectsEnum.Effect_AddAirElementReduction, StatsEnum.AirElementReduction},
            {EffectsEnum.Effect_AddFireElementReduction, StatsEnum.FireElementReduction},
            {EffectsEnum.Effect_AddWaterElementReduction, StatsEnum.WaterElementReduction},
            {EffectsEnum.Effect_AddNeutralElementReduction, StatsEnum.NeutralElementReduction},
            {EffectsEnum.Effect_AddEarthElementReduction, StatsEnum.EarthElementReduction},
            {EffectsEnum.Effect_AddPvpAirElementReduction, StatsEnum.PvpAirElementReduction},
            {EffectsEnum.Effect_AddPvpEarthElementReduction, StatsEnum.PvpEarthElementReduction},
            {EffectsEnum.Effect_AddPvpNeutralElementReduction, StatsEnum.PvpNeutralElementReduction},
            {EffectsEnum.Effect_AddPvpFireElementReduction,StatsEnum.PvpFireElementReduction},
            {EffectsEnum.Effect_AddPvpWaterElementReduction, StatsEnum.PvpWaterElementReduction},
        };
    }
}
