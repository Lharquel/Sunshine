﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Items;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Items;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Handlers.Characters.Inventory;
using System;
using System.Collections.Generic;
using Sunshine.Protocol.Utils.Extensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Handlers.Basic;
using Sunshine.WorldServer.Game.Effects;
using Sunshine.WorldServer.Game.Effects.Items;

namespace Sunshine.WorldServer.Game.Actors.Characters.Inventory
{
    public class Inventory
    {
        private Character _character;
        private Dictionary<int, List<BasePlayerItem>> _items;
        private Dictionary<int, List<Effect>> _itemSetsEffects;

        public Inventory(Character character)
        {
            _character = character;
            _items = CharacterManager.Instance.GetCharacterItems(character.Id);
            _itemSetsEffects = new Dictionary<int, List<Effect>>();
            foreach (var item in GetEquipedItems())
            {
                ApplyItemEffects(item, true, false);
                ApplyItemSetEffects(item, false);
            }
        }

        public int Kamas
        {
            get { return _character.Record.Kamas; }
            set { _character.Record.Kamas = value; }
        }

        public bool IsFull(BasePlayerItem item, int stack)
        {
            return (Weight() + (item.Weight * stack)) >= WeightTotal();
        }

        public bool IsFull()
        {
            return Weight() >= WeightTotal();
        }

        public int Weight()
        {
            return GetItems().Sum(x => x.Weight);
        }

        public int WeightTotal()
        {
            return 1000 + _character.Stats[StatsEnum.Strength].Total * 5 + _character.Jobs.GetJobsLevelTotal() * 5 + _character.Jobs.GetJobsCount(100) * 1000 + _character.Stats[StatsEnum.Weight].Total;
        }

        public IEnumerable<BasePlayerItem> GetItems()
        {
            List<BasePlayerItem> playerItems = new List<BasePlayerItem>();
            foreach (var items in _items.Values)
                items.ForEach(x => playerItems.Add(x));
            return playerItems;
        }

        public IEnumerable<BasePlayerItem> GetItems(CharacterInventoryPositionEnum position)
        {
            List<BasePlayerItem> playerItems = new List<BasePlayerItem>();
            foreach (var items in _items.Values)
            {
                foreach (var item in items.Where(x => x.Position == position))
                    playerItems.Add(item);
            }
            return playerItems;
        }

        public IEnumerable<BasePlayerItem> GetEquipedItems()
        {
            List<BasePlayerItem> playerItems = new List<BasePlayerItem>();
            foreach (var items in _items.Values)
            {
                foreach (var item in items.Where(x => x.IsEquiped()))
                    playerItems.Add(item);
            }
            return playerItems;
        }

        public BasePlayerItem GetItemUid(int uid)
        {
            foreach (var items in _items.Values)
            {
                var item = items.FirstOrDefault(x => x.Id == uid);
                if (item != null)
                    return item;
            }
            return null;
        }

        public BasePlayerItem GetItem(int guid)
        {
            if (_items.ContainsKey(guid))
                return _items[guid].FirstOrDefault();
            return null;
        }

        public BasePlayerItem GetItem(int guid, CharacterInventoryPositionEnum position)
        {
            if (_items.ContainsKey(guid))
                return _items[guid].FirstOrDefault(x => x.Position == position);
            return null;
        }

        public BasePlayerItem GetItem(int guid, CharacterInventoryPositionEnum position, List<Effect> effects)
        {
            if (_items.ContainsKey(guid))
                return _items[guid].FirstOrDefault(x => x.Position == position
                                                  && IsSameEffects(x.Effects, effects));
            return null;
        }

        public BasePlayerItem GetItem(CharacterInventoryPositionEnum position)
        {
            foreach (var items in _items.Values)
            {
                var item = items.FirstOrDefault(x => x.Position == position);
                if (item != null)
                    return item;
            }
            return null;
        }

        public IEnumerable<BasePlayerItem> GetItemSetsEquiped(int itemSet)
        {
            return GetEquipedItems().Where(x => x.Template.ItemSetId == itemSet);
        }

        public void AddItem(BasePlayerItem item, int quantity = 1, bool isJetMax = false)
        {
            var sameItem = GetItem(item.Template.Id, item.Position, item.Effects);
            if (sameItem != null) // Stack
            {
                _items[item.Template.Id].Remove(sameItem);
                sameItem.Stack += quantity;
                _items[item.Template.Id].Add(sameItem);
                InventoryHandler.SendObjectModifiedMessage(_character.Client, sameItem);
            }
            else // Add
            {
                item.Stack = quantity;
                if (_items.ContainsKey(item.Template.Id))
                    _items[item.Template.Id].Add(item);
                else
                    _items.Add(item.Template.Id, new List<BasePlayerItem>() { item });

                InventoryHandler.SendObjectAddedMessage(_character.Client, item);
            }
            InventoryHandler.SendInventoryWeightMessage(_character.Client);
        }

        public void RemoveItem(BasePlayerItem item, int quantity = 1)
        {
            var sameItem = GetItem(item.Template.Id, item.Position, item.Effects);

            if (sameItem != null) // Stack
            {
                _items[item.Template.Id].Remove(sameItem);

                if (sameItem.Stack - quantity <= 0)
                {
                    if (_items[item.Template.Id].Count == 0)
                        _items.Remove(item.Template.Id);
                    InventoryHandler.SendObjectDeletedMessage(_character.Client, item);
                }
                else
                {
                    sameItem.Stack -= quantity;
                    _items[item.Template.Id].Add(sameItem);
                    InventoryHandler.SendObjectModifiedMessage(_character.Client, sameItem);
                }
            }
            else // Add
            {
                if (_items.ContainsKey(item.Template.Id))
                {
                    _items[item.Template.Id].Remove(item);

                    if (_items[item.Template.Id].Count == 0)
                        _items.Remove(item.Template.Id);

                    InventoryHandler.SendObjectDeletedMessage(_character.Client, item);
                }
                else
                    _character.SendServerMessage($"Item {item.Template.Id} doesn't exist in your inventory !");
            }

            InventoryHandler.SendInventoryWeightMessage(_character.Client);
        }

        public void MoveItem(BasePlayerItem item, CharacterInventoryPositionEnum position)
        {
            if (!HasItem(item))
                return;

            if (position == item.Position)
                return;

            if (position != CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED)
                UnEquipedDouble(item);

            if (IsEquipPosition(position))
            {
                if (!CanEquip(item, position))
                    return;

                this.EquipedItem(item, position);
                _character.UpdateLook(item);
            }
            else
            {
                if (IsEquipment(item.Type))
                    _character.UpdateLook(item, true);

                var sameItem = GetItem(item.Template.Id, CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED, item.Effects);

                if (sameItem != null) // Stack
                    UnEquipedItem(item, sameItem, position, true);
                else
                    UnEquipedItem(item, null, position);
            }

            InventoryHandler.SendInventoryWeightMessage(_character.Client);
        }

        public void SetKamas(int amount)
        {
            Kamas += amount;
            InventoryHandler.SendKamasUpdateMessage(_character.Client, Kamas);
        }

        public void UnEquipedDouble(BasePlayerItem item)
        {
            if (item.Type == ItemTypeEnum.DOFUS || item.Type == ItemTypeEnum.TROPHÉE)
            {
                var itemDouble = GetItems().FirstOrDefault(x => x.IsEquiped() && x.Template.Id == item.Template.Id);
                if (itemDouble != null)
                {
                    MoveItem(itemDouble, CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED);
                    return;
                }
            }
            else
            {
                if (item.Type != ItemTypeEnum.ANNEAU)
                    return;

                var ring = GetItems().FirstOrDefault(x => x.IsEquiped() && x.Template.Id == item.Template.Id);
                if (ring != null)
                {
                    MoveItem(ring, CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED);
                    return;
                }
            }
        }

        public bool CanEquip(BasePlayerItem item, CharacterInventoryPositionEnum position)
        {
            if (_character.Level < item.Level)
                return false;

            if (item.IsEquiped())
                return false;

            if (_character.IsInFight && _character.Fight.State != FightStateEnum.Placement)
                return false;

            if (position == CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED)
                return true;

            if (!IsPossiblePosition(item, position))
                return false;

            var itemExit = GetItem(position);
            if (itemExit != null)
                MoveItem(itemExit, CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED);

            var weapon = GetItem(CharacterInventoryPositionEnum.ACCESSORY_POSITION_WEAPON);
            if (item.Type == ItemTypeEnum.BOUCLIER && weapon != null && weapon.Template.TwoHanded)
            {
                BasicHandler.SendTextInformationMessage(_character.Client, TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 78);
                MoveItem(weapon, CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED);
                return true;
            }

            var shield = GetItem(CharacterInventoryPositionEnum.ACCESSORY_POSITION_SHIELD);
            if (shield != null && (item.Template is WeaponTemplate && item.Template.TwoHanded))
            {
                BasicHandler.SendTextInformationMessage(_character.Client, TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 79);
                MoveItem(shield, CharacterInventoryPositionEnum.INVENTORY_POSITION_NOT_EQUIPED);
                return true;
            }
            return true;
        }

        public bool HasItem(BasePlayerItem item)
        {
            if (_items.ContainsKey(item.Template.Id))
            {
                if (_items[item.Template.Id].FirstOrDefault(x => x == item) != null)
                    return true;
                return false;
            }
            else
                return false;
        }

        public bool HasItem(int item)
        {
            if (_items.ContainsKey(item))
            {
                if (_items[item].FirstOrDefault(x => x.Id == item) != null)
                    return true;
                return false;
            }
            else
                return false;
        }

        public bool IsSameEffects(List<Effect> effects, List<Effect> secondEffects)
        {
            if (effects.Count == 0 && secondEffects.Count == 0)
                return true;
            for (int i = 0; i < effects.Count; i++)
            {
                if (effects[i].Value != secondEffects[i].Value)
                    return false;
            }
            return true;
        }

        public bool IsEquipPosition(CharacterInventoryPositionEnum position)
        {
            if (position >= CharacterInventoryPositionEnum.ACCESSORY_POSITION_AMULET
              && position <= CharacterInventoryPositionEnum.INVENTORY_POSITION_MOUNT)
                return true;
            return false;
        }

        public bool IsPossiblePosition(BasePlayerItem item, CharacterInventoryPositionEnum position)
        {
            #region Position
            switch (item.Type)
            {
                case ItemTypeEnum.CHAPEAU:
                    return position == CharacterInventoryPositionEnum.ACCESSORY_POSITION_HAT;
                case ItemTypeEnum.CAPE:
                case ItemTypeEnum.SAC_À_DOS:
                    return position == CharacterInventoryPositionEnum.ACCESSORY_POSITION_CAPE;
                case ItemTypeEnum.BOTTES:
                    return position == CharacterInventoryPositionEnum.ACCESSORY_POSITION_BOOTS;
                case ItemTypeEnum.ANNEAU:
                    return position == CharacterInventoryPositionEnum.INVENTORY_POSITION_RING_LEFT ||
                           position == CharacterInventoryPositionEnum.INVENTORY_POSITION_RING_RIGHT;
                case ItemTypeEnum.AMULETTE:
                    return position == CharacterInventoryPositionEnum.ACCESSORY_POSITION_AMULET;
                case ItemTypeEnum.CEINTURE:
                    return position == CharacterInventoryPositionEnum.ACCESSORY_POSITION_BELT;
                case ItemTypeEnum.BOUCLIER:
                    return position == CharacterInventoryPositionEnum.ACCESSORY_POSITION_SHIELD;
                case ItemTypeEnum.DOFUS:
                    return position == CharacterInventoryPositionEnum.INVENTORY_POSITION_DOFUS_1 ||
                           position == CharacterInventoryPositionEnum.INVENTORY_POSITION_DOFUS_2 ||
                           position == CharacterInventoryPositionEnum.INVENTORY_POSITION_DOFUS_3 ||
                           position == CharacterInventoryPositionEnum.INVENTORY_POSITION_DOFUS_4 ||
                           position == CharacterInventoryPositionEnum.INVENTORY_POSITION_DOFUS_5 ||
                           position == CharacterInventoryPositionEnum.INVENTORY_POSITION_DOFUS_6;
                case ItemTypeEnum.ARC:
                case ItemTypeEnum.DAGUE:
                case ItemTypeEnum.MARTEAU:
                case ItemTypeEnum.BAGUETTE:
                case ItemTypeEnum.BÂTON:
                case ItemTypeEnum.ÉPÉE:
                case ItemTypeEnum.HACHE:
                case ItemTypeEnum.FAUX:
                    return position == CharacterInventoryPositionEnum.ACCESSORY_POSITION_WEAPON;
                default:
                    return false;
            }
            #endregion
        }

        public bool IsEquipment(ItemTypeEnum type)
        {
            return type >= ItemTypeEnum.AMULETTE && type <= ItemTypeEnum.BOTTES || type >= ItemTypeEnum.CHAPEAU
                 && type <= ItemTypeEnum.HACHE || type == ItemTypeEnum.PIOCHE || type == ItemTypeEnum.DOFUS
                 || type == ItemTypeEnum.BOUCLIER;
        }

        private void EquipedItem(BasePlayerItem item, CharacterInventoryPositionEnum position)
        {
            if (item.Stack > 1)
            {
                var newItem = item.Clone();
                newItem.Id = ItemManager.Instance.GenerateId();
                newItem.Stack = item.Stack - 1;
                _items[item.Template.Id].Remove(item);
                item.Stack = 1;
                InventoryHandler.SendObjectModifiedMessage(_character.Client, item);
                _items[item.Template.Id].Add(newItem);
                InventoryHandler.SendObjectAddedMessage(_character.Client, newItem);
                item.Position = position;
                _items[item.Template.Id].Add(item);
                InventoryHandler.SendObjectMovementMessage(_character.Client, item);
            }
            else
            {
                _items[item.Template.Id].Remove(item);
                item.Position = position;
                _items[item.Template.Id].Add(item);
                InventoryHandler.SendObjectMovementMessage(_character.Client, item);
            }
            ApplyItemEffects(item, true);
            ApplyItemSetEffects(item);
        }

        private void UnEquipedItem(BasePlayerItem item, BasePlayerItem sameItem, CharacterInventoryPositionEnum position, bool isStacked = false)
        {
            if (isStacked)
            {
                _items[item.Template.Id].Remove(item);
                InventoryHandler.SendObjectDeletedMessage(_character.Client, item);
                _items[item.Template.Id].Remove(sameItem);
                item.Position = position;
                sameItem.Stack += 1;
                _items[item.Template.Id].Add(sameItem);
                InventoryHandler.SendObjectModifiedMessage(_character.Client, sameItem);
            }
            else
            {
                _items[item.Template.Id].Remove(item);
                item.Position = position;
                _items[item.Template.Id].Add(item);
                InventoryHandler.SendObjectMovementMessage(_character.Client, item);
            }
            ApplyItemEffects(item, false);
            ApplyItemSetEffects(item);
        }

        private void ApplyItemEffects(BasePlayerItem item, bool apply, bool send = true)
        {
            foreach (var effect in item.Effects)
            {
                if (ItemEffectHandler.EffectsRelations.ContainsKey(effect.Id))
                {
                    var stats = ItemEffectHandler.EffectsRelations[effect.Id];
                    if (apply)
                        _character.Stats[stats].Equiped += (short)effect.Value;
                    else
                        _character.Stats[stats].Equiped -= (short)effect.Value;
                }
                else
                    Logs.Logger.WriteError($"{effect.Id} doesn't exist !");
            }

            if (send)
                _character.RefreshStats();
        }

        private void ApplyItemEffects(List<Effect> effects, bool apply, bool send = true)
        {
            foreach (var effect in effects)
            {
                if (ItemEffectHandler.EffectsRelations.ContainsKey(effect.Id))
                {
                    var stats = ItemEffectHandler.EffectsRelations[effect.Id];
                    if (apply)
                        _character.Stats[stats].Equiped += (short)effect.Value;
                    else
                        _character.Stats[stats].Equiped -= (short)effect.Value;
                }
                else
                    Logs.Logger.WriteError($"{effect.Id} doesn't exist !");
            }

            if (send)
                _character.RefreshStats();
        }

        private void ApplyItemSetEffects(BasePlayerItem item, bool send = true)
        {
            List<Effect> effects = null;
            int effectsCount = 0;

            if (item.EffectSets.Count <= 0)
                return;

            if (_itemSetsEffects.ContainsKey(item.Template.ItemSetId))
            {
                effects = _itemSetsEffects[item.Template.ItemSetId];
                ApplyItemEffects(effects, false, send);
                _itemSetsEffects.Remove(item.Template.ItemSetId);
            }

            var itemSetsCount = GetItemSetsEquiped(item.Template.ItemSetId).Count();
            if (itemSetsCount <= 1)
                return;

            if (itemSetsCount >= (item.EffectSets.Count - 1))
                effectsCount = itemSetsCount - 2;
            else
                effectsCount = (itemSetsCount - (item.EffectSets.Count - 1)) + 2;

            effects = item.EffectSets[effectsCount];
            _itemSetsEffects.Add(item.Template.ItemSetId, effects);
            ApplyItemEffects(effects, true, send);
        }
    }
}
