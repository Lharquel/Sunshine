﻿using Sunshine.MySql.Database.Auth.Accounts;
using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.Protocol.Utils;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Actors.Characters;
using Sunshine.WorldServer.Game.Actors.Characters.Inventory;
using Sunshine.WorldServer.Game.Actors.Characters.Jobs;
using Sunshine.WorldServer.Game.Actors.Characters.Shortcuts;
using Sunshine.WorldServer.Game.Actors.Characters.Spells;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Actors.Monsters;
using Sunshine.WorldServer.Game.Actors.Npcs;
using Sunshine.WorldServer.Game.Actors.Stats;
using Sunshine.WorldServer.Game.BidsHouse;
using Sunshine.WorldServer.Game.Dialogs;
using Sunshine.WorldServer.Game.Exchanges;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.WorldServer.Game.Fights.Types;
using Sunshine.WorldServer.Game.Items;
using Sunshine.WorldServer.Game.Maps;
using Sunshine.WorldServer.Game.Maps.Triggers;
using Sunshine.WorldServer.Game.Parties;
using Sunshine.WorldServer.Handlers;
using Sunshine.WorldServer.Handlers.Actions;
using Sunshine.WorldServer.Handlers.Basic;
using Sunshine.WorldServer.Handlers.Characters;
using Sunshine.WorldServer.Handlers.Characters.Inventory;
using Sunshine.WorldServer.Handlers.Context;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using Sunshine.WorldServer.Handlers.Context.RolePlay.Party;
using System;
using System.Collections.Generic;
using System.Runtime.Versioning;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Game.Actors.Characters.Quests;

namespace Sunshine.WorldServer.Game.Characters
{
    public class Character : RolePlayActor
    {
        public WorldClient Client { get; set; }
        public CharacterRecord Record { get; set; }
        public StatsFields Stats { get; set; }
        public CharacterAlignment Alignment { get; set; }
        public SpellInventory Spells { get; set; }
        public ShortcutBar Shortcuts { get; set; }
        public Inventory Inventory { get; set; }
        public BidHouseBag BidHouseBag { get; set; }
        public JobsCollection Jobs { get; set; }
        public QuestsCollection Quests { get; set; }

        public Character(CharacterRecord record)
        {
            Record = record;
            Account = AccountManager.Instance.GetAccount(record.Id);
            this.LoadRecord();
        }

        public Account Account { get; set; }

        public override int Id { get { return Record.Id; } }

        public string Name
        {
            get { return Record.Name; }
            set { Record.Name = value; }
        }

        public short Level
        {
            get { return Record.Level; }
            set { Record.Level = value; }
        }

        public int Breed
        {
            get { return Record.Breed; }
            set { Record.Breed = value; }
        }

        public bool Sex
        {
            get { return Record.Sex; }
            set { Record.Sex = value; }
        }

        public string CustomLook
        {
            get { return Record.CustomLook; }
            set { Record.CustomLook = value; }
        }

        public long Experience
        {
            get { return Record.Experience; }
            set
            {
                Record.Experience = value;
                if (Level >= 200)
                    return;
                var nextLevel = ExperienceManager.Instance.GetNextLevelExperienceFloor(value);
                if (Level < nextLevel)
                {
                    int levelDiff = nextLevel - Level;
                    LevelUp((byte)levelDiff);
                }
            }
        }

        public long ExperienceLevelFloor
        {
            get { return ExperienceManager.Instance.GetExperienceLevelFloor(Level); }
        }

        public long ExperienceNextLevelFloor
        {
            get { return ExperienceManager.Instance.GetNextExperienceLevelFloor(Level); }
        }

        public int Direction
        {
            get { return Record.Direction; }
            set { Record.Direction = value; }
        }

        public int StatsPoints
        {
            get { return Record.StatsPoints; }
            set { Record.StatsPoints = value; }
        }

        public int SpellsPoints
        {
            get { return Record.SpellsPoints; }
            set { Record.SpellsPoints = value; }
        }

        public List<Map> Zaaps { get; set; }

        public Map Map { get; set; }

        public Cell Cell { get; set; }

        public ActorLook Look { get; set; }

        public CharacterBaseInformations GetCharacterBaseInformations
            => new CharacterBaseInformations(Id, (byte)Level, Name, Look.GetEntityLook(), (sbyte)Breed, Sex);

        public ActorAlignmentInformations GetActorAlignmentInformations
            => new ActorAlignmentInformations((sbyte)Alignment.Side, Alignment.Value, Alignment.Grade, Alignment.Dishonor, Id + Level);

        public ActorExtendedAlignmentInformations GetActorExtendedAlignmentInformations
            =>new ActorExtendedAlignmentInformations((sbyte)Alignment.Side, Alignment.Value, Alignment.Grade, Alignment.Dishonor, Id + Level, Alignment.Honor, Alignment.HonorGradeFloor, Alignment.HonorNextGradeFloor, Alignment.Enabled);

        public override GameRolePlayActorInformations GetGameRolePlayActorInformations
            =>new GameRolePlayCharacterInformations(Id, Look.GetEntityLook(), new EntityDispositionInformations(Cell.Id, (sbyte)Direction), Name,
               new HumanInformations(new List<EntityLook>(), 0, 0,
               new ActorRestrictionsInformations(false, false, false, false, false, false, false, false, false, false, false, false, false, true, true, true, false, false, false, false, false), 0, ""), GetActorAlignmentInformations);

        public CharacterCharacteristicsInformations GetCharacterCharacteristicsInformations
            => new CharacterCharacteristicsInformations(Experience, ExperienceLevelFloor, ExperienceNextLevelFloor, (int)Inventory.Kamas, StatsPoints, SpellsPoints,
                GetActorExtendedAlignmentInformations, Stats.Health.Total, Stats.Health.TotalMax, 1000, 1000, Stats.AP.TotalMax, Stats.MP.TotalMax,
                Stats[StatsEnum.Initiative], Stats[StatsEnum.Prospecting], Stats[StatsEnum.AP], Stats[StatsEnum.MP], Stats[StatsEnum.Strength],
                Stats[StatsEnum.Vitality], Stats[StatsEnum.Wisdom], Stats[StatsEnum.Chance], Stats[StatsEnum.Agility], Stats[StatsEnum.Intelligence],
                Stats[StatsEnum.Range], Stats[StatsEnum.SummonLimit], Stats[StatsEnum.DamageReflection], Stats[StatsEnum.CriticalHit], 0, Stats[StatsEnum.CriticalMiss],
                Stats[StatsEnum.HealBonus], Stats[StatsEnum.DamageBonus], Stats[StatsEnum.WeaponDamageBonus], Stats[StatsEnum.DamageBonusPercent], Stats[StatsEnum.TrapBonus],
                Stats[StatsEnum.TrapBonusPercent], Stats[StatsEnum.PermanentDamagePercent], Stats[StatsEnum.TackleBlock], Stats[StatsEnum.TackleEvade], Stats[StatsEnum.APAttack],
                Stats[StatsEnum.MPAttack], Stats[StatsEnum.PushDamageBonus], Stats[StatsEnum.CriticalDamageBonus], Stats[StatsEnum.NeutralDamageBonus], Stats[StatsEnum.EarthDamageBonus],
                Stats[StatsEnum.WaterDamageBonus], Stats[StatsEnum.AirDamageBonus], Stats[StatsEnum.FireDamageBonus], Stats[StatsEnum.DodgeAPProbability], Stats[StatsEnum.DodgeMPProbability],
                Stats[StatsEnum.NeutralResistPercent], Stats[StatsEnum.EarthResistPercent], Stats[StatsEnum.WaterResistPercent], Stats[StatsEnum.AirResistPercent], Stats[StatsEnum.FireResistPercent],
                Stats[StatsEnum.NeutralElementReduction], Stats[StatsEnum.EarthElementReduction], Stats[StatsEnum.WaterElementReduction], Stats[StatsEnum.AirElementReduction], Stats[StatsEnum.FireElementReduction],
                Stats[StatsEnum.PushDamageReduction], Stats[StatsEnum.CriticalDamageReduction], Stats[StatsEnum.PvpNeutralResistPercent], Stats[StatsEnum.PvpEarthResistPercent], Stats[StatsEnum.PvpWaterResistPercent],
                Stats[StatsEnum.PvpAirResistPercent], Stats[StatsEnum.PvpFireResistPercent], Stats[StatsEnum.PvpNeutralElementReduction], Stats[StatsEnum.PvpEarthElementReduction], Stats[StatsEnum.PvpWaterElementReduction],
                Stats[StatsEnum.PvpAirElementReduction], Stats[StatsEnum.PvpFireElementReduction], new System.Collections.Generic.List<CharacterSpellModification>());

        public PartyMemberInformations GetPartyMemberInformations
            => new PartyMemberInformations(Id, (byte)Level, Name, Look.GetEntityLook(), Stats.Health.Total, Stats.Health.TotalMax, (short)Stats.Prospecting.Total, 1, (short)Stats.Initiative.Total, Alignment.Enabled, (sbyte)Alignment.Side);

        public PartyInvitationMemberInformations GetPartyInvitationMemberInformations
            => new PartyInvitationMemberInformations(Id, (byte)Level, Name, Look.GetEntityLook(), (sbyte)Breed, Sex, 0, 0, Map.Id);

        public PartyGuestInformations GetPartyGuestInformations
            => new PartyGuestInformations(Id, Party.Leader.Id, Name, Look.GetEntityLook());

        public Party Party { get; set; }

        public Fight Fight { get; set; }

        public CharacterFighter Fighter { get; set; }

        public Trade Trade { get; set; }

        public IDialog Dialog { get; set; }

        public bool IsInParty { get { return Party != null; } }

        public bool IsPartyLeader { get { return !IsInParty ? false : Party.Leader == this; } }

        public bool IsInFight { get { return Fight != null; } }

        public bool IsInTrade { get { return Trade != null; } }

        public bool IsInDialog { get { return Dialog != null; } }

        public bool IsInWorld { get { return Client != null; } }

        private bool _isInRegen;

        private DateTime _regenStartTime;

        private byte _regenRate;

        public bool HasZaap(int map)
        {
            return Zaaps.Any(x => x.Id == map);
        }

        public void DiscoverZaap(Map map)
        {
            this.Zaaps.Add(map);
            this.SendInformationMessage(TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 24);
        }

        public void EnterMap(Map map)
        {
            map.EnterActor(this);
        }

        public void LeaveMap(Map map)
        {
            map.LeaveActor(this);
        }

        public void Teleport(int mapId, short cellId)
        {
            Map.LeaveActor(this);
            Record.MapId = mapId;
            Record.CellId = cellId;
            Map = MapManager.Instance.GetMap(this);
            ContextRoleplayHandler.SendCurrentMapMessage(Client, mapId);
        }

        public void EnterWorld()
        {
            this.SendInformationMessage(TextInformationTypeEnum.TEXT_INFORMATION_ERROR, 89);
            this.BidHouseBag.LoadBag();
            this.StartRegen();
        }

        public void SetParty(PartyTypeEnum party, Character leader)
        {
            switch (party)
            {
                case PartyTypeEnum.PARTY_TYPE_CLASSICAL:
                    Party = PartyManager.Instance.SetParty(leader);
                    Party.AddMember(leader);
                    break;
                case PartyTypeEnum.PARTY_TYPE_DUNGEON:
                    break;
            }
        }

        public void SetInvitation(PartyTypeEnum party, Character guest)
        {
            switch (party)
            {
                case PartyTypeEnum.PARTY_TYPE_CLASSICAL:
                    PartyHandler.SendPartyInvitationMessage(guest.Client, Party, this);
                    Party.Sender = this;
                    Party.AddGuest(guest);
                    break;
                case PartyTypeEnum.PARTY_TYPE_DUNGEON:
                    break;
            }
        }

        public void LeaveParty(PartyTypeEnum party)
        {
            switch (party)
            {
                case PartyTypeEnum.PARTY_TYPE_CLASSICAL:
                    Party.RemoveMember(this);
                    break;
                case PartyTypeEnum.PARTY_TYPE_DUNGEON:
                    break;
            }
        }

        public void SetFight(FightTypeEnum fight, object argv = null)
        {
            switch (fight)
            {
                case FightTypeEnum.FIGHT_TYPE_PvM:
                    Fight = argv != null ? (FightPvM)argv : FightManager.Instance.SetFight<FightPvM>(this);                   
                    break;

                case FightTypeEnum.FIGHT_TYPE_CHALLENGE:
                    Fight = argv != null ? (FightDuel)argv : FightManager.Instance.SetFight<FightDuel>(this);
                    break;

                case FightTypeEnum.FIGHT_TYPE_AGRESSION:
                    Fight = argv != null ? (FightAgression)argv : FightManager.Instance.SetFight<FightAgression>(this);
                    break;
            }
            
        }

        public void SetFightRequest(FightTypeEnum fight, Character target)
        {
            switch(fight)
            {
                case FightTypeEnum.FIGHT_TYPE_CHALLENGE:
                    ContextRoleplayHandler.SendGameRolePlayPlayerFightFriendlyRequestedMessage(Client, target, this, target);
                    ContextRoleplayHandler.SendGameRolePlayPlayerFightFriendlyRequestedMessage(target.Client, this, this, target);
                    SetFight(fight);
                    target.SetFight(fight, Fight);
                    break;
            }
        }

        public void LeaveFight(FightTypeEnum fight)
        {
            switch (fight)
            {
                case FightTypeEnum.FIGHT_TYPE_PvM:
                    Fight.LeaveFighter(Client, Fighter);
                    break;

                case FightTypeEnum.FIGHT_TYPE_CHALLENGE:
                    Fight.LeaveFighter(Client, Fighter);
                    break;

                case FightTypeEnum.FIGHT_TYPE_AGRESSION:
                    Fight.LeaveFighter(Client, Fighter);
                    break;                   
            }
            Fight = null;
        }

        public void SetTrade(ExchangeTypeEnum exchange, object argv = null, object target = null)
        {
            switch (exchange)
            {
                case ExchangeTypeEnum.PLAYER_TRADE:
                    Trade = argv != null ? (PlayerTrade)argv : new PlayerTrade(this, (Character)target);
                    break;

                case ExchangeTypeEnum.NPC_TRADE:
                    Trade = argv != null ? (NpcTrade)argv : new NpcTrade(this, (Npc)target);
                    break;

                case ExchangeTypeEnum.CRAFT:
                    Trade = argv != null ? (CraftTrade)argv : new CraftTrade(this, (Character)target);
                    break;                
            }
        }

        public void SetTradeRequest(ExchangeTypeEnum exchange, Character target)
        {
            switch (exchange)
            {
                case ExchangeTypeEnum.PLAYER_TRADE:
                    InventoryHandler.SendExchangeRequestedTradeMessage(Client, exchange, this, target);
                    InventoryHandler.SendExchangeRequestedTradeMessage(target.Client, exchange, this, target);
                    this.SetTrade(exchange, null, target);
                    target.SetTrade(exchange, Trade);
                    break;
            }
        }

        public void LeaveExchange(ExchangeTypeEnum exchange)
        {
            switch (exchange)
            {
                case ExchangeTypeEnum.PLAYER_TRADE:
                    this.Trade.Cancel();
                    break;

                case ExchangeTypeEnum.NPC_TRADE:
                    this.Trade.Cancel();
                    break;
            }
        }

        public void AddExperience(long amount)
        {
            Experience += amount;
        }

        public void AddHonor(ushort amount)
        {
            Alignment.Honor += amount;
        }

        public void SubHonor(ushort amount)
        {
            if (Alignment.Honor - amount < 0)
                Alignment.Honor = 0;
            else
                Alignment.Honor -= amount;
        }

        public void AddDishonor(ushort amount)
        {
            Alignment.Dishonor += amount;
        }

        public void SubDishonor(ushort amount)
        {
            if (Alignment.Dishonor - amount < 0)
                Alignment.Dishonor = 0;
            else
                Alignment.Dishonor -= amount;
        }

        public void LevelUp(byte levelAdded)
        {
            if (Level >= 200)
                return;

            if (Level + levelAdded > 200)
                levelAdded = 199;

            OnLevelChanged(Level, Level += levelAdded);
        }

        private void OnLevelChanged(int lastLevel, int newLevel)
        {
            Experience = ExperienceLevelFloor;
            Stats[StatsEnum.Health].Base = Stats.Health.TotalMax;
            StatsPoints += 5 * (newLevel - lastLevel);
            SpellsPoints += (newLevel - lastLevel);

            if (newLevel >= 100 && lastLevel < 100)
                Stats[StatsEnum.AP].Base ++;

            Shortcuts.AddNextShortcuts(ShortcutBarEnum.SPELL);
            CharacterHandler.SendCharacterLevelUpMessage(Client, (byte)Level);
            Map.Clients.ForEach(x => CharacterHandler.SendCharacterLevelUpInformationMessage(x, this, (byte)Level));            
            RefreshStats();
        }

        public void TogglePvPMode(bool state)
        {
            Alignment.Enabled = state;
            Map.Refresh(this);
            RefreshStats();
        }

        public void ChangeAlignementSide(AlignmentSideEnum side)
        {
            Alignment.Side = side;
            Alignment.Honor = 0;
            TogglePvPMode(false);
        }

        public void StartRegen(byte regenRate = 20)
        {
            if (_isInRegen)
                StopRegen();
            _isInRegen = true;
            _regenRate = regenRate;
            _regenStartTime = DateTime.Now;
            CharacterHandler.SendLifePointsRegenBeginMessage(Client, regenRate);
        }

        public void StopRegen()
        {
            if(_isInRegen)
            {
                int lifePointsGained = (int)Math.Floor((DateTime.Now - _regenStartTime).TotalSeconds / (_regenRate / 10f));
                if (Stats.Health.Total + lifePointsGained > Stats.Health.TotalMax)
                    lifePointsGained = Stats.Health.TotalMax - Stats.Health.Total;
                Stats.Health.Taken -= (short)lifePointsGained;
                CharacterHandler.SendLifePointsRegenEndMessage(Client, lifePointsGained);
                _isInRegen = false;   //je t'aime +++          
                //CharacterHandler.SendUpdateLifePointsMessage(Client); Update for Party
            }
        }

        public void RefreshStats()
        {
            CharacterHandler.SendCharacterStatsListMessage(Client);
        }

        public void RefreshActor()
        {
            Map.Refresh(this);
        }

        public void UpdateLook(bool isCustomLook = false)
        {
            Look.SetLook(isCustomLook);
            OnLookRefreshed();
        }

        public void UpdateLook(BasePlayerItem item, bool isRemoved = false)
        {
            if (item.AppearanceId != 0)
            {
                if (isRemoved)
                    Look.RemoveSkin(item.AppearanceId);
                else
                    Look.AddSkin(item.AppearanceId);
                OnLookRefreshed();
            }
        }

        public void OnLookRefreshed()
        {
            if (IsInFight && !Fighter.IsDead)
                ContextHandler.SendGameContextRefreshEntityLookMessage(Fight.Clients, Fighter.Id, Look.GetEntityLook());
            else
                ContextHandler.SendGameContextRefreshEntityLookMessage(Map.Clients, Id, Look.GetEntityLook());
        }

        public void StartMove(IEnumerable<short> keyMovements)
        {
            if (IsInFight)
                Fighter.StartMove(keyMovements);
            else
            {
                Cell.Id = (short)(keyMovements.Last() & 4095);
                Direction = keyMovements.Last() >> 12;
                ContextHandler.SendGameMapMovementMessage(Map.Clients, Id, keyMovements);
            }
        }

        public void StopMove()
        {
            BasicHandler.SendBasicNoOperationMessage(Client);
            var monstersGroup = Map.RolePlayActors.Where(x => x is MonsterGroup && (x as MonsterGroup).Map.Record.Id == Map.Id && 
                                                         x.GetGameRolePlayActorInformations.disposition.cellId == Cell.Id);

            var monsters = monstersGroup.Select(x => (x as MonsterGroup).Monsters).FirstOrDefault();       
            if (monsters != null && monsters.Count > 0)
            {
                if (Map.RedCells == null || Map.BlueCells == null 
                    || Map.BlueCells.Count <= 0 || Map.RedCells.Count <= 0)
                {
                    SendServerMessage("This map doesn't have fight cells.", Color.Red);
                    return;
                }

                SetFight(FightTypeEnum.FIGHT_TYPE_PvM);
                monsters.ForEach(x => Fight.AddFighter(new MonsterFighter(x, Fight)));
                Fight.AddFighter(Fighter = new CharacterFighter(this), true);
                Map.LeaveActor(monstersGroup.FirstOrDefault());
                Map.RolePlayActors.Remove(monstersGroup.FirstOrDefault());
            }
            else
            {
                var trigger = Map.Triggers.FirstOrDefault(x => x.Map == Map.Id && x.Cell == Cell.Id);
                if (trigger != null)
                    TriggerDispatcher.Dispatch(Client, trigger);
            }          
        }

        public FighterRefusedReasonEnum CanRequestFight(Character target)
        {
            if (!target.IsInWorld || target.IsInFight)
                return FighterRefusedReasonEnum.OPPONENT_OCCUPIED;

            if (!IsInWorld || IsInFight)
                return FighterRefusedReasonEnum.IM_OCCUPIED;

            if (target == this)
                return FighterRefusedReasonEnum.FIGHT_MYSELF;

            if (target.Map != Map || !Map.AllowChallenge)
                return FighterRefusedReasonEnum.WRONG_MAP;

            return FighterRefusedReasonEnum.FIGHTER_ACCEPTED;
        }

        public FighterRefusedReasonEnum CanAgress(Character target)
        {
            if (target == this)
                return FighterRefusedReasonEnum.FIGHT_MYSELF;

            if (!target.Alignment.Enabled || !Alignment.Enabled)
                return FighterRefusedReasonEnum.INSUFFICIENT_RIGHTS;

            if (!target.IsInWorld || target.IsInFight)
                return FighterRefusedReasonEnum.OPPONENT_OCCUPIED;

            if (!IsInWorld || IsInFight)
                return FighterRefusedReasonEnum.IM_OCCUPIED;

            if (Alignment.Side <= AlignmentSideEnum.ALIGNMENT_NEUTRAL || target.Alignment.Side <= AlignmentSideEnum.ALIGNMENT_NEUTRAL)
                return FighterRefusedReasonEnum.WRONG_ALIGNMENT;

            if (target.Alignment.Side == Alignment.Side)
                return FighterRefusedReasonEnum.WRONG_ALIGNMENT;

            if (target.Map != Map /*|| !Map.AllowAggression)*/)
                return FighterRefusedReasonEnum.WRONG_MAP;

            if (target.Client.Ip == Client.Ip)
                return FighterRefusedReasonEnum.MULTIACCOUNT_NOT_ALLOWED;

            if (Level - target.Level > 20)
                return FighterRefusedReasonEnum.INSUFFICIENT_RIGHTS;

            return FighterRefusedReasonEnum.FIGHTER_ACCEPTED;
        }

        public void SendServerMessage(string message)
        {
            BasicHandler.SendTextInformationMessage(Client, TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 0, new object[]{ message });
        }

        public void SendServerMessage(string message, Color color)
        {
            SendServerMessage(string.Format("<font color=\"#{0}\">{1}</font>", color.ToArgb().ToString("X"), message));
        }

        public void SendInformationMessage(TextInformationTypeEnum msgType, short msgId, params object[] parameters)
        {
            BasicHandler.SendTextInformationMessage(Client, msgType, msgId, parameters);
        }

        public void SendNotificationByServerMessage(string message, NotificationEnum notification, bool isForced = true)
        {
            BasicHandler.SendNotificationByServerMessage(Client, message, notification, isForced);
        }

        public void LogOut()
        {        
            if (this.IsInParty)
                this.LeaveParty(Party.Type);
            if (this.IsInFight)
                this.LeaveFight(Fight.Type);
            if (this.IsInTrade)
                this.LeaveExchange(Trade.Type);
            this.LeaveMap(Map);
            Save();
        }

        public void LoadRecord()
        {
            Look = new ActorLook(this);
            Stats = new StatsFields(this);
            Alignment = new CharacterAlignment(this);
            Spells = new SpellInventory(this);
            Shortcuts = new ShortcutBar(this);
            Inventory = new Inventory(this);
            BidHouseBag = new BidHouseBag(this);
            Jobs = new JobsCollection(this);
            Quests = new QuestsCollection(this);
            Map = MapManager.Instance.GetMap(this);
            Cell = MapManager.Instance.GetCell(this);
            Zaaps = Record.Zaaps == null || Record.Zaaps == "" ? new List<Map>() : 
                    Record.Zaaps.Split(',').Select(x => MapManager.Instance.GetMap(x)).ToList();
        }

        private void Save()
        {
            Client = null;
            CharacterManager.Instance.Characters.Remove(Id);
            CharacterManager.Instance.Characters.Add(Id, this);       
        }
    }
}
