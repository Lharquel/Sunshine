﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.Protocol.Messages;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Characters.Jobs
{
    public class JobsCollection
    {
        private Character _character;
        private List<CharacterJobRecord> _jobs;

        public JobsCollection(Character character)
        {
            _character = character;
            _jobs = CharacterManager.Instance.GetCharacterJobs(character.Id);
        }

        public IEnumerable<CharacterJobRecord> GetJobs()
        {
            return _jobs;
        }

        public CharacterJobRecord GetJob(sbyte id)
        {
            return _jobs.FirstOrDefault(x => x.Job == id);
        }

        public IEnumerable<JobCrafterDirectorySettings> GetJobsCraftersDirectorySettings()
        {
            List<JobCrafterDirectorySettings> jobsCrafterSettings = new List<JobCrafterDirectorySettings>();
            foreach (var job in _jobs)
            {
                sbyte level = ExperienceManager.Instance.GetJobLevelExperienceFloor(job.Experience);
                jobsCrafterSettings.Add(new JobCrafterDirectorySettings(job.Job, JobManager.Instance.GetJobSlot(level), 0));
            }
            return jobsCrafterSettings;
        }

        public IEnumerable<JobDescription> GetJobsDescriptions()
        {
            List<JobDescription> jobsDescriptions = new List<JobDescription>();
            foreach (var job in _jobs)
                jobsDescriptions.Add(new JobDescription(job.Job, JobManager.Instance.GetSkillActionDescription(job.Job, 
                                                        ExperienceManager.Instance.GetJobLevelExperienceFloor(job.Experience))));
            return jobsDescriptions;
        }

        public IEnumerable<JobExperience> GetJobsExperiences()
        {
            List<JobExperience> jobsExperiences = new List<JobExperience>();
            foreach(var job in _jobs)
            {
                sbyte jobLevel = ExperienceManager.Instance.GetJobLevelExperienceFloor(job.Experience);
                double jobExp = job.Experience;
                double jobExpTotal = ExperienceManager.Instance.GetJobExperienceLevelFloor(jobLevel);
                double jobNextExp = ExperienceManager.Instance.GetJobNextExperienceLevelFloor(jobLevel);
                jobsExperiences.Add(new JobExperience(job.Job, jobLevel, jobExp, jobExpTotal, jobNextExp));
            }
            return jobsExperiences;
        }

        public int GetJobsLevelTotal()
        {
            return _jobs.Sum(x => ExperienceManager.Instance.GetJobLevelExperienceFloor(x.Experience));
        }

        public int GetJobsCount(int level)
        {
            return _jobs.Where(x => ExperienceManager.Instance.GetJobLevelExperienceFloor(x.Experience) >= level).Count();
        }

        public void AddJob(sbyte job)
        {
            _jobs.Add(new CharacterJobRecord { OwnerId = _character.Id, Job = job, Experience = 0 });
        }

        public void AddExperience(sbyte job, long amount)
        {
            var jobRecord = GetJob(job);
            sbyte jobLevel = ExperienceManager.Instance.GetJobLevelExperienceFloor(jobRecord.Experience);
            double jobNextExp = ExperienceManager.Instance.GetNextExperienceLevelFloor(jobLevel);
            double jobExp = ExperienceManager.Instance.GetJobExperienceLevelFloor(jobLevel);

            if (jobLevel >= 100)
                return;

            jobRecord.Experience += amount;
            sbyte nextLevel = ExperienceManager.Instance.GetNextJobLevelExperienceFloor(jobRecord.Experience);
            if (jobLevel < nextLevel)
            {
                double jobExpTotal = ExperienceManager.Instance.GetJobExperienceLevelFloor(nextLevel);
                jobNextExp = ExperienceManager.Instance.GetJobNextExperienceLevelFloor(nextLevel);

                _character.Client.Send(new JobLevelUpMessage(nextLevel, new JobDescription(job, JobManager.Instance.GetSkillActionDescription(jobRecord.Job,
                    ExperienceManager.Instance.GetJobLevelExperienceFloor(jobRecord.Experience)))));
                
                _character.Client.Send(new JobExperienceUpdateMessage(new JobExperience(job, nextLevel, jobRecord.Experience, jobExpTotal, jobNextExp)));
            }
        }
    }
}
