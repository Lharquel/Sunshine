﻿using Sunshine.Protocol.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Utils;
using Sunshine.Protocol.Enums;
using System.Drawing;
using System.Globalization;

namespace Sunshine.WorldServer.Game.Actors
{
    public class EntityManager : Singleton<EntityManager>
    {
        public string BuildEntityLook(int breedId, bool sex, List<int> colors)
        {        
            string breedLook = BreedManager.Instance.GetLook(breedId, sex);
            var baseColors = sex ? BreedManager.Instance.BreedColors[breedId].FemaleColors.ConvertAll(x => (int)x)
                             : BreedManager.Instance.BreedColors[breedId].MaleColors.ConvertAll(x => (int)x);
            StringBuilder buildColors = new StringBuilder();
            for (int i = 0; i < colors.Count - 1; i++)
            {
                colors[i] = colors[i] == -1 ? baseColors[i] : colors[i];
                buildColors.Append(i == 4 ? $"{i + 1}={colors[i]}" : $"{i + 1}={colors[i]},");
            }
            var index = breedLook.IndexOf("||");
            string beginLook = breedLook.Remove(index, breedLook.Length - index);
            string endLook = breedLook.Remove(0, index).Trim('|');
            return $"{beginLook}|{buildColors}|{endLook}";
        }

        public string ParseEntityLook(EntityLook look)
        {
            var result = new StringBuilder();

            result.Append("{");

            var missingBars = 0;

            result.Append(look.bonesId);

            if (look.skins == null || !look.skins.Any())
                missingBars++;
            else
            {
                result.Append("|".ConcatCopy(missingBars + 1));
                missingBars = 0;
                result.Append(string.Join(",", look.skins));
            }

            if (look.indexedColors == null || !look.indexedColors.Any())
                missingBars++;
            else
            {
                result.Append("|".ConcatCopy(missingBars + 1));
                missingBars = 0;
                Dictionary<int, int> colors = new Dictionary<int, int>();
                var indexColors = look.indexedColors.ToList();
                for (int i = 0; i < indexColors.Count; i++)
                    colors.Add(i + 1, indexColors[i]);
                result.Append(string.Join(",", from entry in colors
                                               select entry.Key + "=" + entry.Value));
            }
            if (look.scales == null || !look.scales.Any())
                missingBars++;
            else
            {
                result.Append("|".ConcatCopy(missingBars + 1));
                missingBars = 0;
                result.Append(string.Join(",", look.scales));
            }

            if (look.subentities == null || !look.subentities.Any())
                missingBars++;
            else
            {
                result.Append("|".ConcatCopy(missingBars + 1));
                result.Append(string.Join(",", look.subentities.Select(entry => entry)));
            }

            result.Append("}");

            return result.ToString();
        }

        public EntityLook GetEntitylook(string actualLook)
        {
            if (string.IsNullOrEmpty(actualLook) || actualLook[0] != '{')
                throw new Exception("Incorrect EntityLook format : " + actualLook);
            var cursorPos = 1;
            var separatorPos = actualLook.IndexOf('|');

            if (separatorPos == -1)
            {
                separatorPos = actualLook.IndexOf("}");

                if (separatorPos == -1)
                    throw new Exception("Incorrect EntityLook format : " + actualLook);
            }

            var bonesId = short.Parse(actualLook.Substring(cursorPos, separatorPos - cursorPos));
            cursorPos = separatorPos + 1;

            var skins = new short[0];
            if ((separatorPos = actualLook.IndexOf('|', cursorPos)) != -1 ||
                 (separatorPos = actualLook.IndexOf('}', cursorPos)) != -1)
            {
                skins = ParseCollection(actualLook.Substring(cursorPos, separatorPos - cursorPos), short.Parse);
                cursorPos = separatorPos + 1;
            }

            var colors = new Tuple<int, int>[0];
            if ((separatorPos = actualLook.IndexOf('|', cursorPos)) != -1 ||
                 (separatorPos = actualLook.IndexOf('}', cursorPos)) != -1) // if false there are no informations between the two separators
            {
                colors = ParseCollection(actualLook.Substring(cursorPos, separatorPos - cursorPos), ParseIndexedColor);
                cursorPos = separatorPos + 1;
            }

            var scales = new short[0];
            if ((separatorPos = actualLook.IndexOf('|', cursorPos)) != -1 ||
                 (separatorPos = actualLook.IndexOf('}', cursorPos)) != -1) // if false there are no informations between the two separators
            {
                scales = ParseCollection(actualLook.Substring(cursorPos, separatorPos - cursorPos), short.Parse);
                cursorPos = separatorPos + 1;
            }

            var subEntities = new List<SubEntity>();
            while (cursorPos < actualLook.Length && (actualLook.Length - cursorPos) >= 3)
            {
                var atSeparatorIndex = actualLook.IndexOf('@', cursorPos, 3); // max size of a byte = 255, so 3 characters
                var equalsSeparatorIndex = actualLook.IndexOf('=', atSeparatorIndex + 1, 3); // max size of a byte = 255, so 3 characters
                var category = byte.Parse(actualLook.Substring(cursorPos, atSeparatorIndex - cursorPos));
                var index = byte.Parse(actualLook.Substring(atSeparatorIndex + 1, equalsSeparatorIndex - (atSeparatorIndex + 1)));

                var hookDepth = 0;
                var i = equalsSeparatorIndex + 1;
                var subEntity = new StringBuilder();
                do
                {
                    subEntity.Append(actualLook[i]);

                    switch (actualLook[i])
                    {
                        case '{':
                            hookDepth++;
                            break;
                        case '}':
                            hookDepth--;
                            break;
                    }

                    i++;
                } while (hookDepth > 0);

                subEntities.Add(new SubEntity((sbyte)index, (sbyte)category, GetEntitylook(subEntity.ToString())));

                cursorPos = i + 1; // ignore the comma and the last '}' char
            }
            var newColors = colors.ToDictionary(x => x.Item1, x => Color.FromArgb(x.Item2));           
            return new EntityLook(bonesId, skins, newColors.Select(x => x.Key << 24 | x.Value.ToArgb() & 0xFFFFFF), scales, subEntities.ToArray());
        }

        public Tuple<int, int> ParseIndexedColor(string str)
        {
            var signPos = str.IndexOf("=");
            var hexNumber = str[signPos + 1] == '#';
            var index = int.Parse(str.Substring(0, signPos));
            var color = int.Parse(str.Substring(signPos + (hexNumber ? 2 : 1), str.Length - (signPos + (hexNumber ? 2 : 1))),
                                  hexNumber ? NumberStyles.HexNumber : NumberStyles.Integer);
            return Tuple.Create(index, color);
        }

        public T[] ParseCollection<T>(string str, Func<string, T> converter)
        {
            if (string.IsNullOrEmpty(str))
                return new T[0];

            var cursorPos = 0;
            var subseparatorPos = str.IndexOf(',', cursorPos);

            // if not empty
            if (subseparatorPos == -1)
                return new[] { converter(str) };

            var collection = new T[str.CountOccurences(',', cursorPos, str.Length - cursorPos) + 1];

            // if there are commas
            int index = 0;
            while (subseparatorPos != -1)
            {
                collection[index] = converter(str.Substring(cursorPos, subseparatorPos - cursorPos));
                cursorPos = subseparatorPos + 1;
                subseparatorPos = str.IndexOf(',', cursorPos);
                index++;
            }

            // last value is not read yet because subseparatorPos = -1
            // 'cause the value is after the comma
            collection[index] = converter(str.Substring(cursorPos, str.Length - cursorPos));

            return collection;
        }
    }
}
