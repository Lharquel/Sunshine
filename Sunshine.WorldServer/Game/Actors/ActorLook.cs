﻿using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Actors.Monsters;
using Sunshine.WorldServer.Game.Actors.Npcs;
using Sunshine.WorldServer.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors
{
    public class ActorLook
    {
        private EntityLook _entityLook;
        private object _actor;

        public ActorLook(object actor)
        {
            _actor = actor;
            switch (actor.GetType().Name)
            {
                case "Character":
                    var character = actor as Character;
                    if (character.Record.CustomLook != null && character.Record.CustomLook != "")
                        _entityLook = EntityManager.Instance.GetEntitylook(character.Record.CustomLook);
                    else
                        _entityLook = EntityManager.Instance.GetEntitylook(character.Record.EntityLook);
                    break;

                case "Monster":
                    _entityLook = EntityManager.Instance.GetEntitylook((actor as Monster).Record.EntityLook);
                    break;

                case "Npc":
                    _entityLook = EntityManager.Instance.GetEntitylook((actor as Npc).Record.EntityLook);
                    break;
            }
        }

        public EntityLook GetEntityLook()
        {
            return _entityLook;
        }

        public void SetLook(bool isCustomLook = false)
        {
            if (isCustomLook)
                _entityLook = EntityManager.Instance.GetEntitylook((_actor as Character).Record.CustomLook);
            else
                _entityLook = EntityManager.Instance.GetEntitylook((_actor as Character).Record.EntityLook);
        }

        public void AddSkin(short skin)
        {
            var skins = _entityLook.skins.ToList();
            skins.Add(skin);
            _entityLook.skins = skins;
        }

        public void RemoveSkin(short skin)
        {
            var skins = _entityLook.skins.ToList();
            skins.Remove(skin);
            _entityLook.skins = skins;
        }
    }
}
