﻿using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Actors.Monsters;
using Sunshine.WorldServer.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Stats
{
    public class StatsHealth : StatsData
    {
        private object _actor;

        public StatsHealth(object actor, short baseValue) 
            : base(baseValue, null)
        {
            _actor = actor;
        }

        public short Taken { get; set; }

        public short PermanentTaken { get; set; }

        public override short Total 
        {
            get
            {
                if (_actor is Character)
                    return (short)((TotalMax + PermanentTaken) - Taken);
                else
                    return (short)((TotalMax + PermanentTaken) - Taken);
            }
        }

        public override short TotalMax
        {
            get
            {
                if(_actor is Character)
                {
                    var character = (_actor as Character);
                    return (short)((50 + (character.Level * 5) + character.Stats[StatsEnum.Vitality].TotalMax + Equiped + Context) - PermanentTaken);
                }
                else
                    return (short)((Base + Equiped + (_actor as Monster).Stats[StatsEnum.Vitality].TotalMax + Equiped + Context) - PermanentTaken);
            }
        }
    }
}
