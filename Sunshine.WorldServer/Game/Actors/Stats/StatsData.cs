﻿using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Stats
{
    public class StatsData
    {
        private short _base;
        private short _equiped;
        private short _context;
        private short? _limit;

        public StatsData(short baseValue, short? limitValue)
        {
            _base = baseValue;
            _limit = limitValue;
        }

        public virtual short Base
        {
            get { return _base; }
            set { _base = value; }
        }

        public virtual short Equiped
        {
            get { return _equiped; }
            set { _equiped = value; }
        }

        public virtual short Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public virtual short? Limit
        {
            get { return _limit.HasValue ? _limit : null; }
            set { _limit = value; }          
        }
        
        public virtual short Total
        {
            get
            {
                if (Limit.HasValue && Base + Equiped > Limit)
                    return Limit.Value;
                else
                    return (short)(Base + Equiped);
            }
        }

        public virtual short TotalMax
        {
            get
            {
                if (Limit.HasValue && Total + Context > Limit)
                    return Limit.Value;
                else
                    return (short)(Total + Context);
            }
        }

        public static implicit operator CharacterBaseCharacteristic(StatsData stats)
            => new CharacterBaseCharacteristic(stats.Base, stats.Equiped, 0, stats.Context);
    }
}
