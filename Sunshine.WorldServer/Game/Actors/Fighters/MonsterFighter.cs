﻿using Sunshine.MySql.Database.World.Monsters;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Actors.Monsters;
using Sunshine.WorldServer.Game.Actors.Stats;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.WorldServer.Game.Fights.Teams;
using Sunshine.WorldServer.Handlers.Actions;
using Sunshine.WorldServer.Handlers.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Fighters
{
    public class MonsterFighter : FightActor
    {
        public Monster Monster { get; set; }

        public MonsterFighter(Monster monster, Fight fight)
        {
            Id = monster.Id;
            Monster = monster;
            Fight = fight;
            Visibility = GameActionFightInvisibilityStateEnum.VISIBLE;
        }

        public override int Id { get; }

        public override ActorLook Look { get { return Monster.Look; } }

        public override Fight Fight { get; }

        public override FightTeam Team { get { return Fight.Team; } }

        public override bool IsAttacker { get { return Fight.Team.Attackers.Contains(this); } }

        public override short Level { get { return Monster.Grade.Level; } }

        public override StatsFields Stats { get { return Monster.Stats; } }

        public override GameActionFightInvisibilityStateEnum Visibility { get; set; }

        public IEnumerable<MonsterDrop> Drops { get { return Monster.Drops; } }

        public MonsterGrade Grade { get { return Monster.Grade; } }

        public override GameFightMinimalStatsPreparation GetGameFightMinimalStatsPreparation
            => new GameFightMinimalStatsPreparation(Stats.Health.Total, Stats.Health.TotalMax, Stats.Health.Base, Stats[StatsEnum.PermanentDamagePercent].Total,
                Stats[StatsEnum.Shield].Total, Stats.AP.TotalMax, Stats.MP.TotalMax, Stats[StatsEnum.SummonLimit].Total,
                Stats[StatsEnum.NeutralResistPercent].Total, Stats[StatsEnum.EarthResistPercent].Total, Stats[StatsEnum.WaterResistPercent].Total,
                Stats[StatsEnum.AirResistPercent].Total, Stats[StatsEnum.FireResistPercent].Total, Stats[StatsEnum.DodgeAPProbability].Total,
                Stats[StatsEnum.DodgeMPProbability].Total, Stats[StatsEnum.TackleBlock].Total, Stats[StatsEnum.TackleEvade].Total, (sbyte)Visibility,
                Stats[StatsEnum.Initiative].Total);

        public override GameFightMinimalStats GetGameFightMinimalStats
            => new GameFightMinimalStats(Stats.Health.Total, Stats.Health.TotalMax, Stats.Health.Base, Stats[StatsEnum.PermanentDamagePercent].Total,
                Stats[StatsEnum.Shield].Total, Stats.AP.TotalMax, Stats.MP.TotalMax, Stats[StatsEnum.SummonLimit].Total,
                Stats[StatsEnum.NeutralResistPercent].Total, Stats[StatsEnum.EarthResistPercent].Total, Stats[StatsEnum.WaterResistPercent].Total,
                Stats[StatsEnum.AirResistPercent].Total, Stats[StatsEnum.FireResistPercent].Total, Stats[StatsEnum.DodgeAPProbability].Total,
                Stats[StatsEnum.DodgeMPProbability].Total, Stats[StatsEnum.TackleBlock].Total, Stats[StatsEnum.TackleEvade].Total, (sbyte)Visibility);

        public override GameFightFighterInformations GetGameFightFighterInformations
            => new GameFightMonsterInformations(Id, Look.GetEntityLook(), new EntityDispositionInformations(Position.Cell, (sbyte)Position.Direction), (sbyte)TeamEnum.TEAM_DEFENDER, IsAlive,
                GetGameFightMinimalStats, (short)Monster.Record.Id, (sbyte)Monster.Grade.GradeId);

        public override GameFightFighterInformations GetGameFightFighterPreparationInformations
            => new GameFightMonsterInformations(Id, Look.GetEntityLook(), new EntityDispositionInformations(Position.Cell, (sbyte)Position.Direction), (sbyte)TeamEnum.TEAM_DEFENDER, IsAlive,
                GetGameFightMinimalStatsPreparation, (short)Monster.Record.Id, (sbyte)Monster.Grade.GradeId);

        public override IdentifiedEntityDispositionInformations GetIdentifiedEntityDispositionInformations
            => new IdentifiedEntityDispositionInformations(Position.Cell, (sbyte)Position.Direction, Id);

        public override FightTeamMemberInformations GetFightTeamMemberInformations
            => new FightTeamMemberMonsterInformations(Id, Monster.Record.Id, (sbyte)Monster.Grade.GradeId);
    }
}
