﻿using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Maps;
using Sunshine.WorldServer.Game.Actors.Stats;
using Sunshine.WorldServer.Game.Effects;
using Sunshine.Protocol.Utils;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Handlers.Actions;
using Sunshine.WorldServer.Handlers.Context;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Game.Fights.Teams;
using Sunshine.WorldServer.Game.Effects.Spells.Damages;

namespace Sunshine.WorldServer.Game.Actors.Fighters
{
    public abstract class FightActor
    {
        public abstract int Id { get; }

        public abstract FightTeam Team { get; }

        public abstract ActorLook Look { get; }

        public abstract bool IsAttacker { get; }

        public abstract GameFightFighterInformations GetGameFightFighterInformations { get; }

        public abstract GameFightFighterInformations GetGameFightFighterPreparationInformations { get; }

        public abstract IdentifiedEntityDispositionInformations GetIdentifiedEntityDispositionInformations { get; }

        public abstract FightTeamMemberInformations GetFightTeamMemberInformations { get; }

        public abstract GameFightMinimalStatsPreparation GetGameFightMinimalStatsPreparation { get; }

        public abstract GameFightMinimalStats GetGameFightMinimalStats { get; }

        public abstract short Level { get; }

        public abstract StatsFields Stats { get; }

        public abstract GameActionFightInvisibilityStateEnum Visibility { get; set; }

        public abstract Fight Fight { get; }

        public Map Map { get { return Team.Fight.Map; } }

        public ObjectPosition Position { get; set; }

        public bool IsAlive { get { return Stats.Health.Total > 0; } }

        public bool IsDead { get { return !IsAlive; } }

        public bool IsReady { get; set; }

        public bool IsFighterTurn { get { return Fight.FighterPlaying == this; } }

        public bool IsFriendlyWith(FightActor fighter)
        {
            if (this.IsAttacker)
                return Team.Attackers.Contains(fighter);
            else
                return false;
        }

        public bool IsEnnemyWith(FightActor fighter)
        {
            return !IsFriendlyWith(fighter);
        }

        public void SetReadyStatus(bool isReady)
        {
            IsReady = isReady;
            Fight.Clients.ForEach(x => ContextHandler.SendGameFightHumanReadyStateMessage(x, this));
            Fight.CheckAllStatus();
        }

        public void StartTurn()
        {
            Fight.StartAction(35000, "EndTurn");
            ContextHandler.SendGameFightTurnStartMessage(Fight.Clients, this);
            ContextHandler.SendGameFightSynchronizeMessage(Fight.Clients, Fight.GetAllFighters());
            foreach (CharacterFighter fighter in Fight.GetAllFighters(x => x is CharacterFighter))
                fighter.Character.RefreshStats();
            if (this is MonsterFighter)
                this.EndTurn();
        }

        public void EndTurn()
        {
            ContextHandler.SendGameFightTurnEndMessage(Fight.Clients, this);
            ContextHandler.SendGameFightTurnReadyRequestMessage(Fight.Clients, this);
            this.ResetUsedPoints();           
            Fight.FighterPlaying = Fight.GetFighterPlaying();
            Fight.FighterPlaying.StartTurn();
        }

        public void ShowCell(short cellId, bool team = true)
        {
            if (team)
            {
                var teamClients = Fight.Clients.Where(x => x.Character.Fighter.IsAttacker == this.IsAttacker);
                ContextHandler.SendShowCellMessage(teamClients, this, cellId);
            }
            else
                ContextHandler.SendShowCellMessage(this.Fight.Clients, this, cellId);
        }

        public void StartMove(IEnumerable<short> keyMovements)
        {
            short MPCount = (short)Position.Point.DistanceToCell(new MapPoint((short)(keyMovements.Last() & 4095)));
            Fight.StartSequence(SequenceTypeEnum.SEQUENCE_MOVE);
            if (IsFighterTurn && Stats.MP.TotalMax >= MPCount)
            {
                this.UseMP(MPCount);
                Position.Cell = (short)(keyMovements.Last() & 4095);
                Position.Direction = (DirectionsEnum)(keyMovements.Last() >> 12);
                ContextHandler.SendGameMapMovementMessage(Fight.Clients, Id, keyMovements);
            }
            Fight.EndSequence(SequenceTypeEnum.SEQUENCE_MOVE, ActionsEnum.ACTION_CHARACTER_MOVEMENT);
        }

        public void CastSpell(Spell spell, short cell)
        {
            Fight.StartSequence(SequenceTypeEnum.SEQUENCE_SPELL);
            Fight.OnSpellCasting(this, spell, cell, FightSpellCastCriticalEnum.NORMAL, false);
            this.UseAP((short)spell.Template.ApCost);
            foreach (var effect in spell.Effects)
                EffectDispatcher.Dispatch(this, spell, effect, cell);
            Fight.EndSequence(SequenceTypeEnum.SEQUENCE_SPELL, ActionsEnum.ACTION_FIGHT_CAST_SPELL);
            Fight.CheckFightEnd();
        }

        public void InflictDamage(int damage)
        {
            InflictDamage(new Damage(damage));
        }

        public void InflictDamage(Damage damage)
        {
            damage.GenerateDamages();
            damage.Amount = CalculateDamage(damage.Source, damage.Amount, damage.EffectSchool);
            damage.Amount = CalculateDamageResistance(damage.Amount, damage.EffectSchool, Fight.Type == FightTypeEnum.FIGHT_TYPE_AGRESSION);
            this.Stats.Health.Taken += damage.Amount;
            Fight.OnLifePointsChanged(IsDead ? -(Stats.Health.Total + damage.Amount) : -damage.Amount, damage.Source, this);
            if (this.IsDead)
                this.OnDead(this, damage.Source);
        }

        public void Heal(int healPoints, FightActor source, bool withBoost = false)
        {
            if (withBoost)
            {
                healPoints = CalculateHeal(healPoints);
                if (Stats.Health.Total + healPoints > Stats.Health.TotalMax)
                    healPoints = Stats.Health.TotalMax - Stats.Health.Total;
            }
            else
            {
                if (Stats.Health.Total + healPoints > Stats.Health.TotalMax)
                    healPoints = Stats.Health.TotalMax - Stats.Health.Total;              
            }
            this.Stats.Health.Taken -= (short)healPoints;
            Fight.OnLifePointsChanged(healPoints, source, this);
        }

        public void OnDead(FightActor target, FightActor fighter)
        {
            Fight.StartSequence(SequenceTypeEnum.SEQUENCE_CHARACTER_DEATH);          
            ActionsHandler.SendGameActionFightDeathMessage(Fight.Clients, fighter, target);
            Fight.TimeLine.Fighters.Remove(target);
            Fight.TimeLine.Leavers.Add(target);
            Fight.EndSequence(SequenceTypeEnum.SEQUENCE_CHARACTER_DEATH, ActionsEnum.ACTION_CHARACTER_DEATH);
            if (target.IsFighterTurn)
                target.EndTurn();
        }

        public void ResetUsedPoints()
        {
            Stats.AP.Context = 0;
            Stats.MP.Context = 0;
        }

        public void ResetFightProperties()
        {
            ResetUsedPoints();
            foreach (StatsEnum stats in typeof(StatsEnum).GetEnumValues())
                this.Stats[stats].Context = 0;
        }

        public void UseAP(short count)
        {
            Stats.AP.Context -= count;
            Fight.OnFightPointsVariation(ActionsEnum.ACTION_CHARACTER_ACTION_POINTS_USE, this, this, -count);
        }

        public void UseMP(short count)
        {
            Stats.MP.Context -= count;
            Fight.OnFightPointsVariation(ActionsEnum.ACTION_CHARACTER_MOVEMENT_POINTS_USE, this, this, -count);
        }

        private short CalculateDamage(FightActor caster, short damage, EffectSchoolEnum type)
        {
            short result;
            switch (type)
            {
                case EffectSchoolEnum.Neutral:
                    result = (short)((double)(damage * (100 + caster.Stats[StatsEnum.Strength].TotalMax + caster.Stats[StatsEnum.DamageBonusPercent].TotalMax +
                    caster.Stats[StatsEnum.DamageMultiplicator].TotalMax * 100)) / 100.0 + (double)(caster.Stats[StatsEnum.DamageBonus].TotalMax +
                    caster.Stats[StatsEnum.PhysicalDamage].TotalMax + caster.Stats[StatsEnum.NeutralDamageBonus].TotalMax));
                    break;
                case EffectSchoolEnum.Earth:
                    result = (short)((double)(damage * (100 + caster.Stats[StatsEnum.Strength].TotalMax + caster.Stats[StatsEnum.DamageBonusPercent].TotalMax +
                    caster.Stats[StatsEnum.DamageMultiplicator].TotalMax * 100)) / 100.0 + (double)(caster.Stats[StatsEnum.DamageBonus].TotalMax +
                    caster.Stats[StatsEnum.PhysicalDamage].TotalMax + caster.Stats[StatsEnum.EarthDamageBonus].TotalMax));
                    break;
                case EffectSchoolEnum.Water:
                    result = (short)((double)(damage * (100 + caster.Stats[StatsEnum.Chance].TotalMax + caster.Stats[StatsEnum.DamageBonusPercent].TotalMax +
                    caster.Stats[StatsEnum.DamageMultiplicator].TotalMax * 100)) / 100.0 + (double)(caster.Stats[StatsEnum.DamageBonus].TotalMax +
                    caster.Stats[StatsEnum.MagicDamage].TotalMax + caster.Stats[StatsEnum.WaterDamageBonus].TotalMax));
                    break;
                case EffectSchoolEnum.Air:
                    result = (short)((double)(damage * (100 + caster.Stats[StatsEnum.Agility].TotalMax + caster.Stats[StatsEnum.DamageBonusPercent].TotalMax +
                    caster.Stats[StatsEnum.DamageMultiplicator].TotalMax * 100)) / 100.0 + (double)(caster.Stats[StatsEnum.DamageBonus].TotalMax +
                    caster.Stats[StatsEnum.MagicDamage].TotalMax + caster.Stats[StatsEnum.AirDamageBonus].TotalMax));
                    break;
                case EffectSchoolEnum.Fire:
                    result = (short)((double)(damage * (100 + caster.Stats[StatsEnum.Intelligence].TotalMax + caster.Stats[StatsEnum.DamageBonusPercent].TotalMax +
                    caster.Stats[StatsEnum.DamageMultiplicator].TotalMax * 100)) / 100.0 + (double)(caster.Stats[StatsEnum.DamageBonus].TotalMax +
                    caster.Stats[StatsEnum.MagicDamage].TotalMax + caster.Stats[StatsEnum.FireDamageBonus].TotalMax));
                    break;
                default:
                    result = damage;
                    break;
            }
            return result;
        }

        private short CalculateDamageResistance(short damage, EffectSchoolEnum type, bool pvp)
        {
            double result;
            double result1;
            switch (type)
            {
                case EffectSchoolEnum.Neutral:
                    result = (double)(this.Stats[StatsEnum.NeutralResistPercent].TotalMax + (pvp ? this.Stats[StatsEnum.PvpNeutralResistPercent].TotalMax + this.Stats[StatsEnum.NeutralResistPercent].TotalMax >= 50 ? 50 - this.Stats[StatsEnum.NeutralResistPercent].TotalMax : this.Stats[StatsEnum.PvpNeutralResistPercent].TotalMax : 0));
                    result1 = (double)(this.Stats[StatsEnum.NeutralElementReduction].TotalMax + (pvp ? this.Stats[StatsEnum.PvpNeutralElementReduction].TotalMax : 0) + this.Stats[StatsEnum.PhysicalDamageReduction].TotalMax);
                    break;
                case EffectSchoolEnum.Earth:
                    result = (double)(this.Stats[StatsEnum.EarthResistPercent].TotalMax + (pvp ? this.Stats[StatsEnum.PvpEarthResistPercent].TotalMax + this.Stats[StatsEnum.EarthResistPercent].TotalMax >= 50 ? 50 - this.Stats[StatsEnum.EarthResistPercent].TotalMax : this.Stats[StatsEnum.PvpEarthResistPercent].TotalMax : 0));
                    result1 = (double)(this.Stats[StatsEnum.EarthElementReduction].TotalMax + (pvp ? this.Stats[StatsEnum.PvpEarthElementReduction].TotalMax : 0) + this.Stats[StatsEnum.PhysicalDamageReduction].TotalMax);
                    break;
                case EffectSchoolEnum.Water:
                    result = (double)(this.Stats[StatsEnum.WaterResistPercent].TotalMax + (pvp ? this.Stats[StatsEnum.PvpWaterResistPercent].TotalMax + this.Stats[StatsEnum.WaterResistPercent].TotalMax >= 50 ? 50 - this.Stats[StatsEnum.WaterResistPercent].TotalMax : this.Stats[StatsEnum.PvpWaterResistPercent].TotalMax : 0));
                    result1 = (double)(this.Stats[StatsEnum.WaterElementReduction].TotalMax + (pvp ? this.Stats[StatsEnum.PvpWaterElementReduction].TotalMax : 0) + this.Stats[StatsEnum.MagicDamageReduction].TotalMax);
                    break;
                case EffectSchoolEnum.Air:
                    result = (double)(this.Stats[StatsEnum.AirResistPercent].TotalMax + (pvp ? this.Stats[StatsEnum.PvpAirResistPercent].TotalMax + +this.Stats[StatsEnum.AirResistPercent].TotalMax >= 50 ? 50 - this.Stats[StatsEnum.AirResistPercent].TotalMax : this.Stats[StatsEnum.PvpAirResistPercent].TotalMax : 0));
                    result1 = (double)(this.Stats[StatsEnum.AirElementReduction].TotalMax + (pvp ? this.Stats[StatsEnum.PvpAirElementReduction].TotalMax : 0) + this.Stats[StatsEnum.MagicDamageReduction].TotalMax);
                    break;
                case EffectSchoolEnum.Fire:
                    result = (double)(this.Stats[StatsEnum.FireResistPercent].TotalMax + (pvp ? this.Stats[StatsEnum.PvpFireResistPercent].TotalMax + this.Stats[StatsEnum.FireResistPercent].TotalMax >= 50 ? 50 - this.Stats[StatsEnum.FireResistPercent].TotalMax : this.Stats[StatsEnum.PvpFireResistPercent].TotalMax : 0));
                    result1 = (double)(this.Stats[StatsEnum.FireElementReduction].TotalMax + (pvp ? this.Stats[StatsEnum.PvpFireElementReduction].TotalMax : 0) + this.Stats[StatsEnum.MagicDamageReduction].TotalMax);
                    break;
                default:
                    return damage;
            }           
            return (short)((1.0 - result / 100.0) * ((double)damage - result1));
        }

        public int CalculateHeal(int heal)
        {
            return (int)((double)(heal * (100 + this.Stats[StatsEnum.Intelligence].TotalMax)) / 100.0 + (double)this.Stats[StatsEnum.HealBonus].TotalMax);
        }

        public void GeneratePosition()
        {
            List<short> Cells = new List<short>();
            Position = new ObjectPosition(0, DirectionsEnum.DIRECTION_EAST);

            if (IsAttacker)
            {
                Cells = Map.RedCells;
                Position.Cell = Cells.FirstOrDefault(x => Team.Attackers.FirstOrDefault(y =>
                                              y.GetGameFightFighterInformations.disposition.cellId == x && Cells.Last() != x) == null);

                Position.Direction = (DirectionsEnum)Cells.Last();
            }
            else
            {
                Cells = Map.BlueCells;
                Position.Cell = Cells.FirstOrDefault(x => Team.Defenders.FirstOrDefault(y =>
                                              y.GetGameFightFighterInformations.disposition.cellId == x && Cells.Last() != x) == null);

                Position.Direction = (DirectionsEnum)Cells.Last();
            }
        }
    }
}
