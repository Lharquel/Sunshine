﻿using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.Protocol.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using Sunshine.WorldServer.Handlers.Context;
using Sunshine.WorldServer.Handlers.Actions;
using Sunshine.WorldServer.Game.Actors.Stats;
using Sunshine.WorldServer.Game.Fights.Teams;

namespace Sunshine.WorldServer.Game.Actors.Fighters
{
    public class CharacterFighter : FightActor
    {
        public Character Character { get; set; }

        public CharacterFighter(Character character)
        {
            Character = character;
            Visibility = GameActionFightInvisibilityStateEnum.VISIBLE;
        }

        public WorldClient Client { get { return Character.Client; } }

        public override Fight Fight { get { return Character.Fight; } }

        public override int Id { get { return Character.Id; } }

        public string Name { get { return Character.Name; } }

        public override short Level { get { return Character.Level; } }

        public override ActorLook Look { get { return Character.Look; } }  

        public override FightTeam Team { get { return Fight.Team; } }

        public override bool IsAttacker { get { return Fight.Team.Attackers.Contains(this); } }

        public override StatsFields Stats { get { return Character.Stats; } }       

        public override GameActionFightInvisibilityStateEnum Visibility { get; set; }

        public override GameFightMinimalStatsPreparation GetGameFightMinimalStatsPreparation
            => new GameFightMinimalStatsPreparation(Stats.Health.Total, Stats.Health.TotalMax, Stats.Health.Base, Stats[StatsEnum.PermanentDamagePercent].Total,
                Stats[StatsEnum.Shield].Total, Stats.AP.TotalMax, Stats.MP.TotalMax, Stats[StatsEnum.SummonLimit].Total,
                Stats[StatsEnum.NeutralResistPercent].Total, Stats[StatsEnum.EarthResistPercent].Total, Stats[StatsEnum.WaterResistPercent].Total,
                Stats[StatsEnum.AirResistPercent].Total, Stats[StatsEnum.FireResistPercent].Total, Stats[StatsEnum.DodgeAPProbability].Total,
                Stats[StatsEnum.DodgeMPProbability].Total, Stats[StatsEnum.TackleBlock].Total, Stats[StatsEnum.TackleEvade].Total, (sbyte)Visibility, 
                Stats[StatsEnum.Initiative].Total);

        public override GameFightMinimalStats GetGameFightMinimalStats
            => new GameFightMinimalStats(Stats.Health.Total, Stats.Health.TotalMax, Stats.Health.Base, Stats[StatsEnum.PermanentDamagePercent].Total,
                Stats[StatsEnum.Shield].Total, Stats.AP.TotalMax, Stats.MP.TotalMax, Stats[StatsEnum.SummonLimit].Total,
                Stats[StatsEnum.NeutralResistPercent].Total, Stats[StatsEnum.EarthResistPercent].Total, Stats[StatsEnum.WaterResistPercent].Total,
                Stats[StatsEnum.AirResistPercent].Total, Stats[StatsEnum.FireResistPercent].Total, Stats[StatsEnum.DodgeAPProbability].Total,
                Stats[StatsEnum.DodgeMPProbability].Total, Stats[StatsEnum.TackleBlock].Total, Stats[StatsEnum.TackleEvade].Total, (sbyte)Visibility);
               
        public override GameFightFighterInformations GetGameFightFighterInformations
            => new GameFightCharacterInformations(Id, Look.GetEntityLook(), new EntityDispositionInformations(Position.Cell, (sbyte)Position.Direction), IsAttacker ? (sbyte)TeamEnum.TEAM_CHALLENGER : (sbyte)TeamEnum.TEAM_DEFENDER, IsAlive,
                GetGameFightMinimalStats, Name, Level, Character.GetActorAlignmentInformations);

        public override GameFightFighterInformations GetGameFightFighterPreparationInformations
            => new GameFightCharacterInformations(Id, Look.GetEntityLook(), new EntityDispositionInformations(Position.Cell, (sbyte)Position.Direction), IsAttacker ? (sbyte)TeamEnum.TEAM_CHALLENGER : (sbyte)TeamEnum.TEAM_DEFENDER, IsAlive,
                GetGameFightMinimalStatsPreparation, Name, Level, Character.GetActorAlignmentInformations);

        public override IdentifiedEntityDispositionInformations GetIdentifiedEntityDispositionInformations
            => new IdentifiedEntityDispositionInformations(Position.Cell, (sbyte)Position.Direction, Id);

        public override FightTeamMemberInformations GetFightTeamMemberInformations
            => new FightTeamMemberCharacterInformations(Id, Character.Name, Character.Level);

    }
}
