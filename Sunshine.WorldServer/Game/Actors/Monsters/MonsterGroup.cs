﻿using Sunshine.MySql.Database.World.Monsters;
using Sunshine.Protocol.Utils;
using Sunshine.Protocol.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Game.Maps;
using Sunshine.Protocol.Enums;

namespace Sunshine.WorldServer.Game.Actors.Monsters
{
    public class MonsterGroup : RolePlayActor
    {
        public MonsterGroup(List<Monster> monsters, Map map, bool isDungeon = false)
        {
            Id = ActorManager.Instance.GenerateId();
            Monsters = new List<Monster>();
            Map = map;
            GenerateGroup(monsters, isDungeon);
            GenerateLeader();
            GenerateDisposition();
        }

        public override int Id { get;}

        public Monster Leader { get; set; }

        public List<Monster> Monsters { get; set; }

        public Map Map { get; set; }

        public short Cell { get; set; }

        public DirectionsEnum Direction { get; set; }

        public EntityDispositionInformations GetEntityDisposition
            => new EntityDispositionInformations(Cell, (sbyte)Direction);

        public override GameRolePlayActorInformations GetGameRolePlayActorInformations
            => new GameRolePlayGroupMonsterInformations(Id, Leader.Look.GetEntityLook(), GetEntityDisposition, Leader.Record.Id, (sbyte)Leader.Grade.GradeId, Monsters.Where(x => x.Id != Leader.Id).Select(x => x.GetMonsterInGroupInformations), 0, 0); 

        private void GenerateLeader()
        {
            AsyncRandom rdn = new AsyncRandom();
            Leader = Monsters[rdn.Next(0, Monsters.Count)];
        }

        private void GenerateGroup(List<Monster> monsters, bool isDungeon)
        {
            if (isDungeon)
            {
                foreach(var monster in monsters)
                {
                    var newMonster = monster.Clone();
                    newMonster.Id = ActorManager.Instance.GenerateId(true);
                    newMonster.GenerateGrade();
                    newMonster.Stats = new Stats.StatsFields(monster);
                    Monsters.Add(newMonster);
                }
            }
            else
            {
                AsyncRandom rdn = new AsyncRandom();
                int count = rdn.Next(1, 9);
                for (int i = 0; i < count; i++)
                {
                    Monster monster = monsters[rdn.Next(0, monsters.Count)].Clone();
                    monster.Id = ActorManager.Instance.GenerateId(true, i == 0);
                    monster.GenerateGrade();
                    monster.Stats = new Stats.StatsFields(monster);
                    Monsters.Add(monster);
                }
            }
        }

        private void GenerateDisposition()
        {
            AsyncRandom rdn = new AsyncRandom();
            var cells = Map.Cells.Where(x => x.Walkable && Map.RolePlayActors.FirstOrDefault(w => w.GetGameRolePlayActorInformations.disposition.cellId == x.Id) == null).ToList();
            Cell = cells[rdn.Next(0, cells.Count)].Id;          
            Direction = (DirectionsEnum)new AsyncRandom().Next(0, 8);
            Leader.GetMonsterGroup = this;
        }
    }
}
