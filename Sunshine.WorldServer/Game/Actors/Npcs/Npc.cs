﻿using Sunshine.MySql.Database.World.Npcs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Types;
using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Handlers.Characters.Inventory;
using Sunshine.WorldServer.Game.Actors.Npcs.Actions;

namespace Sunshine.WorldServer.Game.Actors.Npcs
{
    public class Npc : RolePlayActor
    {
        public NpcTemplate Record { get; set; }

        public NpcSpawn Spawn { get; set; }

        public NpcMessage Message { get; set; }

        public IEnumerable<NpcReplie> Replies { get; set; }

        public Dictionary<int, NpcShop> Shops { get; set; }

        public Npc(NpcTemplate record, NpcSpawn spawn)
        {
            Id = ActorManager.Instance.GenerateId();
            Record = record;
            Spawn = spawn;
            Message = NpcManager.Instance.GetNpcMessage(Record.Id);
            Replies = NpcManager.Instance.GetNpcReplies(Record.Id);
            Shops = NpcManager.Instance.GetNpcShops(Record.Id);
            Look = new ActorLook(this);
            GetNpcTypes = new List<List<string>>();
            GetDialogMessagesId = new List<List<short>>();
            GetDialogRepliesId = new List<List<string>>();
            GetParameters = new List<List<string>>();
            InitializeNpc();
        }

        public override int Id { get; }

        public ActorLook Look { get; set; }

        public NpcAction Action { get; set; }

        public EntityDispositionInformations GetEntityDisposition
            => new EntityDispositionInformations(Spawn.Cell, (sbyte)Spawn.Direction);

        public override GameRolePlayActorInformations GetGameRolePlayActorInformations
            => new GameRolePlayNpcInformations(Id, Look.GetEntityLook(), GetEntityDisposition, (short)Record.Id, Record.Gender, 0, Record.HasQuest);

        public IEnumerable<ObjectItemToSellInNpcShop> GetObjectItemToSellInNpcShops { get; set; }

        public List<List<string>> GetNpcTypes { get; set; }

        public List<List<short>> GetDialogMessagesId { get; set; }

        public List<List<string>> GetDialogRepliesId { get; set; }

        public List<List<string>> GetParameters { get; set; }

        public void InteractWith(NpcActionTypeEnum action, Character source)
        {
            switch (action)
            {
                case NpcActionTypeEnum.ACTION_TALK:
                    Action = new NpcTalkAction(this, source);
                    Action.Execute();
                    break;

                case NpcActionTypeEnum.ACTION_BUY_SELL:
                    Action = new NpcBuySellAction(this, source);
                    Action.Execute();
                    break;

                case NpcActionTypeEnum.ACTION_BUY:
                    Action = new NpcBuyAction(this, source);
                    Action.Execute();
                    break;

                case NpcActionTypeEnum.ACTION_SELL:
                    Action = new NpcSellAction(this, source);
                    Action.Execute();
                    break;

                case NpcActionTypeEnum.ACTION_EXCHANGE:
                    Action = new NpcTradeAction(this, source);
                    Action.Execute();
                    break;
            }
        }

        private void InitializeNpc()
        {
            List<ObjectItemToSellInNpcShop> objectItems = new List<ObjectItemToSellInNpcShop>();
            foreach (var shop in Shops.Values)
            {
                var item = ItemManager.Instance.Items[shop.Item];
                objectItems.Add(new ObjectItemToSellInNpcShop((short)shop.Item, 0, false, item.EffectsBase.Select(x => x.GetObjectEffectMinMax()), (int)item.Price, ""));
            }       
            GetObjectItemToSellInNpcShops = objectItems;
            foreach (var replie in Replies)
            {
                GetNpcTypes.Add(replie.TypeIdsCSV.Split(',').ToList());
                GetDialogMessagesId.Add(new List<short> { replie.MessageIdsCSV });
                if (replie.ReplieIdsCSV != null)
                    GetDialogRepliesId.Add(replie.ReplieIdsCSV.Split(',').ToList());
                else
                    GetDialogRepliesId.Add(new List<string>());
                if (replie.ParametersCSV != null)
                    GetParameters.Add(replie.ParametersCSV.Split(',').ToList());
                else
                    GetParameters.Add(new List<string>());
            }
        }
    }
}
