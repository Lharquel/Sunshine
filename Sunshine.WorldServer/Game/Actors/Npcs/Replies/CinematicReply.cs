﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Messages;

namespace Sunshine.WorldServer.Game.Actors.Npcs.Replies
{
    [ReplyHandler(4)]
    public class CinematicReply : Reply
    {
        public CinematicReply()
        {
        }

        public override bool Execute()
        {
            short cinematic = (short)Parameters[0];
            Client.Send(new CinematicMessage(cinematic));
            return true;
        }
    }
}
