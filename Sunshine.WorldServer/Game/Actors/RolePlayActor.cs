﻿using Sunshine.Protocol.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors
{
    public abstract class RolePlayActor
    {
        public abstract int Id { get; }

        public abstract GameRolePlayActorInformations GetGameRolePlayActorInformations { get; }
    }
}
