﻿using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Dialogs;
using Sunshine.WorldServer.Handlers.Context;
using Sunshine.WorldServer.Handlers.Interactives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Maps.Interactives.Skills
{
    [SkillHandler(184), SkillHandler(183)]
    public class SkillTeleport : Skill, IDialog
    {
        public override void Execute()
        {
            var door = Client.Character.Map.Interactives.FirstOrDefault(x => x.Element == Element);
            if (door != null)
            {
                var map = MapManager.Instance.GetMap(door.Parameters[0]);
                if (map == null)
                    return;
                short cell = (short)door.Parameters[1];
                var direction = door.Parameters[2];
                Client.Character.Teleport(map.Id, cell);
                Client.Character.Direction = direction;
                ContextHandler.SendGameMapChangeOrientationMessage(Client);
            }
        }
    }
}
