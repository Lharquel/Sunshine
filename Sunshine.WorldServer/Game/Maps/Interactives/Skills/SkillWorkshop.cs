﻿using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using Sunshine.WorldServer.Game.Actors.Characters.Jobs;
using Sunshine.WorldServer.Game.Dialogs;
using Sunshine.WorldServer.Game.Exchanges;
using Sunshine.WorldServer.Handlers.Interactives;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Maps.Interactives.Skills
{
    [SkillHandler(63), SkillHandler(64), SkillHandler(123)]
    public class SkillWorkshop : Skill, IDialog
    {
        public override void Execute()
        {
            Client.Character.Dialog = this;
            var workshop = Client.Character.Map.Interactives.FirstOrDefault(x => x.Element == Element);
            if (workshop != null)
            {
                InteractiveHandler.SendInteractiveUsedMessage(Client.Character.Map.Clients, Client.Character, this);
                sbyte jobId = (sbyte)workshop.Parameters[0];
                Job = Client.Character.Jobs.GetJob(jobId);
                if (Job != null)
                {
                    var jobLevel = ExperienceManager.Instance.GetJobLevelExperienceFloor(Job.Experience);
                    var caseAvailable = JobManager.Instance.GetJobSlot(jobLevel);
                    Client.Character.SetTrade(ExchangeTypeEnum.CRAFT);
                    Client.Character.Trade.Open(new List<object> { jobId, caseAvailable, Id });
                }   
                else
                    Client.Character.SendNotificationByServerMessage("Vous ne possédez pas le métier nécessaire", NotificationEnum.ERREUR);
            }
        }
    }
}
