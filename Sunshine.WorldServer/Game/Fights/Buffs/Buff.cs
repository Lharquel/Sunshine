﻿using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Fights.Buffs
{
    public abstract class Buff
    {
        public FightActor Caster { get; set; }

        public FightActor Target { get; set; }

        public BuffTypeEnum Type { get; set; }

        public Spell Spell { get; set; }
        
        public short Duration { get; set; }
       
        public bool Dispellable { get; set; }

        public abstract void Apply();

        public abstract void Dispell();
    }
}
