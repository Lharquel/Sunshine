﻿using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Items;
using Sunshine.WorldServer.Handlers.Characters.Inventory;
using Sunshine.WorldServer.Handlers.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Exchanges
{
    public class PlayerTrade : Trade
    {
        public Trader Trader { get; set; }

        public Trader Target { get; set; }

        public PlayerTrade(Character trader, Character target)
        {
            Trader = new Trader(trader);
            Target = new Trader(target);
            Inventories = new Dictionary<RolePlayActor, List<Tuple<BasePlayerItem, int>>>();                  
        }

        public override ExchangeTypeEnum Type { get { return ExchangeTypeEnum.PLAYER_TRADE; } }

        public override void Open(List<object> parameters = null)
        {           
            InventoryHandler.SendExchangeStartedWithPodsMessage(Trader.Client, Type, Trader, Target);
            InventoryHandler.SendExchangeStartedWithPodsMessage(Target.Client, Type, Trader, Target);           
        }

        public override void Close(List<object> parameters = null)
        {
            Trader.Trade = null;
            Target.Trade = null;
            InventoryHandler.SendExchangeLeaveMessage(Trader.Client, true);
            InventoryHandler.SendExchangeLeaveMessage(Target.Client, true);        
        }

        public override void Cancel(List<object> parameters = null)
        {
            Trader.Trade = null;
            Target.Trade = null;
            InventoryHandler.SendExchangeLeaveMessage(Trader.Client, false);
            InventoryHandler.SendExchangeLeaveMessage(Target.Client, false);            
        }

        public override void SetReadyStatus(Character trader, bool isReady)
        {
            GetTrader(trader).IsReady = isReady;            
            InventoryHandler.SendExchangeIsReadyMessage(Trader.Client, GetTrader(trader), isReady);
            InventoryHandler.SendExchangeIsReadyMessage(Target.Client, GetTrader(trader), isReady);

            if(Trader.IsReady && Target.IsReady)
                Apply();
        }

        public override void SetKamas(Character trader, int quantity)
        {
            if (quantity > trader.Inventory.Kamas)
                quantity = (int)trader.Inventory.Kamas;

            GetTrader(trader).Kamas = quantity;
            InventoryHandler.SendExchangeKamaModifiedMessage(Trader.Client, trader != Trader.Actor, quantity);
            InventoryHandler.SendExchangeKamaModifiedMessage(Target.Client, trader != Target.Actor, quantity);
        }

        public override void MoveItem(Character trader, int objectUid, int quantity)
        {
            #region Move Item
            var item = trader.Inventory.GetItemUid(objectUid).Clone();
            if (item != null)
            {
                if (Inventories.ContainsKey(trader))
                {
                    var itemStock = Inventories[trader].FirstOrDefault(x => x.Item1.Id == item.Id);
                    if (itemStock != null)
                    {
                        if (quantity < 0)
                        {
                            quantity *= -1;
                            if (itemStock.Item2 > quantity) // Remove Stack
                            {
                                item.Stack = itemStock.Item2 - quantity;
                                InventoryHandler.SendExchangeObjectModifiedMessage(Trader.Client, trader != Trader.Actor, item);
                                InventoryHandler.SendExchangeObjectModifiedMessage(Target.Client, trader != Trader.Actor, item);
                                Inventories[trader].Remove(itemStock);
                                Inventories[trader].Add(new Tuple<BasePlayerItem, int>(item, item.Stack));
                            }
                            else // Removed
                            {
                                InventoryHandler.SendExchangeObjectRemovedMessage(Trader.Client, trader != Trader.Actor, item);
                                InventoryHandler.SendExchangeObjectRemovedMessage(Target.Client, trader != Trader.Actor, item);
                                Inventories[trader].Remove(itemStock);
                            }
                        }
                        else // Add Stack
                        {
                            if(item.Stack - itemStock.Item2 >= quantity)
                            {
                                item.Stack = itemStock.Item2 + quantity;
                                InventoryHandler.SendExchangeObjectModifiedMessage(Trader.Client, trader != Trader.Actor, item);
                                InventoryHandler.SendExchangeObjectModifiedMessage(Target.Client, trader != Trader.Actor, item);
                                Inventories[trader].Remove(itemStock);
                                Inventories[trader].Add(new Tuple<BasePlayerItem, int>(item, item.Stack));
                            }
                        }
                    }
                    else // Add
                    {
                        if (quantity < item.Stack)
                            item.Stack = quantity;

                        InventoryHandler.SendExchangeObjectAddedMessage(Trader.Client, trader != Trader.Actor, item);
                        InventoryHandler.SendExchangeObjectAddedMessage(Target.Client, trader != Trader.Actor, item);
                        Inventories[trader].Add(new Tuple<BasePlayerItem, int>(item, item.Stack));
                    }
                }
                else // Add
                {
                    if (quantity < item.Stack)
                        item.Stack = quantity;

                    InventoryHandler.SendExchangeObjectAddedMessage(Trader.Client, trader != Trader.Actor, item);
                    InventoryHandler.SendExchangeObjectAddedMessage(Target.Client, trader != Trader.Actor, item);
                    Inventories.Add(trader, new List<Tuple<BasePlayerItem, int>>() { new Tuple<BasePlayerItem, int>(item, item.Stack) });
                }
            }
            #endregion
        }

        public override void Apply()
        {
            if (Inventories.ContainsKey(Trader.Actor))
            {
                foreach (var items in Inventories[Trader.Actor])
                {
                    Trader.Inventory.RemoveItem(items.Item1, items.Item2);
                    var item = ItemManager.Instance.CreatePlayerItem(items.Item1.Template.Id, items.Item1.Effects, items.Item2);
                    Target.Inventory.AddItem(item, item.Stack);
                }
            }

            if (Inventories.ContainsKey(Target.Actor))
            {
                foreach (var items in Inventories[Target.Actor])
                {
                    Target.Inventory.RemoveItem(items.Item1, items.Item2);
                    var item = ItemManager.Instance.CreatePlayerItem(items.Item1.Template.Id, items.Item1.Effects, items.Item2);
                    Trader.Inventory.AddItem(item, item.Stack);
                }
            }

            Trader.Inventory.SetKamas(-Trader.Kamas);
            Trader.Inventory.SetKamas(Target.Kamas);
            Target.Inventory.SetKamas(-Target.Kamas);
            Target.Inventory.SetKamas(Trader.Kamas);
            Close();
        }

        public Trader GetTrader(RolePlayActor owner)
        {
            return Trader.Actor.Id == owner.Id ? Trader : Target;
        }
    }
}
