﻿using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Client;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Commands.Moderator
{
    [CommandHandler("levelup", RoleEnum.Moderator)]
    public class LevelCommand : WorldCommand
    {
        public override void Execute()
        {          
            if (Parameters.Length < 1)
                Client.Character.SendServerMessage("Unknown level !", Color.Red);
            else
            {
                int level = int.TryParse((string)Parameters[0], out int levelId) ? int.Parse((string)Parameters[0]) : -1;
                Client.Character.LevelUp((byte)level);
            }
        }

        public override string Description
        {
            get { return "Allows change your level."; }
        }
    }
}
