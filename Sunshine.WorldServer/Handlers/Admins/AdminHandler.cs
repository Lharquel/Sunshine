﻿using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Handlers.Admins
{
    public class AdminHandler : WorldPacketHandler
    {
        [WorldHandler(5662)]
        public static void HandleAdminQuietCommandMessage(WorldClient client, AdminQuietCommandMessage message)
        {
            if (client.Account.Role == (sbyte)RoleEnum.Player)
                return;

            if (!string.IsNullOrEmpty(message.content))
            {
                if (message.content.Contains("moveto"))
                {
                    int position = int.TryParse(message.content.Remove(0, 6), out int pos) ? int.Parse(message.content.Remove(0, 6)) : -1;
                    if (position != -1)
                    {
                        short cell = MapManager.Instance.GetMap(position).Cells.FirstOrDefault(x => x.Walkable && x.Id > 100).Id;
                        client.Character.Teleport(position, cell);
                    }
                }
                else
                    client.Character.SendServerMessage($"unknown command : {message.content} !");
            }
        }

        [WorldHandler(76)]
        public static void HandleAdminCommandMessage(WorldClient client, AdminCommandMessage message)
        {
            if (client.Account.Role == (sbyte)RoleEnum.Player)
                return;

            
        }
    }
}
