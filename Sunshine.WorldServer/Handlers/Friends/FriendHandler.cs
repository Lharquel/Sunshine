﻿using Sunshine.Protocol.Messages;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Handlers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.Sunshine.WorldServer.Handlers.Friends
{
    public class FriendHandler : WorldPacketHandler
    {
        [WorldHandler(5676)]
        public static void HandleIgnoredGetListMessage(WorldClient client, IgnoredGetListMessage message)
        {
            FriendHandler.SendIgnoredListMessage(client);
        }

        public static void SendIgnoredListMessage(WorldClient client)
        {
            client.Send(new IgnoredListMessage(new List<IgnoredInformations>()));
        }
    }
}
