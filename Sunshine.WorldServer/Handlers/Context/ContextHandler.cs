﻿using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.WorldServer.Game.Fights.Teams;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Handlers.Basic;
using Sunshine.WorldServer.Handlers.Context;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Handlers.Context
{
    public class ContextHandler : WorldPacketHandler
    {
        [WorldHandler(250)]
        public static void HandleGameContextCreateRequestMessage(WorldClient client, GameContextCreateRequestMessage message)
        {
            ContextHandler.SendGameContextDestroyMessage(client);
            ContextHandler.SendGameContextCreateMessage(client, GameContextEnum.ROLE_PLAY);
            ContextRoleplayHandler.SendCurrentMapMessage(client, client.Character.Map.Id);
        }

        [WorldHandler(255)]
        public static void HandleGameContextQuitMessage(WorldClient client, GameContextQuitMessage message)
        {
            if (client.Character.IsInFight)
                client.Character.LeaveFight(client.Character.Fight.Type);
        }

        [WorldHandler(950)]
        public static void HandleGameMapMovementRequestMessage(WorldClient client, GameMapMovementRequestMessage message)
        {
            client.Character.StartMove(message.keyMovements);
        }

        [WorldHandler(945)]
        public static void HandleGameMapChangeOrientationRequestMessage(WorldClient client, GameMapChangeOrientationRequestMessage message)
        {
            client.Character.Direction = (int)message.direction;
            ContextHandler.SendGameMapChangeOrientationMessage(client);
        }

        [WorldHandler(952)]
        public static void HandleGameMapMovementConfirmMessage(WorldClient client, GameMapMovementConfirmMessage message)
        {
            client.Character.StopMove();
        }

        [WorldHandler(953)]
        public static void HandleGameMapMovementCancelMessage(WorldClient client, GameMapMovementCancelMessage message)
        {
            client.Character.StopMove();
        }

        [WorldHandler(701)]
        public static void HandleGameFightJoinRequestMessage(WorldClient client, GameFightJoinRequestMessage message)
        {
            Fight fight = FightManager.Instance.GetFight(message.fightId);

            if (fight == null)
            {
                ContextHandler.SendChallengeFightJoinRefusedMessage(client, client.Character, FighterRefusedReasonEnum.TOO_LATE);
                return;
            }
            else
            {
                if (fight.Map != client.Character.Map)
                {
                    ContextHandler.SendChallengeFightJoinRefusedMessage(client, client.Character, FighterRefusedReasonEnum.WRONG_MAP);
                }
                else
                {
                    FightActor fighter = fight.GetOneFighter(message.fighterId);
                    if(fighter != null)
                    {
                        client.Character.SetFight(fight.Type, fight);
                        if(fighter.IsAttacker)
                            fight.AddFighter(client.Character.Fighter = new CharacterFighter(client.Character), true);
                        else
                            fight.AddFighter(client.Character.Fighter = new CharacterFighter(client.Character));
                    }
                    else
                        ContextHandler.SendChallengeFightJoinRefusedMessage(client, client.Character, FighterRefusedReasonEnum.WRONG_MAP);
                }
            }
        }

        [WorldHandler(708)]
        public static void HandleGameFightReadyMessage(WorldClient client, GameFightReadyMessage message)
        {
            if (client.Character.IsInFight)
                client.Character.Fighter.SetReadyStatus(message.isReady);
        }

        [WorldHandler(716)]
        public static void HandleGameFightTurnReadyMessage(WorldClient client, GameFightTurnReadyMessage message)
        {

        }

        [WorldHandler(718)]
        public static void HandleGameFightTurnFinishMessage(WorldClient client, GameFightTurnFinishMessage message)
        {
            if (client.Character.IsInFight)
            {
                if (client.Character.Fighter == client.Character.Fight.FighterPlaying)
                    client.Character.Fighter.EndTurn();
            }
        }

        [WorldHandler(5611)]
        public static void HandleShowCellRequestMessage(WorldClient client, ShowCellRequestMessage message)
        {
            if (client.Character.IsInFight)
                client.Character.Fighter.ShowCell(message.cellId);
        }

        [WorldHandler(1005)]
        public static void HandleGameActionFightCastRequestMessage(WorldClient client, GameActionFightCastRequestMessage message)
        {
            if (client.Character.IsInFight)
            {
                var characterSpell = client.Character.Spells.GetSpell(message.spellId);
                if (characterSpell != null)
                {
                    var spells = SpellManager.Instance.Spells[message.spellId];
                    var spell = spells[characterSpell.Level - 1];
                    client.Character.Fighter.CastSpell(spell, message.cellId);
                }
                else
                    Logs.Logger.WriteError($"Fighter {client.Character.Id} try to cast spell {message.spellId} !");
            }
        }

        public static void SendGameFightHumanReadyStateMessage(WorldClient client, FightActor actor)
        {
            client.Send(new GameFightHumanReadyStateMessage(actor.Id, actor.IsReady));
        }

        public static void SendGameContextDestroyMessage(WorldClient client)
        {
            client.Send(new GameContextDestroyMessage());
        }

        public static void SendGameContextCreateMessage(WorldClient client, GameContextEnum context)
        {
            client.Send(new GameContextCreateMessage((sbyte)context));
        }

        public static void SendGameContextRemoveElementMessage(WorldClient client, RolePlayActor actor)
        {
            client.Send(new GameContextRemoveElementMessage(actor.Id));
        }

        public static void SendGameMapChangeOrientationMessage(WorldClient client)
        {
            client.Character.Map.Clients.ForEach(x => x.Send(new GameMapChangeOrientationMessage(new ActorOrientation(client.Character.Id, (sbyte)client.Character.Direction))));
        }

        public static void SendGameMapMovementMessage(List<WorldClient> clients, int actorId, IEnumerable<short> keymovements)
        {
            clients.ForEach(x => x.Send(new GameMapMovementMessage(keymovements, actorId)));
        }

        public static void SendGameMapFightCountMessage(List<WorldClient> clients, short fightCount)
        {
            clients.ForEach(x => x.Send(new MapFightCountMessage(fightCount)));
        }

        public static void SendGameFightStartingMessage(WorldClient client, FightTypeEnum fightType)
        {
            client.Send(new GameFightStartingMessage((sbyte)fightType));
        }

        public static void SendGameFightJoinMessage(WorldClient client, Fight fight)
        {
            client.Send(new GameFightJoinMessage(true, true, false, fight.IsStarted, fight.GetPlacementTimeLeft, (sbyte)fight.Type));
        }

        public static void GameFightStartingMessage(WorldClient client, FightTypeEnum fightType)
        {
            client.Send(new GameFightStartingMessage((sbyte)fightType));
        }

        public static void SendGameFightStartMessage(List<WorldClient> clients)
        {
            clients.ForEach(x => x.Send(new GameFightStartMessage()));
        }

        public static void SendGameFightEndMessage(List<WorldClient> clients, Fight fight, IEnumerable<FightResultListEntry> results)
        {
            clients.ForEach(x => x.Send(new GameFightEndMessage(fight.TimeLine.RoundNumber, 0, results)));
        }

        public static void SendGameFightTurnListMessage(List<WorldClient> clients, Fight fight)
        {
            clients.ForEach(x => x.Send(new GameFightTurnListMessage(fight.GetAliveFightersIds, fight.GetDeadFightersIds)));
        }

        public static void SendGameFightTurnStartMessage(List<WorldClient> clients, FightActor actor)
        {
            clients.ForEach(x => x.Send(new GameFightTurnStartMessage(actor.Id, 35000)));
        }

        public static void SendGameFightTurnEndMessage(List<WorldClient> clients, FightActor actor)
        {
            clients.ForEach(x => x.Send(new GameFightTurnEndMessage(actor.Id)));
        }

        public static void SendGameFightTurnReadyRequestMessage(List<WorldClient> clients, FightActor actor)
        {
            clients.ForEach(x => x.Send(new GameFightTurnReadyRequestMessage(actor.Id)));
        }

        public static void SendChallengeFightJoinRefusedMessage(WorldClient client, Character character, FighterRefusedReasonEnum reason)
        {
            client.Send(new ChallengeFightJoinRefusedMessage(character.Id, (sbyte)reason));
        }

        public static void SendGameFightPlacementPossiblePositionsMessage(WorldClient client, Fight fight)
        {
            var redCells = fight.Map.RedCells.Where(x => x != fight.Map.RedCells.Last());
            var blueCells = fight.Map.BlueCells.Where(x => x != fight.Map.BlueCells.Last());
            client.Send(new GameFightPlacementPossiblePositionsMessage(redCells, blueCells, 2));
        }

        public static void SendGameRolePlayShowChallengeMessage(List<WorldClient> clients, Fight fight)
        {
            clients.ForEach(x => x.Send(new GameRolePlayShowChallengeMessage(fight.GetFightCommonInformations)));
        }

        public static void GameRolePlayRemoveChallengeMessage(List<WorldClient> clients, Fight fight)
        {
            clients.ForEach(x => x.Send(new GameRolePlayRemoveChallengeMessage(fight.Id)));
        }

        public static void SendGameFightShowFighterPreparationMessage(List<WorldClient> clients, List<FightActor> actors)
        {
            foreach (var actor in actors)
                clients.ForEach(x => x.Send(new GameFightShowFighterMessage(actor.GetGameFightFighterPreparationInformations)));
        }

        public static void SendGameFightShowFighterMessage(List<WorldClient> clients, List<FightActor> actors)
        {
            foreach (var actor in actors)
                clients.ForEach(x => x.Send(new GameFightShowFighterMessage(actor.GetGameFightFighterInformations)));
        }

        public static void SendGameFightSynchronizeMessage(List<WorldClient> clients, IEnumerable<FightActor> actors)
        {
            clients.ForEach(x => x.Send(new GameFightSynchronizeMessage(actors.Select(y => y.GetGameFightFighterInformations))));
        }

        public static void SendGameEntitiesDispositionMessage(List<WorldClient> clients, List<FightActor> actors)
        {
            clients.ForEach(x => x.Send(new GameEntitiesDispositionMessage(actors.Select(y => y.GetIdentifiedEntityDispositionInformations))));
        }

        public static void SendGameContextRefreshEntityLookMessage(List<WorldClient> clients, int actorId, EntityLook look)
        {
            clients.ForEach(x => x.Send(new GameContextRefreshEntityLookMessage(actorId, look)));
        }

        public static void SendGameFightUpdateTeamMessage(List<WorldClient> clients, FightTeam team)
        {
            clients.ForEach(x => x.Send(new GameFightUpdateTeamMessage((short)team.Fight.Id, team.GetFightTeamInformations())));
            clients.ForEach(x => x.Send(new GameFightUpdateTeamMessage((short)team.Fight.Id, team.GetFightTeamInformations(true))));
        }

        public static void SendGameFightOptionStateUpdateMessage(List<WorldClient> clients, FightTeam team, FightOptionsEnum option, bool state)
        {
            clients.ForEach(x => x.Send(new GameFightOptionStateUpdateMessage((short)team.Fight.Id, x.Character.Fighter.IsAttacker ? (sbyte)TeamEnum.TEAM_CHALLENGER : (sbyte)TeamEnum.TEAM_CHALLENGER, (sbyte)option, state)));
        }

        public static void SendGameFightNewRoundMessage(List<WorldClient> clients, int roundNumber)
        {
            clients.ForEach(x => x.Send(new GameFightNewRoundMessage(roundNumber)));
        }

        public static void SendShowCellMessage(IEnumerable<WorldClient> clients, FightActor source, short cellId)
        {
            foreach (var client in clients)
                client.Send(new ShowCellMessage(source.Id, cellId));
        }

        public static void SendGameActionFightSpellCastMessage(List<WorldClient> clients, ActionsEnum actionId, FightActor caster, Spell spell, short cellId, FightSpellCastCriticalEnum critical, bool silentCast)
        {
            clients.ForEach(x => x.Send(new GameActionFightSpellCastMessage((short)actionId, caster.Id, cellId, (sbyte)critical, silentCast, (short)spell.Id, spell.Level)));
        }
    }
}
