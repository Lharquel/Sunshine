﻿using Sunshine.WorldServer.Client;
using System;
using System.Collections.Generic;
using Sunshine.Protocol.Messages;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Types;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Items;
using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Exchanges;
using Sunshine.WorldServer.Game.Actors.Npcs;
using Sunshine.WorldServer.Game.Actors.Npcs.Actions;
using Sunshine.WorldServer.Game.BidsHouse;

namespace Sunshine.WorldServer.Handlers.Characters.Inventory
{
    public class InventoryHandler : WorldPacketHandler
    {      
        [WorldHandler(5608)]
        public static void HandleSpellUpgradeRequestMessage(WorldClient client, SpellUpgradeRequestMessage message)
        {
            client.Character.Spells.BoostSpell(message.spellId);
            client.Character.RefreshStats();
        }

        [WorldHandler(3021)]
        public static void HandleObjectSetPositionMessage(WorldClient client, ObjectSetPositionMessage message)
        {
            var item = client.Character.Inventory.GetItemUid(message.objectUID);
            client.Character.Inventory.MoveItem(item, (CharacterInventoryPositionEnum)message.position);
        }

        [WorldHandler(3022)]
        public static void HandleObjectDeleteMessage(WorldClient client, ObjectDeleteMessage message)
        {
            var item = client.Character.Inventory.GetItemUid(message.objectUID);
            client.Character.Inventory.RemoveItem(item, message.quantity);
        }

        [WorldHandler(5773)]
        public static void HandleExchangePlayerRequestMessage(WorldClient client, ExchangePlayerRequestMessage message)
        {
            ExchangeTypeEnum exchangeType = (ExchangeTypeEnum)message.exchangeType;
            if (exchangeType != ExchangeTypeEnum.PLAYER_TRADE)
                InventoryHandler.SendExchangeErrorMessage(client, ExchangeErrorEnum.REQUEST_IMPOSSIBLE);
            else
            {
                Character target = CharacterManager.Instance.GetCharacter(message.target);
                if (target == null)
                    InventoryHandler.SendExchangeErrorMessage(client, ExchangeErrorEnum.BID_SEARCH_ERROR);
                else
                {
                    if (target.Map.Id != client.Character.Map.Id)
                        InventoryHandler.SendExchangeErrorMessage(client, ExchangeErrorEnum.REQUEST_CHARACTER_TOOL_TOO_FAR);
                    else
                    {
                        if (target.IsInTrade)
                            InventoryHandler.SendExchangeErrorMessage(client, ExchangeErrorEnum.REQUEST_CHARACTER_OCCUPIED);
                        else
                            client.Character.SetTradeRequest(exchangeType, target);
                    }
                }
            }
        }

        [WorldHandler(5508)]
        public static void HandleExchangeAcceptMessage(WorldClient client, ExchangeAcceptMessage message)
        {
            if (client.Character.IsInTrade && client.Character.Trade is PlayerTrade)
                (client.Character.Trade as PlayerTrade).Open();
        }

        [WorldHandler(5520)]
        public static void HandleExchangeObjectMoveKamaMessage(WorldClient client, ExchangeObjectMoveKamaMessage message)
        {
            if (client.Character.IsInTrade)
                client.Character.Trade.SetKamas(client.Character, message.quantity);
        }

        [WorldHandler(5518)]
        public static void HandleExchangeObjectMoveMessage(WorldClient client, ExchangeObjectMoveMessage message)
        {
            if (client.Character.IsInTrade)
                client.Character.Trade.MoveItem(client.Character, message.objectUID, message.quantity);
            else if (client.Character.IsInDialog && client.Character.Dialog is NpcSellAction)
                client.Character.BidHouseBag.MoveItem(message.objectUID, message.quantity);
        }

        [WorldHandler(5511)]
        public static void HandleExchangeReadyMessage(WorldClient client, ExchangeReadyMessage message)
        {
            if (client.Character.IsInTrade)
                client.Character.Trade.SetReadyStatus(client.Character, message.ready);
        }

        [WorldHandler(5774)]
        public static void HandleExchangeBuyMessage(WorldClient client, ExchangeBuyMessage message)
        {
            if (client.Character.IsInDialog && client.Character.Dialog is NpcBuySellAction)
                (client.Character.Dialog as NpcBuySellAction).BuyItem(message.objectToBuyId, message.quantity);
        }

        [WorldHandler(5805)]
        public static void HandleExchangeBidHousePriceMessage(WorldClient client, ExchangeBidHousePriceMessage message)
        {
            client.Send(new ObjectsQuantityMessage(new List<ObjectItemQuantity>()));
            client.Send(new ExchangeBidPriceMessage(message.genId, client.Character.Level));
        }

        [WorldHandler(5803)]
        public static void HandleExchangeBidHouseTypeMessage(WorldClient client, ExchangeBidHouseTypeMessage message)
        {
            if(client.Character.IsInDialog && client.Character.Dialog is NpcBuyAction)
            {                
                var bidHouse = BidHouseManager.Instance.BidsHouse[client.Character.Map.Id];
                InventoryHandler.SendExchangeTypesExchangerDescriptionForUserMessage(client, bidHouse, message.type);
            }
        }

        [WorldHandler(5514)]
        public static void HandleExchangeObjectMovePricedMessage(WorldClient client, ExchangeObjectMovePricedMessage message)
        {
            if (message.quantity > 0 && message.price > 0)
            {
                var item = client.Character.Inventory.GetItemUid(message.objectUID);
                if (item != null)
                {
                    var bidHouse = BidHouseManager.Instance.BidsHouse[client.Character.Map.Id];
                    if (bidHouse.GetSellerBuyerDescriptor().types.Contains((int)item.Type))
                    {
                        int taxBidHouse = Math.Abs(message.price / 100);
                        if (client.Character.Inventory.Kamas > taxBidHouse)
                        {
                            client.Character.Inventory.SetKamas(-taxBidHouse);
                            bidHouse.SellItem(client.Character, item, message.quantity, message.price);
                        }
                        else
                            client.Character.SendInformationMessage(TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 65, new object[0]);
                    }
                    else
                        client.Character.SendInformationMessage(TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 64, new object[0]);
                }
            }
        }

        [WorldHandler(5807)]
        public static void HandleExchangeBidHouseListMessage(WorldClient client, ExchangeBidHouseListMessage message)
        {
            if (client.Character.IsInDialog && client.Character.Dialog is NpcBuyAction)
            {
                var bidHouse = BidHouseManager.Instance.BidsHouse[client.Character.Map.Id];
                InventoryHandler.SendExchangeTypesItemsExchangerDescriptionForUserMessage(client, bidHouse);
            }
        }

        [WorldHandler(5804)]
        public static void HandleExchangeBidHouseBuyMessage(WorldClient client, ExchangeBidHouseBuyMessage message)
        {
            if (client.Character.IsInDialog && client.Character.Dialog is NpcBuyAction)
            {
                var bidHouse = BidHouseManager.Instance.BidsHouse[client.Character.Map.Id];
                bidHouse.BuyItem(client.Character, message.uid, message.qty, message.price);
            }
        }

        [WorldHandler(6002)]
        public static void HandleExchangeReplayMessage(WorldClient client, ExchangeReplayMessage message)
        {
            if (client.Character.IsInTrade && client.Character.Trade is CraftTrade)
                (client.Character.Trade as CraftTrade).UpdateReplay(message.count);
        }

        [WorldHandler(6001)]
        public static void HandleExchangeReplayStopMessage(WorldClient client, ExchangeReplayStopMessage message)
        {
            if (client.Character.IsInTrade && client.Character.Trade is CraftTrade)
                (client.Character.Trade as CraftTrade).Stop();
        }

        public static void SendExchangeErrorMessage(WorldClient client, ExchangeErrorEnum errorEnum)
        {
            client.Send(new ExchangeErrorMessage((sbyte)errorEnum));
        }

        public static void SendExchangeBuyOkMessage(WorldClient client)
        {
            client.Send(new ExchangeBuyOkMessage());
        }

        public static void SendExchangeStartOkCraftWithInformationMessage(WorldClient client, sbyte nbCase, int skill)
        {
            client.Send(new ExchangeStartOkCraftWithInformationMessage(nbCase, skill));
        }

        public static void SendExchangeReplayCountModifiedMessage(WorldClient client, int count)
        {
            client.Send(new ExchangeReplayCountModifiedMessage(count));
        }

        public static void SendExchangeReplayStopMessage(WorldClient client)
        {
            client.Send(new ExchangeReplayStopMessage());
        }

        public static void SendExchangeItemAutoCraftRemainingMessage(WorldClient client, int count)
        {
            client.Send(new ExchangeItemAutoCraftRemainingMessage(count));
        }

        public static void SendExchangeCraftResultWithObjectDescMessage(WorldClient client, BasePlayerItem item, CraftResultEnum result)
        {
            client.Send(new ExchangeCraftResultWithObjectDescMessage((sbyte)result, item.GetObjectItemNotInContainer()));
        }

        public static void SendExchangeItemAutoCraftStopedMessage(WorldClient client, ExchangeReplayStopReasonEnum reason)
        {
            client.Send(new ExchangeItemAutoCraftStopedMessage((sbyte)reason));
        }

        public static void SendExchangeRequestedTradeMessage(WorldClient client, ExchangeTypeEnum type, Character source, Character target)
        {
            client.Send(new ExchangeRequestedTradeMessage((sbyte)type, source.Id, target.Id));
        }

        public static void SendExchangeLeaveMessage(WorldClient client, bool apply)
        {
            client.Send(new ExchangeLeaveMessage(apply));
        }

        public static void SendExchangeStartedWithPodsMessage(WorldClient client, ExchangeTypeEnum type, Trader source, Trader target)
        {
            client.Send(new ExchangeStartedWithPodsMessage((sbyte)type, source.Id, source.Inventory.Weight(),
                source.Inventory.WeightTotal(), target.Id, target.Inventory.Weight(), target.Inventory.WeightTotal()));
        }

        public static void SendExchangeStartOkNpcShopMessage(WorldClient client, Npc npc)
        {
            client.Send(new ExchangeStartOkNpcShopMessage(npc.Id, npc.Record.Token, npc.GetObjectItemToSellInNpcShops));               
        }

        public static void SendExchangeStartOkNpcTradeMessage(WorldClient client, Npc npc)
        {
            client.Send(new ExchangeStartOkNpcTradeMessage(npc.Id));
        }

        public static void SendExchangeStartedBidBuyerMessage(WorldClient client)
        {
            if (client.Character.IsInDialog && client.Character.Dialog is NpcBuyAction)
            {
                var bidHouse = BidHouseManager.Instance.BidsHouse[client.Character.Map.Id];
                client.Send(new ExchangeStartedBidBuyerMessage(bidHouse.GetSellerBuyerDescriptor()));
            }
        }

        public static void SendExchangeStartedBidSellerMessage(WorldClient client)
        {
            if (client.Character.IsInDialog && client.Character.Dialog is NpcSellAction)
            {
                var bidHouse = BidHouseManager.Instance.BidsHouse[client.Character.Map.Id];
                client.Send(new ExchangeStartedBidSellerMessage(bidHouse.GetSellerBuyerDescriptor(), client.Character.BidHouseBag.GetObjectItemsToSell()));
            }
        }

        public static void SendExchangeBidHouseItemAddOkMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ExchangeBidHouseItemAddOkMessage(item.GetObjectItemToSell()));
        }

        public static void SendExchangeBidHouseItemRemoveOkMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ExchangeBidHouseItemRemoveOkMessage(item.Template.Id));
        }

        public static void SendExchangeBidHouseInListRemovedMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ExchangeBidHouseInListRemovedMessage(item.Id));
        }

        public static void SendExchangeBidHouseInListAddedMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ExchangeBidHouseInListAddedMessage(item.Id, item.Template.Id, 0, false, item.Effects.Select(x => x.GetObjectEffectInteger()), BidHouseManager.Instance.BidsHousePrices[item]));
        }

        public static void SendExchangeTypesItemsExchangerDescriptionForUserMessage(WorldClient client, BidHouse bidHouse)
        {
            client.Send(new ExchangeTypesItemsExchangerDescriptionForUserMessage(bidHouse.GetBidExchangerObjectsInfo()));
        }

        public static void SendExchangeTypesItemsExchangerDescriptionForUserMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ExchangeTypesItemsExchangerDescriptionForUserMessage(new List<BidExchangerObjectInfo> { item.GetBidExchangerObjectInfo() }));
        }

        public static void SendExchangeTypesExchangerDescriptionForUserMessage(WorldClient client, BidHouse bidHouse, int type)
        {
            client.Send(new ExchangeTypesExchangerDescriptionForUserMessage(bidHouse.GetItems((ItemTypeEnum)type)));
        }

        public static void SendExchangeKamaModifiedMessage(WorldClient client, bool remote, int quantity)
        {
            client.Send(new ExchangeKamaModifiedMessage(remote, quantity));
        }

        public static void SendExchangeObjectAddedMessage(WorldClient client, bool remote, BasePlayerItem item)
        {
            client.Send(new ExchangeObjectAddedMessage(remote, item.GetObjectItem()));
        }

        public static void SendExchangeObjectModifiedMessage(WorldClient client, bool remote, BasePlayerItem item)
        {
            client.Send(new ExchangeObjectModifiedMessage(remote, item.GetObjectItem()));
        }

        public static void SendExchangeObjectRemovedMessage(WorldClient client, bool remote, BasePlayerItem item)
        {
            client.Send(new ExchangeObjectRemovedMessage(remote, item.Id));
        }

        public static void SendExchangeIsReadyMessage(WorldClient client, Trader trader, bool ready)
        {
            client.Send(new ExchangeIsReadyMessage(trader.Id, ready));
        }

        public static void SendKamasUpdateMessage(WorldClient client, int kamasTotal)
        {
            client.Send(new KamasUpdateMessage(kamasTotal));
        }

        public static void SendSpellUpgradeSuccessMessage(WorldClient client, SpellItem spellItem)
        {
            client.Send(new SpellUpgradeSuccessMessage(spellItem.spellId, spellItem.spellLevel));
        }
        public static void SendSpellUpgradeFailureMessage(WorldClient client)
        {
            client.Send(new SpellUpgradeFailureMessage());
        }

        public static void SendSpellListMessage(WorldClient client, bool spellPrevisualization)
        {
            client.Send(new SpellListMessage(spellPrevisualization, 
                from entry in client.Character.Spells.GetSpells()
                select entry.GetSpellItem()));               
        }

        public static void SendInventoryContentMessage(WorldClient client)
        {
            client.Send(new InventoryContentMessage(
                from entry in client.Character.Inventory.GetItems()
                select entry.GetObjectItem(), (int)client.Character.Inventory.Kamas));

        }

        public static void SendInventoryWeightMessage(WorldClient client)
        {
            client.Send(new InventoryWeightMessage(client.Character.Inventory.Weight(), 
                                                   client.Character.Inventory.WeightTotal()));
        }

        public static void SendObjectAddedMessage(WorldClient client, BasePlayerItem item)
        {        
            client.Send(new ObjectAddedMessage(item.GetObjectItem()));
        }

        public static void SendObjectsAddedMessage(WorldClient client, System.Collections.Generic.IEnumerable<BasePlayerItem> addeditems)
        {
            client.Send(new ObjectsAddedMessage(
                from entry in addeditems
                select entry.GetObjectItem()));
        }

        public static void SendObjectDeletedMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ObjectDeletedMessage(item.Id));
        }

        public static void SendObjectModifiedMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ObjectModifiedMessage(item.GetObjectItem()));
        }

        public static void SendObjectQuantityMessage(WorldClient client, BasePlayerItem item, int quantity)
        {
            client.Send(new ObjectQuantityMessage(item.Id, quantity));
        }

        public static void SendObjectDropMessage(WorldClient client, BasePlayerItem item, int quantity)
        {           
            client.Send(new ObjectDropMessage(item.Id, quantity));
        }

        public static void SendObjectMovementMessage(WorldClient client, BasePlayerItem item)
        {
            client.Send(new ObjectMovementMessage(item.Id, (byte)item.Position));
        }

        public static void SendObjectErrorMessage(WorldClient client, ObjectErrorEnum error)
        {
            client.Send(new ObjectErrorMessage((sbyte)error));
        }
    }
}
