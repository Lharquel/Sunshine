﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using Sunshine.Protocol.Types;
using Sunshine.Protocol.Utils;
using Sunshine.WorldServer.Handlers;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Handlers.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Handlers.Characters;
using Sunshine.MySql.Database.Auth;
using Sunshine.WorldServer.Game;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Handlers.Chat;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using Sunshine.WorldServer.Handlers.Characters.Inventory;
using Sunshine.WorldServer.Handlers.Characters.Shorcuts;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Handlers.PvP;
using Sunshine.WorldServer.Handlers.Characters.Jobs;

namespace Sunshine.WorldServer.Handlers.Characters
{
    public class CharacterHandler : WorldPacketHandler
    {
        [WorldHandler(150)]
        public static void HandleCharactersListRequestMessage(WorldClient client, CharactersListRequestMessage message)
        {
            if (client.Account != null && client.Account.Username != "")
            {
                if(client.Characters.Length <= 0)
                    client.Send(new CharactersListMessage(false, new List<CharacterBaseInformations>()));
                else
                    CharacterHandler.SendCharactersListMessage(client, false);
            }
            else
                client.Send(new IdentificationFailedMessage(4));
        }

        [WorldHandler(160)]
        public static void HandleCharacterCreationRequestMessage(WorldClient client, CharacterCreationRequestMessage message)
        {
            if (client.Characters.Length < 5)
            {
                if (CharacterManager.Instance.DoesNameExist(message.name))
                {
                    client.Send(new CharacterCreationResultMessage((sbyte)CharacterCreationResultEnum.ERR_NAME_ALREADY_EXISTS));
                    return;
                }

                CharacterRecord _character = new CharacterRecord
                {
                    Level = 1,
                    Name = message.name,
                    Breed = message.breed,
                    Sex = message.sex,
                    EntityLook = EntityManager.Instance.BuildEntityLook(message.breed, message.sex, message.colors.ToList()),
                    MapId = BreedManager.Instance.GetStartMap(message.breed),
                    CellId = BreedManager.Instance.GetStartCell(message.breed),
                    Direction = BreedManager.Instance.GetStartDirection(message.breed),
                    SpellsPoints = 1
                };

                CharacterManager.Instance.CreateCharacter(client.Account.Id, _character);
                client.LastCharacter = AccountManager.Instance.GetCharacter(message.name).Id;
                CharacterManager.Instance.Characters.Add(client.LastCharacter.Value, new Character(_character));
                client.Send(new CharacterCreationResultMessage((sbyte)CharacterCreationResultEnum.OK));
                CharacterHandler.SendCharactersListMessage(client, false);
            }
            else
                client.Send(new CharacterCreationResultMessage((sbyte)CharacterCreationResultEnum.ERR_TOO_MANY_CHARACTERS));
        }

        [WorldHandler(152)]
        public static void HandleCharacterSelectionMessage(WorldClient client, CharacterSelectionMessage message)
        {
            CharacterHandler.CommonCharacterSelection(client, client.LastCharacter.HasValue ? client.LastCharacter.Value : message.id);
            client.LastCharacter = null;
        }

        [WorldHandler(162)]
        public static void HandleCharacterNameSuggestionRequestMessage(WorldClient client, CharacterNameSuggestionRequestMessage message)
        {
            string nameGenerate = CharacterManager.Instance.GenerateName();
            client.Send(new CharacterNameSuggestionSuccessMessage(nameGenerate));
        }

        [WorldHandler(165)]
        public static void HandleCharacterDeletionRequestMessage(WorldClient client, CharacterDeletionRequestMessage message)
        {
            Character character = CharacterManager.Instance.Characters[message.characterId];

            if (character != null && character.Account.Id == client.Account.Id)
            {
                if (client.Account.SecretAnswer != null)
                {
                    string secretAnswerHash = Utils.GetMD5Hash((message.characterId + "~" + client.Account.SecretAnswer));
                    secretAnswerHash = Utils.GetMD5Hash(client.Account.SecretAnswer);
                    //if (message.secretAnswerHash == secretAnswerHash)
                    // {
                    CharacterManager.Instance.DeleteCharacterOnAccount(client, character);
                    CharacterHandler.SendCharactersListMessage(client, false);
                    // }
                    //else
                    //    client.Send(new CharacterDeletionErrorMessage(3));
                }
                else
                    client.Send(new CharacterDeletionErrorMessage(3));
            }
            else
                client.Send(new CharacterDeletionErrorMessage(1));
        }

        public static void SendCharactersListMessage(WorldClient client, bool tuto)
        {
            List<CharacterBaseInformations> chrBaseInformations = new List<CharacterBaseInformations>();

            foreach (var character in client.Characters)
                chrBaseInformations.Add(CharacterManager.Instance.Characters[character.Id].GetCharacterBaseInformations);

            client.Send(new CharactersListMessage(tuto, chrBaseInformations));
        }

        public static void CommonCharacterSelection(WorldClient client, int characterId)
        {
            client.Character = CharacterManager.Instance.Characters[characterId];
            client.Character.Client = client;
            if (client.Character != null)
            {
                client.Send(new CharacterSelectedSuccessMessage(client.Character.GetCharacterBaseInformations));              
                InventoryHandler.SendInventoryContentMessage(client);
                ShortcutHandler.SendShortcutBarContentMessage(client, ShortcutBarEnum.SPELL);
                ShortcutHandler.SendShortcutBarContentMessage(client, ShortcutBarEnum.OBJECT);
                ContextRoleplayHandler.SendEmoteListMessage(client);
                ChatHandler.SendEnabledChannelsMessage(client);
                PvPHandler.SendAlignmentRankUpdateMessage(client);
                PvPHandler.SendAlignmentSubAreasListMessage(client);
                InventoryHandler.SendSpellListMessage(client, true);
                InventoryHandler.SendInventoryWeightMessage(client);
                CharacterHandler.SendCharacterStatsListMessage(client);
                JobHandler.SendJobDescriptionMessage(client);
                JobHandler.SendJobExperienceUpdateMessage(client);
                JobHandler.SendJobCrafterDirectorySettingsMessage(client);
                client.Character.EnterWorld();
            }
            else
                client.Send(new CharacterSelectedErrorMessage());
        }

        public static void SendCharacterStatsListMessage(WorldClient client)
        {
            client.Send(new CharacterStatsListMessage(client.Character.GetCharacterCharacteristicsInformations));
        }

        public static void SendLifePointsRegenBeginMessage(WorldClient client, byte regenRate)
        {
            client.Send(new LifePointsRegenBeginMessage(regenRate));
        }

        public static void SendUpdateLifePointsMessage(WorldClient client)
        {
            client.Send(new UpdateLifePointsMessage(client.Character.Stats.Health.Total,
                client.Character.Stats.Health.TotalMax));
        }

        public static void SendLifePointsRegenEndMessage(WorldClient client, int lifePointsGained)
        {
            client.Send(new LifePointsRegenEndMessage(client.Character.Stats.Health.Total, 
                client.Character.Stats.Health.TotalMax, lifePointsGained));
        }

        public static void SendCharacterLevelUpMessage(WorldClient client, byte newLevel)
        {
            client.Send(new CharacterLevelUpMessage(newLevel));

        }
        public static void SendCharacterLevelUpInformationMessage(WorldClient client, Character character, byte level)
        {
            client.Send(new CharacterLevelUpInformationMessage(level, character.Name, character.Id, 0));
        }
    }
}
