﻿using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Actors.Fighters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Handlers.Actions
{
    public class ActionsHandler : WorldPacketHandler
    {
        [WorldHandler(957)]
        public static void HandleGameActionAcknowledgementMessage(WorldClient client, GameActionAcknowledgementMessage message)
        {

        }

        public static void SendGameActionFightPointsVariationMessage(List<WorldClient> clients, ActionsEnum action, FightActor source, FightActor target, short delta)
        {
            clients.ForEach(x => x.Send(new GameActionFightPointsVariationMessage((short)action, source.Id, target.Id, delta)));
        }

        public static void SendGameActionFightLifePointsLostMessage(List<WorldClient> clients, FightActor source, FightActor target, short delta)
        {
            clients.ForEach(x => x.Send(new GameActionFightLifePointsVariationMessage((short)ActionsEnum.ACTION_CHARACTER_LIFE_POINTS_LOST, source.Id, target.Id, delta)));
        }

        public static void SendGameActionFightShieldPointsVariationMessage(List<WorldClient> clients, FightActor source, FightActor target, short delta)
        {
            clients.ForEach(x => x.Send(new GameActionFightShieldPointsVariationMessage(1041, source.Id, target.Id, delta)));
        }

        public static void SendGameActionFightDeathMessage(List<WorldClient> clients, FightActor fighter, FightActor target)
        {
            clients.ForEach(x => x.Send(new GameActionFightDeathMessage(103, fighter.Id, target.Id)));
        }

        public static void SendGameActionFightDeathMessage(List<WorldClient> clients, FightActor fighter)
        {
            clients.ForEach(x => x.Send(new GameActionFightDeathMessage(103, fighter.Id, fighter.Id)));
        }

        public static void SendGameActionFightKillMessage(List<WorldClient> clients, FightActor fighter)
        {
            clients.ForEach(x => x.Send(new GameActionFightKillMessage(141, fighter.Id, fighter.Id)));
        }

        public static void SendGameActionFightChangeLookMessage(List<WorldClient> clients, FightActor source, FightActor target, EntityLook look)
        {
            clients.ForEach(x => x.Send(new GameActionFightChangeLookMessage(149, source.Id, target.Id, look)));
        }

        public static void SendGameActionFightExchangePositionsMessage(List<WorldClient> clients, FightActor caster, FightActor target)
        {
            clients.ForEach(x => x.Send(new GameActionFightExchangePositionsMessage(8, caster.Id, target.Id, caster.Position.Cell, target.Position.Cell)));
        }

        public static void SendGameActionFightReflectSpellMessage(List<WorldClient> clients, FightActor source, FightActor target)
        {
            clients.ForEach(x => x.Send(new GameActionFightReflectSpellMessage(106, source.Id, target.Id)));
        }

        public static void SendGameActionFightTeleportOnSameMapMessage(List<WorldClient> clients, FightActor source, FightActor target, short destination)
        {
            clients.ForEach(x => x.Send(new GameActionFightTeleportOnSameMapMessage(4, source.Id, target.Id, destination)));
        }

        public static void SendGameActionFightSlideMessage(List<WorldClient> clients, FightActor source, FightActor target, short startCell, short endCell)
        {
            clients.ForEach(x => x.Send(new GameActionFightSlideMessage(5, source.Id, target.Id, startCell, endCell)));
        }

        public static void SendSequenceStartMessage(List<WorldClient> clients, FightActor actor, SequenceTypeEnum sequenceType)
        {
            clients.ForEach(x => x.Send(new SequenceStartMessage((sbyte)sequenceType, actor.Id)));
        }

        public static void SendSequenceStartMessage(WorldClient client, int actor, SequenceTypeEnum sequenceType)
        {
            client.Send(new SequenceStartMessage((sbyte)sequenceType, actor));
        }

        public static void SendSequenceEndMessage(List<WorldClient> clients, FightActor actor, SequenceTypeEnum sequenceType, ActionsEnum actionType)
        {
            clients.ForEach(x => x.Send(new SequenceEndMessage((short)actionType, actor.Id, (sbyte)sequenceType)));
        }

        public static void SendSequenceEndMessage(WorldClient client, int actor, SequenceTypeEnum sequenceType, ActionsEnum actionType)
        {
            client.Send(new SequenceEndMessage((short)actionType, actor, (sbyte)sequenceType));
        }
    }
}
