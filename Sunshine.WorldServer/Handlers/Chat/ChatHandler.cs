﻿using Sunshine.WorldServer.Client;
using System;
using System.Collections.Generic;
using Sunshine.Protocol.Utils.Extensions;
using System.Linq;
using System.Text;
using Sunshine.Protocol.Messages;
using System.Threading.Tasks;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.Servers;
using Sunshine.WorldServer.Commands;

namespace Sunshine.WorldServer.Handlers.Chat
{
    public class ChatHandler : WorldPacketHandler
    {
        [WorldHandler(890)]
        public static void HandleChannelEnablingMessage(WorldClient client, ChannelEnablingMessage message)
        {
        }

        [WorldHandler(800)]
        public static void HandleChatSmileyRequestMessage(WorldClient client, ChatSmileyRequestMessage message)
        {
            client.Character.Map.Clients.ForEach(x => x.Send(new ChatSmileyMessage(client.Character.Id, message.smileyId, client.Account.Id)));
        }

        [WorldHandler(861)]
        public static void HandleChatClientMultiMessage(WorldClient client, ChatClientMultiMessage message)
        {
            if (!string.IsNullOrEmpty(message.content) && message.content[0] == '.')
                CommandDispatcher.Dispatch(client, message.content);
            else
                ChatHandler.SendChatClientMultiMessage(ServersManager.Instance.GetClients<WorldClient>(), client.Character, message);
        }

        [WorldHandler(851)]
        public static void HandleChatClientPrivateMessage(WorldClient client, ChatClientPrivateMessage message)
        {
            if (!string.IsNullOrEmpty(message.content))
            {
                Character characterReceiver = WorldServerManager.Instance.GetCharacter(message.receiver);
                if (characterReceiver != null)
                {
                    if(client.Character.Name == characterReceiver.Name)
                        ChatHandler.SendChatErrorMessage(client, ChatErrorEnum.CHAT_ERROR_INTERIOR_MONOLOGUE);
                    else
                        ChatHandler.SendChatClientPrivateMessage(client, characterReceiver, message);
                }
                else
                    ChatHandler.SendChatErrorMessage(client, ChatErrorEnum.CHAT_ERROR_RECEIVER_NOT_FOUND);
            }
        }

        public static void SendEnabledChannelsMessage(WorldClient client)
        {
            client.Send(new EnabledChannelsMessage(new List<sbyte> { 0, 1, 2, 3, 4, 5, 6, 8, 9, 10, 11 }, new List<sbyte>()));
        }

        public static void SendChatServerMessage(WorldClient client, ChatActivableChannelsEnum channel, string content, int senderid, string sendername, int accountid)
        {
            if (!string.IsNullOrEmpty(content))
                client.Send(new ChatServerMessage((sbyte)channel, content, System.DateTime.Now.GetUnixTimeStamp(), "", senderid, sendername, accountid));
        }

        public static void SendChatErrorMessage(WorldClient client, ChatErrorEnum error)
        {
            client.Send(new ChatErrorMessage((sbyte)error));
        }

        private static void SendChatClientMultiMessage(IEnumerable<WorldClient> clients, Character sender, ChatClientMultiMessage message)
        {
            var clientsConnected = clients.Where(x => message.channel == 0 ? (x.Character != null && x.Character.Map == sender.Map) : x.Character != null);
            if ((message.channel == (sbyte)ChatActivableChannelsEnum.CHANNEL_PARTY) && sender.IsInParty)
                clientsConnected = clientsConnected.Where(x => x.Character.IsInParty && x.Character.Party.Id == sender.Party.Id);

            clientsConnected.ToList().ForEach(x => SendChatServerMessage(x, (ChatActivableChannelsEnum)message.channel, message.content, sender.Id, sender.Name, sender.Account.Id));

        }

        private static void SendChatClientPrivateMessage(WorldClient client, Character receiver, ChatClientPrivateMessage message)
        {
            client.Send(new ChatServerCopyMessage((sbyte)ChatActivableChannelsEnum.PSEUDO_CHANNEL_PRIVATE, message.content, System.DateTime.Now.GetUnixTimeStamp(), "", receiver.Id, receiver.Name));
            ChatHandler.SendChatServerMessage(receiver.Client, ChatActivableChannelsEnum.PSEUDO_CHANNEL_PRIVATE, message.content, client.Character.Id, client.Character.Name, client.Character.Account.Id);
        }

    }
}
