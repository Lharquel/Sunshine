﻿using Sunshine.BaseClient;
using Sunshine.Logs;
using Sunshine.MySql.Database.Auth;
using Sunshine.MySql.Database.Auth.Accounts;
using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.Protocol.IO;
using Sunshine.Protocol.Messages;
using Sunshine.Servers;
using Sunshine.WorldServer.Game;
using Sunshine.WorldServer.Game.BidsHouse;
using Sunshine.WorldServer.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Client
{
    public class WorldClient : IBaseClient
    {     
        #region Socket
        private Socket _client;
        private byte[] _buffer = new byte[8192];
        public string Ip { get; set; } 
        public int Port { get; set; }

        public void Initialize()
        {
            _client.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), _client);
        }

        public void Disconnect()
        {
            Logger.WriteInfo(string.Format("Client <{0}:{1}> disconnected from WorldServer...", Ip, Port));
            ServersManager.Instance.RemoveClient(this);
        }

        private void ReceiveCallBack(IAsyncResult result)
        {
            _client = (Socket)result.AsyncState;

            if (!this.Connected())
            {
                this.Disconnect();
                return;
            }

            int size = _client.EndReceive(result);
            byte[] buffer = new byte[size];
            Array.Copy(_buffer, buffer, buffer.Length);
            MessageDispatcher.Dispatch(this, buffer);
            _client.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallBack), _client);
        }

        private void SendCallBack(IAsyncResult result)
        {
            try
            {
                _client = (Socket)result.AsyncState;
                _client.EndSend(result);
            }
            catch (Exception e)
            {
                return;
            }
        }

        public void Send(Message message)
        {
            try
            {
                using (BigEndianWriter writer = new BigEndianWriter())
                {
                    message.Pack(writer);
                    _client.BeginSend(writer.Data, 0, writer.Data.Length, SocketFlags.None, new AsyncCallback(SendCallBack), _client);
                    Logger.WriteServer("WorldServer", WorldServer.Ip, WorldServer.Port, message.ToString());
                }
            }
            catch (Exception e)
            {
                Logger.WriteError(e.ToString());
                Disconnect();
                return;
            }
        }

        private bool Connected()
        {
            if ((_client != null && _client.Connected))
            {
                try
                {
                    if ((_client.Poll(0, SelectMode.SelectRead)))
                    {
                        if (_client.Receive(new byte[1], SocketFlags.Peek) == 0)
                        {
                            return false;
                        }
                    }
                    return true;
                }
                catch (SocketException ex)
                {
                    return false;
                }
            }
            else
                return false;
        }
        #endregion

        public WorldClient(Socket client)
        {
            _client = client;
            Ip = ((IPEndPoint)_client.RemoteEndPoint).Address.ToString();
            Port = ((IPEndPoint)_client.RemoteEndPoint).Port;
        }

        public Account Account { get; set; }

        public CharacterRecord[] Characters
            => AccountManager.Instance.GetCharacters(Account.Id);

        public Character Character { get; set; }

        public int? LastCharacter { get; set; }
    }
}
