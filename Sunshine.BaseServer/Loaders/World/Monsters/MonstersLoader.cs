﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Monsters;
using Sunshine.WorldServer.Game.Actors.Monsters;
using Sunshine.WorldServer.Game.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.BaseServer.Loaders.World.Monsters
{
    public static class MonstersLoader
    {
        public static void Initialize()
        {
            Logs.Logger.Write("[ World ] Loading MonsterSpawns...");
            IEnumerable<MonsterSpawn> monsterSpawns = MonsterManager.Instance.GetMonsterSpawns();
            foreach(var monster in monsterSpawns)
            {
                var maps = MapManager.Instance.Maps.Where(x => x.Value.SubAreaId == monster.SubArea && x.Value.Cells != null
                                                                                && x.Value.Cells.Count() > 0);
                foreach(var map in maps.Where(x => !x.Value.IsDungeon))
                {
                    var monsterIds = monster.MonstersCSV.Split(',').Select(x => int.Parse(x));
                    List<Monster> monsters = monsterIds.Select(x => new Monster(MonsterManager.Instance.GetMonster(x), MonsterManager.Instance.GetMonsterGrades(x))).ToList();
                    for (int i = 0; i < 3; i++)
                        map.Value.RolePlayActors.Add(new MonsterGroup(monsters, map.Value));
                }
            }                       
            Logs.Logger.Write($"[ World ] {monsterSpawns.Count()} Monsters Spawned");
        }
    }
}
