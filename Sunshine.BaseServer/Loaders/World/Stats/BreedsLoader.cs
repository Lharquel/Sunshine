﻿using Sunshine.MySql.Database.Managers;
using System;
using System.Collections.Generic;
using Sunshine.Protocol.Enums;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Tools.D2o;
using Sunshine.Protocol.Tools.D2o.Classes;

namespace Sunshine.BaseServer.Loaders.World.Stats
{
    public static class BreedsLoader
    {
        public static void Initialize()
        {
            List<Tuple<byte, int, int>> stats;

            for (int i = 1; i < 15; i++) // Breed Count
            {
                stats = new List<Tuple<byte, int, int>>();
                for (byte y = 10; y < 16; y++) // Stats Count
                {                  
                    if (y != (byte)StatsBoostTypeEnum.Vitality && y != (byte)StatsBoostTypeEnum.Wisdom)
                    {
                        string[] formulas = BreedManager.Instance.GetStatsFormulas(i, (StatsBoostTypeEnum)y).Split('|');
                        for (int x = 0; x < formulas.Length; x++)
                        {
                            int maxStats = int.Parse(formulas[x].Split(',')[0]);
                            int coeffStats = int.Parse(formulas[x].Split(',')[1]);
                            stats.Add(new Tuple<byte, int, int>(y, coeffStats, maxStats));
                        }
                    }
                }
                BreedManager.Instance.BreedStats.Add(i, stats);
            }

            D2OReader reader = new D2OReader(".\\d2os\\Breeds.d2o");
            var breeds = reader.ReadObjects<Breed>();
            BreedManager.Instance.BreedColors = breeds;
        }
    }
}
