﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.WorldServer.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.BaseServer.Loaders.World.Characters
{
    public static class CharactersLoader
    {
        public static void Initialize()
        {
            Logs.Logger.Write("[ World ] Loading Characters...");

            ExperienceManager.Instance.LoadAllExperiences();
            IEnumerable<CharacterRecord> characters = CharacterManager.Instance.GetAllCharacters();

            foreach (var character in characters)
                CharacterManager.Instance.Characters.Add(character.Id, new Character(character));

            Logs.Logger.Write($"[ World ] {characters.Count()} Characters Loaded");
        }
    }
}
