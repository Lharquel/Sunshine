﻿using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Items;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Effects;
using Sunshine.WorldServer.Game.Spells;
using System;
using Dapper;
using Dapper.Contrib;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Game.Actors.Characters.Jobs;

namespace Sunshine.BaseServer.Loaders.World.Items
{
    public static class ItemsLoader
    {
        public static void Initialize()
        {
            Logs.Logger.Write("[ World ] Loading Items...");

            JobManager.Instance.Recipes = JobManager.Instance.GetAllRecipes();

            JobManager.Instance.Harvests = JobManager.Instance.GetAllHarvests();

            var items = ItemManager.Instance.GetAllItemsTemplate();
            
            foreach(var item in items)
            {
                var itemSet = ItemManager.Instance.GetItemSetTemplate(item.ItemSetId);
                if (itemSet != null)
                    item.EffectSets = EffectManager.Instance.GetEffects(itemSet.Effects, true);
                else
                    item.EffectSets = new List<List<Effect>>();

                if (item is ItemTemplate)
                    item.EffectsBase = EffectManager.Instance.GetEffects((item as ItemTemplate).Effects);
                else
                    item.EffectsBase = EffectManager.Instance.GetEffects((item as WeaponTemplate).Effects);

                ItemManager.Instance.Items.Add(item.Id, item);
            }

            Logs.Logger.Write($"[ World ] {ItemManager.Instance.Items.Count} Items Loaded");
        }
    }
}
