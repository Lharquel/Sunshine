﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Maps;
using Sunshine.MySql.Database.World;
using Sunshine.WorldServer.Game;
using Sunshine.WorldServer.Game.Maps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Tools.Dlm;
using Sunshine.Protocol.Tools.D2p;
using Sunshine.Protocol.IO;
using Sunshine.WorldServer.Game.Maps.Interactives;
using Sunshine.WorldServer.Game.Maps.Triggers;
using Sunshine.WorldServer.Game.Maps.Dungeons;
using Sunshine.Protocol.Tools.Ele;

namespace Sunshine.BaseServer.Loaders.World.Maps
{
    public static class MapsLoader
    {
        public static void Initialize()
        {
            Logs.Logger.Write("[ World ] Loading Maps...");

            D2pFile d2PFile = new D2pFile(".\\maps\\maps0.d2p");
            IEnumerable<MapRecord> maps = MapManager.Instance.GetAllMaps();
            var interactives = InteractiveManager.Instance.GetAllInteractiveSpawns();
            var triggers = MapManager.Instance.GetAllTriggerSpawns();
            var dungeons = DungeonManager.Instance.GetAllDungeons();
            var mapData = d2PFile.Entries.ToDictionary(entry => int.Parse(entry.FileName.Remove(entry.FileName.IndexOf('.'))));            
            foreach (var mapRecord in maps)
            {
                Map map = new Map(mapRecord);
                if (mapData.ContainsKey(mapRecord.Id))
                {
                    var data = mapData[mapRecord.Id];
                    var layers = new DlmReader(data.Container.ReadFile(data)).ReadMap();
                    //var eles = new EleReader(".\\maps\\elements.ele").ReadElements();                   
                    //MapManager.Instance.InsertHarvest(mapRecord.Id, layers.Layers, eles);
                    map.Cells = new DlmReader(data.Container.ReadFile(data)).ReadMap().Cells;
                    map.Elements = MapManager.Instance.GetElements(map);
                    map.Interactives = interactives.ContainsKey(mapRecord.Id) ? interactives[mapRecord.Id] : new List<Interactive>();
                    map.Triggers = triggers.ContainsKey(mapRecord.Id) ? triggers[mapRecord.Id] : new List<Trigger>();
                    map.Dungeon = dungeons.ContainsKey(mapRecord.Id) ? new Dungeon(dungeons[mapRecord.Id]) : null;
                    map.GeneratePatternCells();
                }
                if (!MapManager.Instance.SubMaps.ContainsKey(mapRecord.SubAreaId))
                    MapManager.Instance.SubMaps.Add(mapRecord.SubAreaId, new List<Map> { map });
                else
                    MapManager.Instance.SubMaps[mapRecord.SubAreaId].Add(map);
                MapManager.Instance.Maps.Add(mapRecord.Id, map);
            }

            Logs.Logger.Write($"[ World ] {maps.Count()} Maps Loaded");
        }
    }
}
