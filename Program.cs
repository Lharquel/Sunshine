﻿using Sunshine.AuthServer;
using Sunshine.Logs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Sunshine.Servers;
using Sunshine.BaseServer.Commands;

namespace Sunshine
{
    public static class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Sunshine";

            #region Sunshine Logo
            Console.ForegroundColor = ConsoleColor.Cyan;
            string[] SunshineLogo = new string[8]
            {
              "                                                    ",
              " #####                                              ",
              "#     # #    # #    #  ####  #    # # #    # ###### ",
              "#       #    # ##   # #      #    # # ##   # #      ",
              " #####  #    # # #  #  ####  ###### # # #  # #####  ",
              "      # #    # #  # #      # #    # # #  # # #      ",
              "#     # #    # #   ## #    # #    # # #   ## #      ",
              "       #####   ####  #    #  ####  #    # # #    # ######  2.3.7"
            };


            foreach(var letter in SunshineLogo)
            {
                int totalWidth = (Console.BufferWidth + letter.Length) / 2;
                Console.WriteLine(letter.PadLeft(totalWidth));
            }
            Console.WriteLine(Environment.NewLine);
            #endregion

            ServersManager.Instance.Start();
            ExecuteCommand(Console.ReadLine());

        }

        static void ExecuteCommand(string cmd)
        {
            BaseCommand.Execute(cmd);
            cmd = Console.ReadLine();
            ExecuteCommand(cmd);
        }   
    }
}
